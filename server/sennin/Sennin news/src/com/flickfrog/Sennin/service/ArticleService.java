package com.flickfrog.Sennin.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.slim3.datastore.Datastore;
import org.slim3.datastore.ModelQuery;
import org.slim3.memcache.Memcache;
import org.slim3.util.BeanUtil;

import com.flickfrog.Sennin.controller.sennin.IndexController;
import com.flickfrog.Sennin.meta.ArticleMeta;
import com.flickfrog.Sennin.meta.WatchLogMeta;
import com.flickfrog.Sennin.model.Article;
import com.flickfrog.Sennin.model.WatchLog;
import com.google.appengine.api.datastore.Transaction;


public class ArticleService {
    private ArticleMeta t = new ArticleMeta();
    private WatchLogMeta watchMeta = new WatchLogMeta();
    
    protected static final Logger logger = Logger.getLogger(IndexController.class.getName());
    
    public void updateCreateTime(String url, String order) {
        Article article = Datastore.query(t).filter(t.url.equal(url)).asSingle();
        if (article != null) {
            long createEpocTime = Long.parseLong(order);
            article.setCreatedDate(new Date(createEpocTime));
            article.setUpdatedDate(new Date());
            
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(article);
            tx.commit();
        }
    }

    
    public void updateThumbnail(String url, String thumbnail) {
        Article article = Datastore.query(t).filter(t.url.equal(url)).asSingle();
        if (article != null) {
            article.setThumbnail(thumbnail);
            article.setUpdatedDate(new Date());
            
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(article);
            tx.commit();
        }
    }
    
    public void deleteArticle(String url, boolean delete) {
        Article article = Datastore.query(t).filter(t.url.equal(url)).asSingle();
        if (article != null) {
            article.setValid(delete);
            article.setUpdatedDate(new Date());
            
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(article);
            tx.commit();
        }
    }
    
    public void addWatchCount(String uuid, String url) {
        List<WatchLog> watchLogs = Datastore.query(watchMeta).filter(watchMeta.url.equal(url), watchMeta.uuid.equal(uuid)).asList();
        if (watchLogs.size() == 0) {
            Article article = Datastore.query(t).filter(t.url.equal(url)).asSingle();
            article.setWatchCount(article.getWatchCount()+1);
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(article);
            tx.commit();
        }
        
        WatchLog newWatchLog = new WatchLog();
        newWatchLog.setUuid(uuid);
        newWatchLog.setUrl(url);
        Datastore.put(newWatchLog);
    }
    
    
    public String getArtileListJSON(long epocTime) {
        String key = "articlelist_" + epocTime;
        String json;
        if (Memcache.contains(key)) {
            json = Memcache.get(key);
            
            logger.info("<<<<<< FROM CACHE!!! : " + key);
        } else {
            Date date = new Date(epocTime);
            List<Article> articles = Datastore.query(t).filter(t.updatedDate.greaterThan(date)).asList();
            json = "{\"list\":" + t.modelsToJson(articles) + "}";
            
            Memcache.put(key, json);
            
            logger.info(">>>>>> SET CACHE!!! : " + key);
        }
        
        return json;
    }
    
    public Article getArticle(String url) {
        return Datastore.query(t).filter(t.url.equal(url)).asSingle();
    }
    
    
    public Article registerTitle(Map<String, Object> title) {
        Article article = Datastore.query(t).filter(t.url.equal(title.get("url").toString())).asSingle();
        if (article == null) {
            Article newArticle = new Article();
            BeanUtil.copy(title, newArticle);
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(newArticle);
            tx.commit();
            
            logger.log(Level.WARNING,"_____ 記事登録！！ " + title.get("url") + "_____");
        } else {
            logger.log(Level.WARNING,"_____ 記事登録済み _____");
        }
        return article;
    }
    
    public Article registerDetail(String url, Map<String, Object> detail) {
        Article article = getArticle(url);
 
        if (article == null) {
            logger.log(Level.WARNING,"_____ ERROR！！ 記事データがない。。");
        } else {
            article.setThumbnail(detail.get("thumbnail").toString());
            Datastore.put(article);     

            logger.log(Level.WARNING,"_____ 詳細記事登録！！ _____");
        }
        
        return article;
    }
    
    
    public List<Article> getArticleList() {
        ModelQuery<Article> query = Datastore.query(t);
        if (query == null)
        {
            return null;
        }
        else
        {
            return query.sort(t.createdDate.desc).asList();
        }
    }
}
