package com.flickfrog.Sennin.controller.sennin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.memcache.Memcache;

import com.flickfrog.Sennin.model.Article;
import com.flickfrog.Sennin.service.ArticleService;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


public class IndexController extends Controller {

    private ArticleService service = new ArticleService();
    private String url = "http://urban-ascetic.com/sitemap/";
    protected static final Logger logger = Logger.getLogger(IndexController.class.getName());
    
    
    @Override
    public Navigation run() throws Exception {
        UserService userService = UserServiceFactory.getUserService();
        String thisURL = request.getRequestURI();
        if (request.getUserPrincipal() == null) {
            return redirect(userService.createLoginURL(thisURL));
        } else if (!userService.isUserAdmin()) {
            requestScope("loginURL", userService.createLoginURL(thisURL));
            return forward("error.jsp");
        } else {
            String clearCacheFlag = asString("clear");
            if (clearCacheFlag != null) {
                // キャッシュのクリアー
                Memcache.cleanAll();
                
                logger.info("xxxxxx clear all cache !! ");
            }
            
            String url = asString("url");
            String thumbnail = asString("thumbnail");
            if (url != null && thumbnail != null) {
                // サムネイルの更新
                service.updateThumbnail(url, thumbnail);
            }
            
            String deleteUrl = asString("delete");
            if (deleteUrl != null) {
                // 記事の論理削除
                service.deleteArticle(deleteUrl, true);
            }
            String recoverUrl = asString("recover");
            if (recoverUrl != null) {
                // 記事の復活
                service.deleteArticle(recoverUrl, false);
            }
            
            String order = asString("order");
            if (url != null && order != null) {
                // 作成日の更新
                service.updateCreateTime(url, order);
            }
            
            // 記事タイトルのクローリング
            searchTitle();
            
            List<Article> articleList = service.getArticleList();
            requestScope("articleList", articleList);
                        
            return forward("index.jsp");
        }
    }
    
    
    private void searchTitle() {
        try {
            // タイトルの検索
            Document document = Jsoup.connect(url).get();
            Elements elements = document.getElementsByTag("li");
            for (int i=0; i<elements.size(); i++) {
                Element element = elements.get(i);
                
                if (element.child(0).getElementsByAttribute("title").text().equals("生活")) {
                    logger.log(Level.WARNING, "TITLE=" + element.child(0).getElementsByAttribute("title").text());
                    stockListData(element, "life");
                } else if (element.child(0).getElementsByAttribute("title").text().equals("運動")) {
                    logger.log(Level.WARNING, "TITLE=" + element.child(0).getElementsByAttribute("title").text());
                    stockListData(element, "activity");
                } else if (element.child(0).getElementsByAttribute("title").text().equals("食事")) {
                    logger.log(Level.WARNING, "TITLE=" + element.child(0).getElementsByAttribute("title").text());
                    stockListData(element, "food");
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING,"_____ ERRRROOR _____");
            e.printStackTrace();
        }
    }
    private void stockListData(Element element, String type) {
        Elements elements = element.getElementsByTag("ul");
        if (elements.size() == 0)
        {
            logger.log(Level.WARNING,"_____ ULないね。。。 _____");
            return;
        }
        
        Elements As = elements.get(0).getElementsByTag("a");
        String[] urls = new String[As.size()];
        
        // タイトルデータの登録
        for (int i=As.size()-1; i>=0; i--) {
            Element a = As.get(i);
            String url = a.attr("href");
            String title = a.attr("title");
            
            HashMap<String, Object> titleMap = new HashMap<String, Object>();
            titleMap.put("title", title);
            titleMap.put("url", url);
            titleMap.put("type", type);

            service.registerTitle(titleMap);
            
            urls[i] = url;
        }
        
        // 記事詳細データの登録
        for (int j=0; j<urls.length; j++) {
            searchArticleDetail(urls[j]);
        }
    }
    
    
    private void searchArticleDetail(String articleUrl) {
        // 記事の存在チェック
        Article article = service.getArticle(articleUrl);
        if (article == null) {
            logger.log(Level.WARNING,"_____ ERROR！！ 記事データがない。。");
            return;
//        } else if (!(article.getThumbnail() == null || article.getThumbnail().equals("") || article.getThumbnail().equals("noimage.png"))) {
        } else if (!(article.getThumbnail() == null || article.getThumbnail().equals(""))) {
            logger.log(Level.WARNING,"_____ 詳細記事登録済み _____");
            return;
        }

        
        try {
            HashMap<String, Object> detailMap = new HashMap<String, Object>();
            String badImagePath = "http://image.with2.net/img/banner/banner_good.gif";
            String badImageHost = "https://lh4.googleusercontent.com";

            
            // サムネイルの検索
            Document document = Jsoup.connect(articleUrl).get();
            Element mainElement = document.getElementById("main");
            
            // 動画がある場合は、動画のサムネイルを取得する
            Elements movieElements = mainElement.getElementsByTag("iframe");
            if (movieElements.size() > 0 && movieElements.get(0).attr("src").indexOf("www.youtube.com") != -1) {
                Element iframeElement = movieElements.get(0);
                String src = iframeElement.attr("src");
                String[] srcParams = src.split("/");
                String videoID = srcParams[srcParams.length-1];
                
                detailMap.put("thumbnail", "http://img.youtube.com/vi/" + videoID + "/0.jpg");
            } else {
                // 文章からサムネイルを探す
                Elements imgElements = mainElement.getElementsByTag("img");
                if (imgElements.size() > 0) {
                    Element imgElement = imgElements.get(0);
                    String imgPath = imgElement.attr("src");
                    
                    if (imgPath.equals(badImagePath)
                            || imgPath.startsWith(badImageHost)) {
                        detailMap.put("thumbnail", "noimage.png");
                    } else {
                        detailMap.put("thumbnail", imgPath);
                    }
                } else {
                    detailMap.put("thumbnail", "noimage.png");
                }
            }
            
            if (movieElements.size()>0) 
            logger.log(Level.WARNING,"SIZE=" + movieElements.size() + ":" + detailMap.get("thumbnail"));
            
            detailMap.put("isValid", true);
//            detailMap.put("html", mainElement.html());
//            detailMap.put("contents", mainElement.text());
            
            service.registerDetail(articleUrl, detailMap);
        } catch (IOException e) {
            logger.log(Level.WARNING,"_____ ERRRROOR _____");
            e.printStackTrace();
        }
    }
}
