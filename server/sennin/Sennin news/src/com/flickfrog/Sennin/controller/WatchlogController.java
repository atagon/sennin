package com.flickfrog.Sennin.controller;

import java.util.logging.Logger;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.flickfrog.Sennin.controller.sennin.IndexController;
import com.flickfrog.Sennin.service.ArticleService;

public class WatchlogController extends Controller {
    private static final Logger logger = Logger.getLogger(IndexController.class.getName());
    private ArticleService articleService = new ArticleService();
    
    @Override
    public Navigation run() throws Exception {
        String uuid = asString("uuid");
        String url = asString("url");
        
        articleService.addWatchCount(uuid, url);
        return null;
    }
}
