package com.flickfrog.Sennin.controller.articlelist;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.ThrowableUtil;

import com.flickfrog.Sennin.service.ArticleService;

public class IndexController extends Controller {
    private ArticleService service = new ArticleService();
    protected static final Logger logger = Logger.getLogger(IndexController.class.getName());
    
    @Override
    public Navigation run() throws Exception {
        String lastDate = asString("lastdate");
        
        String json = service.getArtileListJSON(new Long(lastDate).longValue());
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        try {
            PrintWriter out = null; 
            try { 
                out = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "utf-8")); 
                out.print(json); 
            } finally {
                if (out != null) { 
                    out.close(); 
                }
            } 
        } catch (IOException e) { 
            ThrowableUtil.wrapAndThrow(e); 
        }
        return null; 
    }
}
