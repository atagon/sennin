package com.flickfrog.Sennin.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2015-06-28 23:37:50")
/** */
public final class ArticleMeta extends org.slim3.datastore.ModelMeta<com.flickfrog.Sennin.model.Article> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.util.Date> createdDate = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.util.Date>(this, "createdDate", "createdDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Long> epocTimeCreateDate = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Long>(this, "epocTimeCreateDate", "epocTimeCreateDate", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Integer> goodCount = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Integer>(this, "goodCount", "goodCount", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Boolean> valid = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Boolean>(this, "valid", "valid", boolean.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article> thumbnail = new org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article>(this, "thumbnail", "thumbnail");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article> title = new org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article>(this, "title", "title");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article> type = new org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article>(this, "type", "type");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.util.Date> updatedDate = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.util.Date>(this, "updatedDate", "updatedDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article> url = new org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.Article>(this, "url", "url");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Integer> watchCount = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.Article, java.lang.Integer>(this, "watchCount", "watchCount", int.class);

    private static final ArticleMeta slim3_singleton = new ArticleMeta();

    /**
     * @return the singleton
     */
    public static ArticleMeta get() {
       return slim3_singleton;
    }

    /** */
    public ArticleMeta() {
        super("Article", com.flickfrog.Sennin.model.Article.class);
    }

    @Override
    public com.flickfrog.Sennin.model.Article entityToModel(com.google.appengine.api.datastore.Entity entity) {
        com.flickfrog.Sennin.model.Article model = new com.flickfrog.Sennin.model.Article();
        model.setCreatedDate((java.util.Date) entity.getProperty("createdDate"));
        model.setEpocTimeCreateDate((java.lang.Long) entity.getProperty("epocTimeCreateDate"));
        model.setGoodCount(longToPrimitiveInt((java.lang.Long) entity.getProperty("goodCount")));
        model.setValid(booleanToPrimitiveBoolean((java.lang.Boolean) entity.getProperty("valid")));
        model.setKey(entity.getKey());
        model.setThumbnail((java.lang.String) entity.getProperty("thumbnail"));
        model.setTitle((java.lang.String) entity.getProperty("title"));
        model.setType((java.lang.String) entity.getProperty("type"));
        model.setUpdatedDate((java.util.Date) entity.getProperty("updatedDate"));
        model.setUrl((java.lang.String) entity.getProperty("url"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        model.setWatchCount(longToPrimitiveInt((java.lang.Long) entity.getProperty("watchCount")));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        com.flickfrog.Sennin.model.Article m = (com.flickfrog.Sennin.model.Article) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("createdDate", m.getCreatedDate());
        entity.setProperty("epocTimeCreateDate", m.getEpocTimeCreateDate());
        entity.setProperty("goodCount", m.getGoodCount());
        entity.setProperty("valid", m.isValid());
        entity.setProperty("thumbnail", m.getThumbnail());
        entity.setProperty("title", m.getTitle());
        entity.setProperty("type", m.getType());
        entity.setProperty("updatedDate", m.getUpdatedDate());
        entity.setProperty("url", m.getUrl());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("watchCount", m.getWatchCount());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        com.flickfrog.Sennin.model.Article m = (com.flickfrog.Sennin.model.Article) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        com.flickfrog.Sennin.model.Article m = (com.flickfrog.Sennin.model.Article) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        com.flickfrog.Sennin.model.Article m = (com.flickfrog.Sennin.model.Article) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        com.flickfrog.Sennin.model.Article m = (com.flickfrog.Sennin.model.Article) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        com.flickfrog.Sennin.model.Article m = (com.flickfrog.Sennin.model.Article) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getCreatedDate() != null){
            writer.setNextPropertyName("createdDate");
            encoder0.encode(writer, m.getCreatedDate());
        }
        if(m.getEpocTimeCreateDate() != null){
            writer.setNextPropertyName("epocTimeCreateDate");
            encoder0.encode(writer, m.getEpocTimeCreateDate());
        }
        writer.setNextPropertyName("goodCount");
        encoder0.encode(writer, m.getGoodCount());
        writer.setNextPropertyName("valid");
        encoder0.encode(writer, m.isValid());
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getThumbnail() != null){
            writer.setNextPropertyName("thumbnail");
            encoder0.encode(writer, m.getThumbnail());
        }
        if(m.getTitle() != null){
            writer.setNextPropertyName("title");
            encoder0.encode(writer, m.getTitle());
        }
        if(m.getType() != null){
            writer.setNextPropertyName("type");
            encoder0.encode(writer, m.getType());
        }
        if(m.getUpdatedDate() != null){
            writer.setNextPropertyName("updatedDate");
            encoder0.encode(writer, m.getUpdatedDate());
        }
        if(m.getUrl() != null){
            writer.setNextPropertyName("url");
            encoder0.encode(writer, m.getUrl());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.setNextPropertyName("watchCount");
        encoder0.encode(writer, m.getWatchCount());
        writer.endObject();
    }

    @Override
    protected com.flickfrog.Sennin.model.Article jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        com.flickfrog.Sennin.model.Article m = new com.flickfrog.Sennin.model.Article();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("createdDate");
        m.setCreatedDate(decoder0.decode(reader, m.getCreatedDate()));
        reader = rootReader.newObjectReader("epocTimeCreateDate");
        m.setEpocTimeCreateDate(decoder0.decode(reader, m.getEpocTimeCreateDate()));
        reader = rootReader.newObjectReader("goodCount");
        m.setGoodCount(decoder0.decode(reader, m.getGoodCount()));
        reader = rootReader.newObjectReader("valid");
        m.setValid(decoder0.decode(reader, m.isValid()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("thumbnail");
        m.setThumbnail(decoder0.decode(reader, m.getThumbnail()));
        reader = rootReader.newObjectReader("title");
        m.setTitle(decoder0.decode(reader, m.getTitle()));
        reader = rootReader.newObjectReader("type");
        m.setType(decoder0.decode(reader, m.getType()));
        reader = rootReader.newObjectReader("updatedDate");
        m.setUpdatedDate(decoder0.decode(reader, m.getUpdatedDate()));
        reader = rootReader.newObjectReader("url");
        m.setUrl(decoder0.decode(reader, m.getUrl()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        reader = rootReader.newObjectReader("watchCount");
        m.setWatchCount(decoder0.decode(reader, m.getWatchCount()));
        return m;
    }
}