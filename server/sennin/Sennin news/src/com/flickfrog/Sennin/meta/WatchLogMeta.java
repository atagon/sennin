package com.flickfrog.Sennin.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2015-06-27 15:08:26")
/** */
public final class WatchLogMeta extends org.slim3.datastore.ModelMeta<com.flickfrog.Sennin.model.WatchLog> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.WatchLog, java.util.Date> createdDate = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.WatchLog, java.util.Date>(this, "createdDate", "createdDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.WatchLog, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.WatchLog, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.WatchLog> url = new org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.WatchLog>(this, "url", "url");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.WatchLog> uuid = new org.slim3.datastore.StringAttributeMeta<com.flickfrog.Sennin.model.WatchLog>(this, "uuid", "uuid");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.WatchLog, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<com.flickfrog.Sennin.model.WatchLog, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final WatchLogMeta slim3_singleton = new WatchLogMeta();

    /**
     * @return the singleton
     */
    public static WatchLogMeta get() {
       return slim3_singleton;
    }

    /** */
    public WatchLogMeta() {
        super("WatchLog", com.flickfrog.Sennin.model.WatchLog.class);
    }

    @Override
    public com.flickfrog.Sennin.model.WatchLog entityToModel(com.google.appengine.api.datastore.Entity entity) {
        com.flickfrog.Sennin.model.WatchLog model = new com.flickfrog.Sennin.model.WatchLog();
        model.setCreatedDate((java.util.Date) entity.getProperty("createdDate"));
        model.setKey(entity.getKey());
        model.setUrl((java.lang.String) entity.getProperty("url"));
        model.setUuid((java.lang.String) entity.getProperty("uuid"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        com.flickfrog.Sennin.model.WatchLog m = (com.flickfrog.Sennin.model.WatchLog) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("createdDate", m.getCreatedDate());
        entity.setProperty("url", m.getUrl());
        entity.setProperty("uuid", m.getUuid());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        com.flickfrog.Sennin.model.WatchLog m = (com.flickfrog.Sennin.model.WatchLog) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        com.flickfrog.Sennin.model.WatchLog m = (com.flickfrog.Sennin.model.WatchLog) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        com.flickfrog.Sennin.model.WatchLog m = (com.flickfrog.Sennin.model.WatchLog) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        com.flickfrog.Sennin.model.WatchLog m = (com.flickfrog.Sennin.model.WatchLog) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        com.flickfrog.Sennin.model.WatchLog m = (com.flickfrog.Sennin.model.WatchLog) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getCreatedDate() != null){
            writer.setNextPropertyName("createdDate");
            encoder0.encode(writer, m.getCreatedDate());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getUrl() != null){
            writer.setNextPropertyName("url");
            encoder0.encode(writer, m.getUrl());
        }
        if(m.getUuid() != null){
            writer.setNextPropertyName("uuid");
            encoder0.encode(writer, m.getUuid());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected com.flickfrog.Sennin.model.WatchLog jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        com.flickfrog.Sennin.model.WatchLog m = new com.flickfrog.Sennin.model.WatchLog();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("createdDate");
        m.setCreatedDate(decoder0.decode(reader, m.getCreatedDate()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("url");
        m.setUrl(decoder0.decode(reader, m.getUrl()));
        reader = rootReader.newObjectReader("uuid");
        m.setUuid(decoder0.decode(reader, m.getUuid()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}