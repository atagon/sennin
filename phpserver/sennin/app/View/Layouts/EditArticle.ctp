<html>
<head>
<?= $this->Html->charset(); ?>
<title>
<?= $title_for_layout; ?>
</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<script type="text/javascript">
function updateOrder(url, updateTime) {
	if (updateTime == "")
	{
		alert("更新日付を入力して下さい。");
	}
	else
	{
		if (window.confirm("指定の記事の順番を変更します。よろしいですか？")) {
			location.href = "http://" + location.host + location.pathname + "updateorder?url=" + url + "&order=" + updateTime;
		} else {
			alert("キャンセルしました");
		}
	}
}


function deleteArticle(url) {
	if (window.confirm("指定の記事を削除します。よろしいですか？")) {
		location.href = "http://" + location.host + location.pathname + "deletearticle?url=" + url;
	} else {
		alert("キャンセルしました");
	}
}
function recoverArticle(url) {
	if (window.confirm("指定の記事を復活させます。よろしいですか？")) {
		location.href = "http://" + location.host + location.pathname + "recoverarticle?url=" + url;
	} else {
		alert("キャンセルしました");
	}
}


function updateThumb(urlPath, updatePath) {
	if (updatePath == "")
	{
		alert("サムネイルのURLを入力して下さい。");
	}
	else
	{
		location.href = "http://" + location.host + location.pathname + "updatethumb/?url=" + urlPath + "&thumbnail=" + updatePath;
	}
}

</script>

</head>
<body>

<table class="table table-striped table-bordered table-hover">
<thead><tr class="success">
<th>delete</th>
<th>title</th>
<th>URL</th>
<th>type</th>
<th>thumbnail</th>
<th>create date</th>
<th>update date</th>
<th>order</th>
</tr></thead>

<tbody>
<?php $i=0; ?>
<?php foreach ($datas as $data): ?>
<?php if ($data['mst_blogs']['valid']): ?>
<tr>
<?php else: ?>
<tr class="danger">
<?php endif; ?>

<?php if ($data['mst_blogs']['valid']): ?>
<td><button class="btn" onClick="deleteArticle('<?= $data['mst_blogs']['url']; ?>')">削除</button>
<?php else: ?>
<td><button class="btn" onClick="recoverArticle('<?= $data['mst_blogs']['url']; ?>')">復活</button>
<?php endif; ?>
<td><?= $data['mst_blogs']['title']; ?></td>
<td><?= $data['mst_blogs']['url']; ?></td>
<td><?= $data['mst_blogs']['type']; ?></td>

<?php if ($data['mst_blogs']['thumbnail'] == "noimage.png"): ?>
<td><img src="http://flickfrog.com/sennin/noimage@2x.png" width="110" height="90" />
<?php else: ?>
<td><img src="<?= $data['mst_blogs']['thumbnail']; ?>" width="110" height="90" />
<?php endif; ?>

<br>
<input type="text" value="<?= $data['mst_blogs']['thumbnail']; ?>" id="thumbnail<?= $i ?>"/>
<br>
<button class="btn" onClick="updateThumb('<?= $data['mst_blogs']['url']; ?>', thumbnail<?= $i ?>.value)">更新</button>
</td>

<td><?= $data['mst_blogs']['create_time']; ?></td>
<td><?= $data['mst_blogs']['update_time']; ?></td>

<td><?= $data[0]['epoc_create_time']; ?>
<br>
<input type="text" value="" id="order<?= $i ?>"/>
<br>
<button class="btn" onClick="updateOrder('<?= $data['mst_blogs']['url']; ?>', order<?= $i ?>.value)">更新</button>
</td>

</tr>
<?php $i++; ?>
<?php endforeach; ?>
</tbody>
</table>
</body>
</html>
