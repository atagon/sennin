<?php
App::import('Vendor', 'simple_html_dom');

class EditController extends AppController {
	public function index() {
		$this->loadModel('MstBlog');

		$this->_searchTitle();


		$this->layout = 'EditArticle'; // View/Layouts/EditARticle.ctp が読み込まれる
		$this->set("title_for_layout", "記事の管理");
		$this->set("test", "てすとだよ");

				$datas = $this->MstBlog->query('SELECT url, title, type, thumbnail, create_time, update_time, UNIX_TIMESTAMP(create_time) as epoc_create_time, valid FROM mst_blogs ORDER BY create_time desc;');
		$this->set("datas", $datas);
	}

	public function updatethumb() {
		$url = $this->params['url']['url'];
		$thumbnail = $this->params['url']['thumbnail'];

		$this->loadModel('MstBlog');
		$params = array('url' => $url, 'thumbnail' => $thumbnail);
		$details = $this->MstBlog->query('UPDATE mst_blogs SET thumbnail = :thumbnail, update_time = now() WHERE url = :url;',  $params);

		$this->redirect("./");
	}

	public function deletearticle() {
		$url = $this->params['url']['url'];

		$this->loadModel('MstBlog');
		$params = array('url' => $url);
		$details = $this->MstBlog->query('UPDATE mst_blogs SET valid = 0, update_time = now() WHERE url = :url;',  $params);

		$this->redirect("./");
	}

	
	public function recoverarticle() {
		$url = $this->params['url']['url'];

		$this->loadModel('MstBlog');
		$params = array('url' => $url);
		$details = $this->MstBlog->query('UPDATE mst_blogs SET valid = 1, update_time = now() WHERE url = :url;',  $params);

		$this->redirect("./");
	}

	public function updateorder() {
		$url = $this->params['url']['url'];
		$order = $this->params['url']['order'];

		$this->loadModel('MstBlog');
		$params = array('url' => $url, 'order' => $order);
		$details = $this->MstBlog->query('UPDATE mst_blogs SET create_time = FROM_UNIXTIME(:order), update_time = now() WHERE url = :url;',  $params);

		$this->redirect("./");

	}


	/** 記事のクローリング **/
	private function _searchTitle() {
		$html = file_get_html('http://urban-ascetic.com/sitemap/');
		// タイトルの検索
		foreach ($html->find('li') as $li) {
			$type = "";
			$a = $li->find('a');
			if (count($a) > 0) {
				$title = $a[0]->title;
				if ($title == '生活') {
					$type = 'life';
					
				} else if ($title == '運動') {
					$type = 'activity';

				} else if ($title == '食事') {
					$type = 'food';
				}

				if ($type != "") {
					// 任意のカテゴリーのデータをまとめて登録する
					$this->_stockListData($a, $type);
				}
			}
		}
	}
	
	private function _isRegisteredData($url) {
		$params = array('url' => $url);
		if (count($this->MstBlog->query('SELECT url FROM mst_blogs WHERE url = :url;',  $params)) == 0) {
			return false;
		} else {
			return true;
		}
	}

	private function _stockListData($a, $type) {
		// i=0はカテゴリーなので1まで、古い順に登録していく
		for ($i=count($a)-1; $i>0; $i--) {
			$url = $a[$i]->href;

			if (!$this->_isRegisteredData($url)) {
				$title = $a[$i]->title;
				
				$params = array('title' => $title, 'url' => $url, 'type' => $type);
				$details = $this->MstBlog->query('INSERT INTO mst_blogs(title, url, type, create_time, update_time) VALUES(:title, :url, :type, now(), now());',  $params);
			} else {
				// do nothing.
			}

			// 本文の登録
			$html = $this->_saveDetail($url);

			// サムネイルの登録
			$this->_saveThumbnail($url, $html);
		}
	}

	
	private function _saveDetail($url) {
		$html = "";
		
		$params = array('url' => $url);
		$details = $this->MstBlog->query('SELECT detail FROM mst_blogs WHERE url = :url;',  $params);
		if (count($details) == 0) {
		//	echo "(error) データが無い。";
		} else {
			$detail = $details[0];
			if ($detail['mst_blogs']['detail'] == '') {

				$html = file_get_html($url);
				if ($html == false) {
					//echo "データないかも : " . $url . "</br>";
					return;
				}
				$html_main = $html->find('#main');

				$isMovie = false;
				foreach($html_main[0]->find('iframe') as $iframe) {
					if (strpos($iframe->src, 'www.youtube.com') !== false) {
						$isMovie = true;
						break;
					}
				}

				// 関連記事の抽出
				$relatedArticles = $html->find('[class=related_post wp_rp]');
				if (count($relatedArticles) == 0) {
					$relatedArticles = "なかった";
				} else {
					$relatedArticles = $relatedArticles[0];
				}

				// 上部の日付削除
				$html_main[0]->find('div.singledate')[0]->outertext = '';

				// DATA URI スキーム対策
				if ($isMovie) {
					$noscripts = array("<noscript>", "</noscript>");
					$editedDetail = str_replace($noscripts, "", $html_main[0]);

					// 動画のサイズを変更
					$editedDetail = str_replace("<iframe width=\"560\"", "<iframe width=\"100%\"", $editedDetail);
					// URLが中途半端なところは修復する
					$editedDetail = str_replace("src=\"//www.youtube.com", "src=\"http://www.youtube.com", $editedDetail);
					

				} else {
					$editedDetail = str_replace('src="data:image/gif;base64', 'xx_src="data:image/gif;base64', $html_main[0]);
					$editedDetail = str_replace('data-lazy-src="', 'src="', $editedDetail);
				}
				
				// 本文上部のソーシャルリンクの削除
				$editedDetail = preg_replace('/<ul class="mt10 socialBt2">.*<div class="singlemsg">/', '<div class="singlemsg">', $editedDetail);
				
				// 本文下部のソーシャルリンクの削除
				$editedDetail = preg_replace('/<p>参考になったらクリックお願いします。.*<\/div>/', '</div>', $editedDetail);
				$editedDetail = preg_replace('/<ul class="mt50 socialBt2">.*<\/div>/', '</div>', $editedDetail);


				// メールマガジン登録フォームの修正
				// メール登録フォールを小さくする
				$editedDetail = str_replace('<table style="max-width: 500px; font-size: 14px;" border="0" cellspacing="1" cellpadding="3" bgcolor="#953716">', '<table style="width:100%; font-size: 12px;" border="0" cellspacing="1" cellpadding="3" bgcolor="#953716">', $editedDetail);

				$editedDetail = str_replace('<td align="center" bgcolor="#edbba6" width="150"><b>名前</b></td>', '<td style="width:150px;" bgcolor="#edbba6"><b>名前</b></td>', $editedDetail);

				$editedDetail = str_replace('<td align="center" bgcolor="#edbba6" width="150"><b>メールアドレス</b></td>', '<td bgcolor="#edbba6"><b>メールアドレス</b></td>', $editedDetail);

				// メール登録フィームの送信先変更
				$editedDetail = str_replace(
					'<form action="https://55auto.biz/kurohane/responder.php" enctype="multipart/form-data" method="post"><input name="mcode" type="hidden" value="UTF-8" /><input name="tno" type="hidden" value="131" /><input name="fld40" type="hidden" value="RES_TSG_BLG_RSR" />',
					'<form action="https://55auto.biz/kurohane/responder.php" enctype="multipart/form-data" method="post"><input name="mcode" type="hidden" value="UTF-8" /><input name="tno" type="hidden" value="133" /><input name="fld40" type="hidden" value="RES_TSG_IAP_RSR" /><input name="bak" type="hidden" value="-2" />', $editedDetail);
				

				// スタイルシートの適用
				$editedDetail = sprintf("<html><head>
						<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
						<style type=\"text/css\">
					      html{
					      overflow-x : hidden;
					      overflow-y : auto;
					      }
					      body{
						  overflow-x : hidden;
						  overflow-y : auto;
					      }
					      image{
					      width : 30px;
					      }
					      p{margin:10;}
					      #main p{font-size:13.5pt; color:#524942; padding:0 10 0 10; line-height:150%%;}
					      .art_ttl {height:auto !important; text-align:center; padding:30 0 30 0; font-size:18px; color:#F5F5F5; background:#f8f7ed url(http://urban-ascetic.com/wp/wp-content/themes/cloudtpl_787/images/bg_top.png) repeat-x;}
					      .singledate {visibility: hidden;}
					      body{margin:0; overflow-x:hidden;}
					      .pcmode{padding:0px 0; width:100%%; margin:0;}
					      </style>
					      </head>
					      <body width=\"320px\">%s<div hidden>%s</div></body>
					      </html>", $editedDetail, $relatedArticles);
				 

				// 登録
				$params = array('detail' => $editedDetail, 'url' => $url);
				$this->MstBlog->query('UPDATE mst_blogs SET detail = :detail WHERE url = :url;',  $params);
			}
		}

		return $html;
	}

	private function _saveThumbnail($url, $html) {
		$params = array('url' => $url);
		$thumbnails = $this->MstBlog->query('SELECT thumbnail FROM mst_blogs WHERE url = :url;',  $params);
		if (count($thumbnails) == 0) {
			//echo "(error) データが無い。";
		} else {
			$thumbnail = $thumbnails[0];
			if ($thumbnail['mst_blogs']['thumbnail'] == '') {
				$badImagePath = "http://image.with2.net/img/banner/banner_good.gif";
				$badImageHost = "https://lh4.googleusercontent.com";
				$senninHost = "http://urban-ascetic.com/wp/wp-content/uploads";

				$setImage = false;
				if ($html == "") {
					$mainElement = file_get_html($url)->find('#main');
				} else {
					$mainElement = $html->find('#main');
				}
				if ($mainElement == false) {
					//echo "データないかも2 : " . $url . "</br>";
					return;
				}
				
				// 動画がある場合は、動画のサムネイルを取得する
				$thumbnailPath = "noimage.png";
				
				if (count($mainElement > 0)) {
					foreach($mainElement[0]->find('iframe') as $iframe) {
						if (strpos($iframe->src, 'www.youtube.com') !== false) {
							list($host, $videoID) = split("\/embed\/", $iframe->src);
							$thumbnailPath = "http://img.youtube.com/vi/" . $videoID . "/0.jpg";
							$setImage = true;
							break;
						}
					}
					
					if (!$setImage) {
						// 画像を検索する
						foreach($mainElement[0]->find('img') as $img) {
							if (strpos($img->src, $senninHost) !== false) {
								$thumbnailPath = $img->src;

								break;
							}
						}	
					}

					// 登録
					$params = array('url' => $url, 'thumbnail' => $thumbnailPath);
					$thumbnails = $this->MstBlog->query('UPDATE mst_blogs SET thumbnail = :thumbnail WHERE url = :url;',  $params);
				} else {
					//echo "!!!! #mainが無い: " . $url . "</br>";
					return;
				}

			} else {
				//echo "not null" . $url . "</br>";
			}
		}

	}
}
