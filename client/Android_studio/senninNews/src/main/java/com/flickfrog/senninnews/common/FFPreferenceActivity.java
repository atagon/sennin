package com.flickfrog.senninnews.common;

import com.flickfrog.senninnews.R;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class FFPreferenceActivity extends PreferenceActivity {
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.pref);
	}
}
