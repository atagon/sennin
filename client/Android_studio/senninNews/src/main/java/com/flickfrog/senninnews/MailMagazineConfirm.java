package com.flickfrog.senninnews;

import org.apache.http.Header;

import com.flickfrog.senninnews.data.Config;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class MailMagazineConfirm extends Activity {
	private boolean isCompleted = false; 
	private WebView webView = null;
	private ProgressBar progressBar = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_mailmagazine_confirm);
		
		progressBar = (ProgressBar)findViewById(R.id.mailMagazineProgressBar);
		
		webView = (WebView)findViewById(R.id.mailMagazineConfirmView);
		webView.getSettings().setJavaScriptEnabled(true);
//		if (Build.VERSION.SDK_INT > 7) {
//			webView.getSettings().setPluginState(PluginState.ON);
//		} else {
//			webView.getSettings().setP
//		}
		webView.setVerticalScrollbarOverlay(true);
		webView.setInitialScale((int)(1.3 * 100));
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onLoadResource(WebView view, String url) {
				if (url.equals("https://55auto.biz/kurohane/responder.php")) {
					Log.i("onLoadResource", "登録成功");
					isCompleted = true;
				}
				
				super.onLoadResource(view, url);
			}
		});
		
		
		Intent intent = this.getIntent();
		String name = intent.getStringExtra("name");
		String mail = intent.getStringExtra("mail");
		
		AsyncHttpClient client = new AsyncHttpClient();
		PackageInfo packageInfo;
		String versionName = "Not found";
		try {
			packageInfo = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);
			versionName = packageInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		client.setUserAgent("仙人News " + versionName);
		RequestParams params = new RequestParams();
		params.put("mcode", "UTF-8");
		params.put("tno", "133");
		params.put("fld40", "RES_TSG_IAP_RSR");
		params.put("bak", "-2");
		params.put("name1", name);
		params.put("email", mail);
		
		client.post(Config.MAILMAGAZINE_REGISTER_URL, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				progressBar.setVisibility(View.INVISIBLE);
				
				String htmlString = new String(arg2);
				
				
				// HTMLを改ざんして、スマフォに最適な表示に変える
		        String destHTMLParts = "<style type=\"text/css\">" +
		                            "#wrap{padding:0px 0; width:100%25; margin:0;}" + 
		                            "TABLE{width:250px;}" +
		                            "HR{width:100%25;}" +
		                            "TABLE TD {word-break: break-all;}" +
		        ".btn {" +
			"width: 200px;" +
		    "height: 50px;" +
			"margin: 0;" +
			"padding: 5px;" +
			"background: -webkit-gradient(linear, left top, left bottom, from(#FFAA33), to(#FF8800));" +
			"border: 1px #F27300 solid;" +
			"color: #FFF;" +
		        "-webkit-appearance: none;" +
		        "-webkit-border-radius: 10px;" +
		        "-webkit-box-shadow: 0 2px 2px #CCC;" +
		        "text-shadow: 1px 2px 3px #C45C00;" +
		    "}" +
		                            "</style>" +
		                            "</head>";
		        htmlString = htmlString.replace("</head>", destHTMLParts);
		        htmlString = htmlString.replace("<INPUT type=\"button\" class=\"btn\" name=\"bak\" value=\"戻る\" onClick=\"history.back()\">", "");
		        htmlString = htmlString.replace("/jslib", "https://55auto.biz/jslib");
		        htmlString = htmlString.replace("/css/main.css", "https://55auto.biz/css/main.css");
		        htmlString = htmlString.replace("responder.php", Config.MAILMAGAZINE_REGISTER_URL);
		        
		        webView.loadData(htmlString, "text/html; charset=utf-8", "utf-8");
			}
			
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				progressBar.setVisibility(View.INVISIBLE);
				
				AlertDialog.Builder alert = new AlertDialog.Builder(MailMagazineConfirm.this);
				alert.setTitle("エラー");
				alert.setMessage("通信中にエラーが発生しました。しばらく待ってからもう一度お試しください。");
				alert.setPositiveButton("OK", null);
				alert.show();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		if (isCompleted) {
			Intent intent = new Intent(MailMagazineConfirm.this, com.flickfrog.senninnews.MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		}
		
		super.onDestroy();
	}
}
