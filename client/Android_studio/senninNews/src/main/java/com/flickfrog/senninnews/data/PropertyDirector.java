package com.flickfrog.senninnews.data;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;

public class PropertyDirector {
	public static AdView adView = null;


	public AdView getAdView(Context context) {
		if (adView == null) {
			adView = new AdView(context);
			adView.setAdUnitId(Config.ADMOB_UNITID);
			adView.setAdSize(AdSize.BANNER);
			AdRequest adRequest = new AdRequest.Builder().build();
			adView.loadAd(adRequest);
		}
		
		return adView;
	}
	
	public void sendGoogleAnalitics(String screenName, Context context) {
		EasyTracker tracker = EasyTracker.getInstance(context);
		tracker.set(Fields.SCREEN_NAME, screenName);
		tracker.send(MapBuilder.createAppView().build());
		
		Log.i("Analitics", "スクリーン：" + screenName);
	}
	
	public void sendGoogleAnaliticsActionCategory(String category, String action, String label, Context context) {
		EasyTracker tracker = EasyTracker.getInstance(context);
		tracker.send(MapBuilder.createEvent(category, action, label, null).build());
		
		Log.i("Analitics", "イベント：" + category + ":" + action + ":" + label);
	}
}
