package com.flickfrog.senninnews;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ArticleListAdapter extends BaseAdapter {
	private Context context = null;
	private ArrayList<ArticleItem> data = null;
	private int resource = 0;
	
	public ArticleListAdapter(Context context, ArrayList<ArticleItem> data, int resource) {
		this.context = context;
		this.data = data;
		this.resource = resource;
	}
	
	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return data.get(arg0).getId();
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		Activity activity = (Activity)context;
		ArticleItem item = (ArticleItem)getItem(arg0);
		
		RelativeLayout layout = (RelativeLayout)activity.getLayoutInflater().inflate(resource, null);
		ImageView articleTypeIcon = (ImageView)layout.findViewById(R.id.articleTypeIcon);

		String title = item.getTitle();
		if (title.contains("【動画】")) {
			title = title.replace("【動画】", "");
			articleTypeIcon.setImageResource(R.drawable.view_blog_icondouga);
			articleTypeIcon.setVisibility(View.VISIBLE);
		} else if (title.contains("【告知】")) {
			title = title.replace("【告知】", "");			
			articleTypeIcon.setImageResource(R.drawable.view_blog_iconkokuchi);
			articleTypeIcon.setVisibility(View.VISIBLE);
		} else if (title.contains("【転記】")) {
			title = title.replace("【転記】", "");
			articleTypeIcon.setImageResource(R.drawable.view_blog_icontenki);
			articleTypeIcon.setVisibility(View.VISIBLE);
		} else {
			articleTypeIcon.setVisibility(View.INVISIBLE);
		}
		((TextView)layout.findViewById(R.id.articleTitle)).setText(title);
		
		TextView percentText = ((TextView)layout.findViewById(R.id.percentText));
		ImageView readedIcon = ((ImageView)layout.findViewById(R.id.readedIcon));
		if (item.getPercent() == 0)
		{
			percentText.setVisibility(View.INVISIBLE);
			readedIcon.setVisibility(View.INVISIBLE);
		} else if (item.getPercent() == 100) {
			percentText.setVisibility(View.INVISIBLE);
			readedIcon.setVisibility(View.VISIBLE);
		} else {
			percentText.setVisibility(View.VISIBLE);
			percentText.setText(item.getPercent() + "%");
			readedIcon.setVisibility(View.INVISIBLE);
		}
		
		ImageView newIcon = (ImageView)layout.findViewById(R.id.newIcon);
		if (item.isNew()) {
			newIcon.setVisibility(View.VISIBLE);
		} else {
			newIcon.setVisibility(View.INVISIBLE);
		}
		
		ImageView image = (ImageView)layout.findViewById(R.id.articleThumbnail);
		ProgressBar progressBar = (ProgressBar)layout.findViewById(R.id.thumbnailProgressBar);
		
		Bitmap thumbnail = imageFromFile(item.getThumbnail());
		if (thumbnail == null)
		{
			ImageGetTask task = new ImageGetTask(image, progressBar);
			task.execute(item.getThumbnail());
		}
		else
		{
			image.setImageBitmap(thumbnail);
			progressBar.setVisibility(View.INVISIBLE);
		}
		
		return layout;
	}
	
	public Bitmap imageFromFile(String filePath) {
		String state = Environment.getExternalStorageState();
		String fileName = filePath.replace("http://", "").replace("/", "_").replace(".jpg", "").replace(".", "_");

		File file;
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			file = new File(Environment.getExternalStorageDirectory().getPath() + "/senninnews/" + fileName + ".jpg");
		} else {
			file = new File(this.context.getFilesDir().getAbsolutePath() + "/senninnews/" + fileName + ".jpg");
		}
 
		if (!file.exists()) {
			return null;
		} else {
			return BitmapFactory.decodeFile(file.getPath());
		}
	}
	
	public void saveToFile(Bitmap bitmap, String filePath) {		
		try {
			String state = Environment.getExternalStorageState();
			File file;
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				file = new File(Environment.getExternalStorageDirectory().getPath() + "/senninnews/");
				Log.d("#save thumb", "save to SD.");
			} else {
				file = new File(this.context.getFilesDir().getAbsolutePath() + "/senninnews/");
				Log.d("#save thumb", "save to InsideStrage.");
			}
 
			if (!file.exists()) {
				file.mkdir();
			}
			
			String fileName = filePath.replace("http://", "").replace("/", "_").replace(".jpg", "").replace(".", "_");
			String AttachName = file.getAbsolutePath() + "/" + fileName + ".jpg";
			FileOutputStream out = new FileOutputStream(AttachName);
			bitmap.compress(CompressFormat.JPEG, 100, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	class ImageGetTask extends AsyncTask<String, Void, Bitmap> {
		private ImageView image;
		private ProgressBar progressBar;
		
		public ImageGetTask(ImageView _image, ProgressBar _progressBar) {
			image = _image;
			progressBar = _progressBar;
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap bitmap = null;
			try {
				Log.d("Activity", ">>>" + params[0]);
				URL url = new URL(params[0]);
				InputStream input= url.openStream();
				bitmap = BitmapFactory.decodeStream(input);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// キャッシュとしてストレージに保存
			saveToFile(bitmap, params[0]);

			return bitmap;
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			image.setImageBitmap(result);
			progressBar.setVisibility(View.INVISIBLE);
		}
	}

}
