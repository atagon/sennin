package com.flickfrog.senninnews;

import java.util.ArrayList;
import java.util.HashMap;

import com.flickfrog.senninnews.common.NotificationsCallback;
import com.flickfrog.senninnews.data.Config;
import com.flickfrog.senninnews.data.PropertyDirector;
import com.flickfrog.senninnews.data.SQLiteManager;
import com.flickfrog.senninnews.data.UpdateArticleAsyncTask;
import com.flickfrog.senninnews.data.UpdateArticleAsyncTask.AsyncTaskCallback;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.unicon_ltd.konect.sdk.IKonectNotificationsCallback;
import com.unicon_ltd.konect.sdk.KonectNotificationsAPI;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity {
	private int REQUEST_CODE_ARTICLEDETAIL = 9999;
	
	private ArrayList<String> urlLists;
	private HashMap<String, ArrayList<HashMap<String, Object>>> rowDataDictionary;
	private String[] articleTypes = {"life", "activity", "food", "record"};
	int selectedTabIndex;
	private GestureDetector gestureDetector;
	
	private String prevURL = null;
	private LinearLayout layoutAd;
	private AdView adView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		new PropertyDirector().sendGoogleAnalitics("記事一覧", this);
		
		new SQLiteManager(this).updateDB();

		setContentView(R.layout.activity_main);

		adView = new AdView(this);
		adView.setAdUnitId(Config.ADMOB_UNITID);
		adView.setAdSize(AdSize.BANNER);
		layoutAd = (LinearLayout)findViewById(R.id.mainActivityAd);
		layoutAd.addView(adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		final Button tab0 = (Button)findViewById(R.id.articleTab0);
		tab0.setSelected(true);
		selectedTabIndex = 0;
		
		// ジェスチャーの登録
		gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2,
					float velocityX, float velocityY) {
				Log.d("Gesture", "X="+velocityX + ", Y=" + velocityY);
				
				return super.onFling(e1, e2, velocityX, velocityY);
			}
			
			@Override
			public boolean onDoubleTapEvent(MotionEvent e) {
				Log.d("Gesture", "Double Tap Event");
				
				return super.onDoubleTapEvent(e);
			}
		});
		
		final Button tab1 = (Button)findViewById(R.id.articleTab1);
		final Button tab2 = (Button)findViewById(R.id.articleTab2);
//		final Button tab3 = (Button)findViewById(R.id.articleTab3);
		final View selectedView = (View)findViewById(R.id.selectedTabColorView);
		
		tab0.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tab0.setSelected(true);
				tab1.setSelected(false);
				tab2.setSelected(false);
//				tab3.setSelected(false);
				selectedView.setBackgroundColor(Color.parseColor("#E95849"));
				
				if (selectedTabIndex != 0)
				{
					selectedTabIndex = 0;
					reloadListView();
				}
			}
		});
		tab1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tab0.setSelected(false);
				tab1.setSelected(true);
				tab2.setSelected(false);
//				tab3.setSelected(false);
				selectedView.setBackgroundColor(Color.parseColor("#43A0EE"));

				if (selectedTabIndex != 1)
				{
					selectedTabIndex = 1;
					reloadListView();
				}
			}
		});
		tab2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tab0.setSelected(false);
				tab1.setSelected(false);
				tab2.setSelected(true);
//				tab3.setSelected(false);
				selectedView.setBackgroundColor(Color.parseColor("#3FD07D"));

				if (selectedTabIndex != 2)
				{
					selectedTabIndex = 2;
					reloadListView();
				}
			}
		});
//		tab3.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				tab0.setSelected(false);
//				tab1.setSelected(false);
//				tab2.setSelected(false);
//				tab3.setSelected(true);
//				selectedView.setBackgroundColor(Color.parseColor("#B83BC3"));
//
//				if (selectedTabIndex != 3)
//				{
//					selectedTabIndex = 3;
////					reloadListView();
//				}
//			}
//		});

//		ImageButton settingBtn = (ImageButton)findViewById(R.id.settingBtn);
//		settingBtn.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				
//				ActivityCompat.invalidateOptionsMenu(MainActivity.this);
//				Log.d("click", "click");
//			}
//		});
		
		ListView listView = (ListView)findViewById(R.id.articleList);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				HashMap<String, Object> articleData = getArticleData(arg2);
				
				Intent intent = new Intent(MainActivity.this, com.flickfrog.senninnews.ArticleDetailActivity.class);
				intent.putExtra("url", articleData.get("url").toString());
				
				prevURL = articleData.get("url").toString();

				startActivityForResult(intent, REQUEST_CODE_ARTICLEDETAIL);
			}
		});

		
		// push通知の設定
		IKonectNotificationsCallback callback = new NotificationsCallback();
		KonectNotificationsAPI.initialize(this, callback);
		KonectNotificationsAPI.setupNotifications();
		
		
		reloadFromSQLite();
		updateArticle();
	}
	
	private HashMap<String, Object> getArticleData(int index) {
		ArrayList<HashMap<String,Object>> articleDatasOfType = rowDataDictionary.get(articleTypes[selectedTabIndex]);

		if (articleDatasOfType.size() <= index)
		{
			return null;
		}
		else
		{
			return articleDatasOfType.get(index);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		gestureDetector.onTouchEvent(event);
		
		return super.onTouchEvent(event);
	}
	
	private void reloadListView() {
		if (rowDataDictionary == null) {
			// まだデータの準備ができていない
			return;
		}
		
		ArrayList<ArticleItem> data = new ArrayList<ArticleItem>();
		String type = articleTypes[selectedTabIndex];
		ArrayList<HashMap<String,Object>> articleDatasOfType = rowDataDictionary.get(type);
		
		for (int i=0; i<articleDatasOfType.size(); i++) {
			HashMap<String, Object> articleData = articleDatasOfType.get(i);
			
			ArticleItem item = new ArticleItem();
			item.setUrl(articleData.get("url").toString());
			item.setTitle(articleData.get("title").toString());
			item.setThumbnail(articleData.get("thumbnail").toString());
			item.setNew(articleData.get("new").toString().equals("1"));
			item.setPercent(Integer.parseInt(articleData.get("percent").toString()));
			
			data.add(item);
		}
		
		
		ArticleListAdapter adapter = new ArticleListAdapter(this, data, R.layout.list_article);
		ListView list = (ListView)findViewById(R.id.articleList);
		list.setAdapter(adapter);
	}

	private void updateArticle() {
		UpdateArticleAsyncTask updateTask = new UpdateArticleAsyncTask(this, new AsyncTaskCallback() {
			@Override
//			public void postExecute(boolean needUpdate) {
			public void postExecute(String result) {
				boolean needUpdate = new SQLiteManager(MainActivity.this).updateArticles(result);
				if (needUpdate)
				{
					reloadFromSQLite();
				}
			}
		});
		updateTask.execute();
	}

	private void reloadFromSQLite() {
		// 格納変数の初期化
		urlLists = new ArrayList<String>();
		rowDataDictionary = new HashMap<String, ArrayList<HashMap<String, Object>>>();
		for (int i=0; i<articleTypes.length; i++) {
			ArrayList<HashMap<String, Object>> articleDatasOfType = new ArrayList<HashMap<String, Object>>();  
			rowDataDictionary.put(articleTypes[i], articleDatasOfType);
		}

		ArrayList<HashMap<String, Object>> articleTitles = new SQLiteManager(this).articleTitles();
		for (int i=0; i<articleTitles.size(); i++) {
			setDataAtRowDataDictionary(articleTitles.get(i));
		}

		reloadListView();
	}
	
	// 記事データをtype毎に振り分け
	private void setDataAtRowDataDictionary(HashMap<String, Object> rowData) {
		String url = rowData.get("url").toString();
		String type = rowData.get("type").toString();

	    if (urlLists.indexOf(url) == -1)
	    {
	    	urlLists.add(url);

	    	ArrayList<HashMap<String, Object>> articleDatasOfType = rowDataDictionary.get(type);	        
	    	articleDatasOfType.add(rowData);
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d("onCreateOptionsMenu", "onCreateOptionsMenu");
		boolean ret = super.onCreateOptionsMenu(menu);
		menu.add(0, Menu.FIRST, Menu.NONE, "サポート");
		menu.add(0, Menu.FIRST+1, Menu.NONE, "さらに濃い情報を！");
		return ret;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean ret = true;
		switch (item.getItemId()) {
			default:
				ret = super.onOptionsItemSelected(item);
				break;
			case 1:
				
				PackageInfo packageInfo;
				String versionName = "Not found";
				try {
					packageInfo = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);
					versionName = packageInfo.versionName;
				} catch (NameNotFoundException e) {
					e.printStackTrace();
				}
				
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_SENDTO);
				intent.setData(Uri.parse("mailto:" + Config.SUPPORT_MAIL));
				intent.putExtra(Intent.EXTRA_SUBJECT, "仙人News お問い合わせ");
				intent.putExtra(Intent.EXTRA_TEXT,
						"\r\n\r\n\r\n--- 以下の情報は問題解決に利用するので削除しないで下さい ---\r\n\r\n" +
						"device: " + Build.MODEL + "\r\n" +
						"Android: " + Build.VERSION.RELEASE + " " + "\r\n" + 
						"appName: 仙人News " + versionName
						);
				//createChooserを使うと選択ダイアログのタイトルを変更する事ができます。
				startActivity(Intent.createChooser(intent,"select"));
				//通常のブラウザ起動です。
				//startActivity(intent);
				
				ret = true;
				break;
			case 2:
				Intent intent2 = new Intent(MainActivity.this, com.flickfrog.senninnews.MailMagazineReg.class);
				startActivity(intent2);
				ret = true;
				break;
		}
		return ret;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		updateItemPercent(prevURL);
	}
	
	
	private void updateItemPercent(String url) {
		if (url != null) {
			ListView listView = (ListView)findViewById(R.id.articleList);
			ArticleListAdapter adapter = (ArticleListAdapter) listView.getAdapter();
			
			HashMap<String, Object> articleData = new SQLiteManager(this).getArticleData(url);
			for (int i=0; i<adapter.getCount(); i++) {
				ArticleItem item = (ArticleItem)adapter.getItem(i);
				if (item.getUrl().equals(url)) {
					item.setPercent(Integer.parseInt(articleData.get("percent").toString()));
					item.setNew(false);
					
					listView.invalidateViews();
					break;
				}
			}
			
			ArrayList<HashMap<String,Object>> articleDatasOfType = rowDataDictionary.get(articleTypes[selectedTabIndex]);
			for (int i=0; i<articleDatasOfType.size(); i++) {
				HashMap<String, Object> articleDataHash = articleDatasOfType.get(i);
				if (articleDataHash.get("url").equals(url)) {
					articleDataHash.put("percent", articleData.get("percent"));
					articleDataHash.put("new", 0);
					
					break;
				}
			}
		}
	}
	
	@Override
	protected void onPause() {
		adView.pause();
		super.onPause();
	}
	@Override
	protected void onResume() {
		super.onResume();
		adView.resume();
		
		KonectNotificationsAPI.processIntent(getIntent());
	}
	
	@Override
	protected void onDestroy() {
		adView.destroy();
		super.onDestroy();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}
}
