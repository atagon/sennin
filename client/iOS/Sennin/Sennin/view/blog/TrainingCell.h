//
//  TrainingCell.h
//  Sennin
//
//  Created by abt on 2014/08/23.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainingCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *iconNew;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSString *imagePath;
@property (strong, nonatomic) IBOutlet UIImageView *articleImageView;
@property (strong, readwrite) NSString *movieKey;

@property (assign, readwrite) UIViewController *parentView;

@end
