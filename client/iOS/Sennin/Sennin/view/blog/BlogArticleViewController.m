//
//  BlogArticleViewController.m
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "BlogArticleViewController.h"
#import "AdView.h"
#import "AFNetworking.h"
#import "HTMLParser.h"
#import "UIColor+WithColorCode.h"
#import "Config.h"
#import "SQLiteManager.h"
#import <Social/Social.h>
#import "WToast.h"
#import "PropertyDirector.h"
#import "NSString+stringByUrlEncoding.h"
#import "BlogListViewController.h"
#import "RelatedArticleCell.h"
#import "JMImageCache.h"
#import "UIImage+imagePath.h"

#define RELATEDVIEW_HEIGHT 145.0


@interface BlogArticleViewController ()

@end

@implementation BlogArticleViewController
{
    IBOutlet UIView *browserMenuView;
    IBOutlet UIButton *historyBackBtn;
    IBOutlet UIButton *historyForwardBtn;

    IBOutlet UIWebView *webView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    BOOL mailFormSended;
    IBOutlet UILabel *persentLabel;
    IBOutlet UIButton *relocateBtn;
    
    NSMutableArray *relatedArticles;
    IBOutlet UICollectionView *relatedListView;
    
    IBOutlet UIView *relatedView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    relocateBtn.alpha = 0.0;
    webView.scrollView.delegate = self;
    
    mailFormSended = NO;
    if (self.url != nil)
    {
        NSString *gaAnaName = [NSString stringWithFormat:@"記事: %@", self.url];
        [[PropertyDirector sharedDirector]sendGoogleAnalitics:gaAnaName];
    }
    
    CGRect frame = browserMenuView.frame;
    frame.origin.y = 524;
    browserMenuView.frame = frame;
    
    browserMenuView.hidden = YES;
    
//    NSString *requestULR = [NSString stringWithFormat:ARTICLE_DETAIL_FROM_PHP, PHP_SERVER_HOST, PHP_SERVER_PORT, self.url];
//    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:requestULR]];
    //NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://usi.moo.jp/kurosemi.html"]];
    
    NSURLRequest* urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [webView loadRequest:urlReq];
    webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, RELATEDVIEW_HEIGHT, 0);
    webView.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, RELATEDVIEW_HEIGHT, 0);
    
    [[PropertyDirector sharedDirector].adView setFrame:CGRectMake(0, APP_HEIGHT-50-44, APP_WIDTH, 50)];
    [self.view addSubview:[PropertyDirector sharedDirector].adView];
    
    
    [self.view addSubview:relatedView];
    [relatedView setFrame:CGRectMake(0, APP_HEIGHT, APP_WIDTH, RELATEDVIEW_HEIGHT)];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (request.URL.host == nil
        || [request.URL.host isEqualToString:@"www.youtube.com"])
    {
        return YES;
    }
    
    NSLog(@"%@", request.URL.host);
    NSLog(@"%@", request.URL.absoluteString);
    
    self.url = request.URL.absoluteString;
    
    [activityIndicator startAnimating];
    
    BOOL isArticle = [[SQLiteManager shardManager]isExistURL:self.url];
    if (isArticle)
    {
        //self.url = [NSString stringWithFormat:@"http://%@%@", request.URL.host, request.URL.path];
        [self analyzeURL];
        
        browserMenuView.hidden = YES;
        
        //        // 既読にする
        //        [[SQLiteManager shardManager]setReadWithURL:self.url];
        // newを消す
        [[SQLiteManager shardManager]setUnNewWithURL:self.url];
        
        return NO;
    }
    else if ([request.URL.host isEqualToString:SENNIN_MAILFORM_HOST])
    {
        if (!mailFormSended)
        {
    //        self.url = [NSString stringWithFormat:@"http://%@%@", request.URL.host, request.URL.path];
            [self analizeForMailForm:request];
            
            browserMenuView.hidden = YES;
        
            return NO;
        } else {
            return YES;
        }
    }
    else if ([request.URL.host isEqualToString:SENNIN_HOST])
    {
        return YES;
        
    } else {
        [activityIndicator stopAnimating];
        
        if ([[UIApplication sharedApplication]canOpenURL:request.URL])
        {
            NSLog(@"%@", request.URL.absoluteString);
            [[UIApplication sharedApplication]openURL:request.URL];
            
        } else {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"" message:@"ページを開くのに失敗しました。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
//        mailFormSended = NO;
//        
//        // facebookやtwitterのiframeの場合は無視
//        if (!([request.URL.host isEqualToString:@"www.facebook.com"] || [request.URL.host isEqualToString:@"platform.twitter.com"]))
//        {
//            browserMenuView.hidden = NO;
//            [self refreshBrowserBtnStatus];
//        }
        return NO;
    }
}


- (void)analyzeURL
{
    // ブログ記事かどうかチェック
    NSDictionary *articleData = [[SQLiteManager shardManager]articleWithURL:self.url];
    if (articleData != nil)
    {
        // ブログ記事
        if (![[articleData objectForKey:@"html"]isKindOfClass:[NSNull class]] && ![[articleData objectForKey:@"html"]isEqualToString:@""])
        {
            NSLog(@"キャッシュ! : %@", self.url);
            
            // キャッシュから記事を表示
            [self parseHTMLAndDispWithHTML:[articleData objectForKey:@"html"]];
            
        } else {
            // PHPサーバーに記事を取りに行く
            NSString* phpServerURL = [NSString stringWithFormat:ARTICLE_DETAIL_FROM_PHP, PHP_SERVER_HOST, PHP_SERVER_PORT, self.url];
            NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:phpServerURL]];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:urlReq];
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSData *htmlData = (NSData*)responseObject;
                NSString *htmlString = [[NSString alloc]initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
                
                
                // キャッシュに登録
                NSString* contents = @"";
                NSError *error = nil;
                HTMLParser *parser = [[HTMLParser alloc]initWithString:htmlString error:&error];
                HTMLNode *node = [parser body];
                NSArray *DIVs = [node findChildTags:@"div"];
                for (int i=0; i< [DIVs count]; i++)
                {
                    HTMLNode *node = [DIVs objectAtIndex:i];
                    if ([[node getAttributeNamed:@"id"] isEqualToString:@"main"])
                    {
                        contents = [node allContents];
                        break;
                    }
                }
                [[SQLiteManager shardManager]registerArticleHTML:htmlString contents:contents url:self.url];
                
                
                [self parseHTMLAndDispWithHTML:htmlString];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"" message:@"記事の取得に失敗しました。\nしばらく待ってからもう一度お試しください。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc]init];
            [queue addOperation:operation];
        }
        
        //        [self sendReadLogRequest:self.url];
        //        webView.userInteractionEnabled = YES;
        
        return;
    }
}


- (void)parseHTMLAndDispWithHTML:(NSString*)htmlString
{
    // 解析開始
    NSError *error = nil;
    HTMLParser *parser = [[HTMLParser alloc]initWithString:htmlString error:&error];
    HTMLNode *node = [parser body];
    
    
    // 関連記事の抽出
    relatedArticles = [[NSMutableArray alloc]init];
    HTMLNode *UL_related = [node findChildOfClass:@"related_post wp_rp"];
    NSArray *LIs_related = [UL_related findChildTags:@"li"];
    for (int j=0; j< [LIs_related count]; j++) {
        HTMLNode *LI_related = [LIs_related objectAtIndex:j];
        HTMLNode *IMAGE_related = [LI_related findChildTag:@"img"];
        HTMLNode *A_related = [LI_related findChildTag:@"a"];
        NSString *title = [IMAGE_related getAttributeNamed:@"alt"];
        NSString *img = [IMAGE_related getAttributeNamed:@"src"];
        NSString *url = [A_related getAttributeNamed:@"href"];
        
        [relatedArticles addObject:@{@"title":title, @"img":img, @"url":url}];
    }
    [relatedListView reloadData];
    
    
    webView.backgroundColor = [UIColor colorWithColorCode:BG_COLOR];
    [webView loadHTMLString:htmlString baseURL: nil];
    [activityIndicator stopAnimating];
}


- (void)analizeForMailForm:(NSURLRequest*)request
{
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSData *htmlData = (NSData*)responseObject;
        NSString *htmlString = [[NSString alloc]initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
        
        // HTMLを改ざんして、スマフォに最適な表示に変える
        NSString *destHTMLParts = @"<style type=\"text/css\">\
        #wrap{padding:0px 0; width:100%; margin:0;}\
        TABLE{width:250px;}\
        HR{width:100%;}\
        TABLE TD {word-break: break-all;}\
        .btn {\
        width: 200px;\
        height: 50px;\
        margin: 0;\
        padding: 5px;\
        background: -webkit-gradient(linear, left top, left bottom, from(#FFAA33), to(#FF8800));\
        border: 1px #F27300 solid;\
        color: #FFF;\
        -webkit-appearance: none;\
        -webkit-border-radius: 10px;\
        -webkit-box-shadow: 0 2px 2px #CCC;\
        text-shadow: 1px 2px 3px #C45C00;\
        }\
        </style>\
        </head>";
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"</head>" withString:destHTMLParts];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<INPUT type=\"button\" class=\"btn\" name=\"bak\" value=\"戻る\" onClick=\"history.back()\">" withString:@""];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/jslib" withString:@"https://55auto.biz/jslib"];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/css/main.css" withString:@"https://55auto.biz/css/main.css"];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"responder.php" withString:MAILMAGAZINE_REGISTER_URL];
        
        // 戻るボタンを消す
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<INPUT type=\"button\" class=\"backbtn\" name=\"bak\" value=\"戻る\" onClick=\"history.back()\">&nbsp;&nbsp;&nbsp;" withString:@""];
        
        
        [webView loadHTMLString:htmlString baseURL: nil];
        mailFormSended = YES;
         //   [webView loadHTMLString:[node rawContents] baseURL:nil];
        [activityIndicator stopAnimating];
        [self parseHTMLAndDispWithHTML:htmlString];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error");
    }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [queue addOperation:operation];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (![scrollView.superview isKindOfClass:[UIWebView class]])
    {
        // UIWebView以外のスクロールはハンドリングしない
        return;
    }
    
    
    int percent = (int)((scrollView.contentOffset.y/(scrollView.contentSize.height-APP_HEIGHT))*100);
    int setPercent = (percent > 100) ? 100 : (percent > 0) ? percent : 0;
    persentLabel.text = [NSString stringWithFormat:@"%d%%", setPercent];
    if (setPercent == 100)
    {
        persentLabel.textColor = [UIColor orangeColor];
    }
    else
    {
        persentLabel.textColor = [UIColor lightGrayColor];
    }
    
    relocateBtn.alpha = (100.0 - scrollView.contentOffset.y)/100;
    

    
    if ([relatedArticles count] == 0)
    {
        // 関連記事がない場合は、関連記事UIを表示させない
        [relatedView setFrame:CGRectMake(0, APP_HEIGHT, APP_WIDTH, RELATEDVIEW_HEIGHT)];
        return;
    }
    float boundPercent = 0.1;
    if (scrollView.contentSize.height-APP_HEIGHT < 10000.0) {
        boundPercent = 0.2;
    }
    float bound = (scrollView.contentSize.height-APP_HEIGHT) * boundPercent;
    if ((scrollView.contentSize.height-APP_HEIGHT)-scrollView.contentOffset.y < bound) {
        float destY = APP_HEIGHT-(bound-((scrollView.contentSize.height-APP_HEIGHT)-scrollView.contentOffset.y));
        if (destY < APP_HEIGHT-(44.0+RELATEDVIEW_HEIGHT)) {
            destY = APP_HEIGHT-(44.0+RELATEDVIEW_HEIGHT);
        }
        [relatedView setFrame:CGRectMake(0, destY, APP_WIDTH, RELATEDVIEW_HEIGHT)];
    } else {
        [relatedView setFrame:CGRectMake(0, APP_HEIGHT, APP_WIDTH, RELATEDVIEW_HEIGHT)];
    }
    
/*
    int boundPercent = 80;
    if (setPercent >= boundPercent) {
        float destY = APP_HEIGHT-((44.0+RELATEDVIEW_HEIGHT)/(100-boundPercent) * setPercent - (44.0+RELATEDVIEW_HEIGHT)*(boundPercent/(100-boundPercent)));
        [relatedView setFrame:CGRectMake(0, destY, APP_WIDTH, RELATEDVIEW_HEIGHT)];
        
        NSLog(@"destY==%f", destY);
    } else {
        [relatedView setFrame:CGRectMake(0, APP_HEIGHT, APP_WIDTH, RELATEDVIEW_HEIGHT)];
    }
*/
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    int percent = (int)((scrollView.contentOffset.y/(scrollView.contentSize.height-APP_HEIGHT))*100);
    [[SQLiteManager shardManager]updateReadPercentWithURL:self.url percent:percent];
}


- (IBAction)relocateScroll:(id)sender {
    [relocateBtn removeFromSuperview];

    NSDictionary *articleData = [[SQLiteManager shardManager]articleWithURL:self.url];
    int percent = [[articleData objectForKey:@"percent"]intValue];
    float contentHeight = webView.scrollView.contentSize.height;
    [webView.scrollView setContentOffset:CGPointMake(0, (contentHeight-APP_HEIGHT) * 0.01 * percent) animated:YES];
    
    [WToast showWithText:@"前回読んだ位置を表示しました。"];
}


- (IBAction)tweet:(id)sender {
    NSString *baseURLString = @"http://api.bit.ly/v3/shorten?&login=%@&apiKey=%@&longUrl=%@&format=txt";
    NSString *encodedLongURL = [self.url stringByUrlEncoding];
    
    NSString *urlString = [NSString stringWithFormat:baseURLString, BITLY_LOGIN_ID, BITLY_APIKEY, encodedLongURL];
    NSURL *url = [NSURL URLWithString:urlString];

    // bit.lyのAPIをコールする
    NSString *shortenURL = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];

    
    //利用可能チェック
    NSString* serviceType = SLServiceTypeTwitter;
    //    if ([SLComposeViewController isAvailableForServiceType:serviceType]) {
    SLComposeViewController *composeCtl = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    
    NSString *tweetText = [NSString stringWithFormat:@"%@\n%@\npowerd by bit.ly/sennin-news",self.blogTitle, shortenURL];
    [composeCtl setInitialText:tweetText];
    [composeCtl setCompletionHandler:^(SLComposeViewControllerResult result) {
        if (result == SLComposeViewControllerResultDone) {
            
//            [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"CanvasMenu" action:@"post" label:@"twitter"];
            
            //投稿成功時の処理
            [WToast showWithText:@"投稿しました。"];
        }
        else if (result == SLComposeViewControllerResultCancelled)
        {
//            [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"CanvasMenu" action:@"cancel" label:@"twitter"];
            
            //投稿キャンセル時の処理
            [WToast showWithText:@"投稿をキャンセルしました。"];
        }
    }];
    
    [self presentViewController:composeCtl animated:YES completion:^{
        //
    }];
    //    }
}


- (IBAction)historyBack:(id)sender {
    [webView goBack];
    
    if ([webView canGoBack])
    {
        [self refreshBrowserBtnStatus];
    }
    else
    {
        NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
        [webView loadRequest:urlReq];
    }


}
- (IBAction)historyForward:(id)sender {
    [webView goForward];
    [self refreshBrowserBtnStatus];
}
- (IBAction)reload:(id)sender {
    [webView reload];
}

- (void)refreshBrowserBtnStatus
{
    if ([webView canGoForward])
    {
        historyForwardBtn.enabled = YES;
    }
    else
    {
        historyForwardBtn.enabled = NO;
    }
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [activityIndicator stopAnimating];
    
    NSDictionary *articleData = [[SQLiteManager shardManager]articleWithURL:self.url];
    if ([[articleData objectForKey:@"percent"]intValue] != 0
        && [[articleData objectForKey:@"percent"]intValue] != 100)
    {
        [UIView animateWithDuration:0.3 animations:^{
            relocateBtn.alpha = 1.0;
        }];
    }
    else
    {
        [relocateBtn removeFromSuperview];
        relocateBtn = nil;
    }
}








- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    webView.scrollView.alwaysBounceHorizontal = NO;
   
    
    NSDictionary *articleData = [[SQLiteManager shardManager]articleWithURL:self.url];
    NSString *url = [NSString stringWithFormat:@"%@/", self.url];
    
    NSMutableArray *array = [self.blogListView.rowDataDictionary objectForKey:[articleData objectForKey:@"type"]];
    for (int i=0; i<[array count]; i++)
    {
        NSMutableDictionary *dic = [array objectAtIndex:i];
        if ([[dic objectForKey:@"url"]isEqualToString:url])
        {
            [dic setObject:[articleData objectForKey:@"percent"] forKey:@"percent"];
            break;
        }
    }
    
    
    self.url = nil;
    self.blogTitle = nil;
}




//- (void)sendReadLogRequest:(NSString*)url
//{
//    NSString *urlStr = [NSString stringWithFormat:ARTICLE_WATCH_LOG_GAE, GAE_HOST, [PropertyDirector sharedDirector].uuid, url];
//    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:urlReq];
//    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
//    [queue addOperation:operation];
//}







// 関連記事
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [relatedArticles count];
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RelatedArticleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"relatedArticleCell" forIndexPath:indexPath];
    
    NSDictionary *articleData = [relatedArticles objectAtIndex:indexPath.row];
    UIImage *image = [[JMImageCache sharedCache]cachedImageForKey:[articleData objectForKey:@"img"]];
    if (image != nil)
    {
        cell.imageView.image = image;
    }
    else
    {
        [cell.activitiIndicator startAnimating];

        NSURL *nsURL = [NSURL URLWithString:[articleData objectForKey:@"img"]];
        [[JMImageCache sharedCache]imageForURL:nsURL key:[articleData objectForKey:@"img"] completionBlock:^(UIImage *image) {
            cell.imageView.image = image;
            [cell.activitiIndicator stopAnimating];
        }];
    }
    cell.titleLabel.layer.cornerRadius = 3;
    cell.titleLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.2];
    cell.titleLabel.text = [articleData objectForKey:@"title"];


    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = [NSString stringWithFormat:@"記事から :%@",[[[SQLiteManager shardManager]articleWithURL:self.url]objectForKey:@"title"]];
    NSString *toTitle = [NSString stringWithFormat:@"記事へ :%@",[[[SQLiteManager shardManager]articleWithURL:[[relatedArticles objectAtIndex:indexPath.row]objectForKey:@"url"]]objectForKey:@"title"]];
    [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"関連リンク" action:toTitle label:title];
    
    
    [activityIndicator startAnimating];
    self.url = [[relatedArticles objectAtIndex:indexPath.row]objectForKey:@"url"];

    [self analyzeURL];

//    webView.userInteractionEnabled = NO;

    [UIView animateWithDuration:0.3 animations:^{
         [relatedView setFrame:CGRectMake(0, APP_HEIGHT, APP_WIDTH, RELATEDVIEW_HEIGHT)];
     } completion:^(BOOL finished) {
         [relatedListView setContentOffset:CGPointZero];
     }];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
}


//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    NSDictionary *articleData = [[SQLiteManager shardManager]articleWithURL:self.url];
//    NSString *url = [NSString stringWithFormat:@"%@/", self.url];
//    
//    BlogListViewController *listView = [segue destinationViewController];
//    NSMutableArray *array = [listView.rowDataDictionary objectForKey:[articleData objectForKey:@"type"]];
//    for (int i=0; i<[array count]; i++)
//    {
//        NSMutableDictionary *dic = [array objectAtIndex:i];
//        if ([[dic objectForKey:@"url"]isEqualToString:url])
//        {
//            [dic setObject:[articleData objectForKey:@"percent"] forKey:@"percent"];
//            break;
//        }
//    }
//    
//    
//    self.url = nil;
//    self.blogTitle = nil;
//}


@end
