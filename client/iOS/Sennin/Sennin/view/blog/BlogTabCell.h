//
//  BlogTabCell.h
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogTabCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *tabBtn;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
