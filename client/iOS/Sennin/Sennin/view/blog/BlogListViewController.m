//
//  BlogListViewController.m
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "BlogListViewController.h"
#import "Config.h"
#import "BlogTabCell.h"
#import "UIImage+imagePath.h"
#import "UIColor+WithColorCode.h"
#import "AdView.h"
#import "ArticleTableCellTableViewCell.h"
#import "TrainingCell.h"
#import "BlogArticleViewController.h"
#import "WeightTrainingViewController.h"
#import "AFNetworking.h"
#import "HTMLParser.h"
#import "PropertyDirector.h"
#import "SQLiteManager.h"
#import "UIImage+Custom.h"
#import "Notification.h"
#import "JMImageCache.h"

#define TOOL_TAB_INDEX 3

@interface BlogListViewController ()

@end

@implementation BlogListViewController
{
    BOOL isNotFirstLaunch;
    
    IBOutlet UITableView *searchListTable;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UIButton *settingBtn;
    IBOutlet UITableView *tableView;
    IBOutlet UICollectionView *tabCollectionView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIView *selectedTabView;
    NSArray *dataSource;
    NSArray *dataSourceKey;
    NSMutableArray *urlLists; // rowDataDictionaryに登録済みか確認用

    NSArray *searchDatasource;
    NSInteger selectedTabIndex;
    
    UIButton *flickGuideBtn;
    
    enum CHANGE_TAB_TYPE {
        kChangeTabLeft,
        kChangeTabRight,
    } changeTabType;
    CGPoint startPanTableOffset;
    
    
    NSArray *toolIcons;
    NSArray *toolMovieKeys;
    NSArray *toolLabels;
    
    UIImageView *tab0NewIcon;
    UIImageView *tab1NewIcon;
    UIImageView *tab2NewIcon;
    UIImageView *tab3NewIcon;
}



// 検索
- (void)searchBarTextDidBeginEditing:(UISearchBar *)_searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    [self.view addSubview:searchListTable];    
}

- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchText
{
    searchDatasource = [[SQLiteManager shardManager]searchTodoWithKeyword:searchText];
    [searchListTable reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar
{
    [_searchBar setText:@""];
    [_searchBar setShowsCancelButton:NO animated:YES];
    [_searchBar resignFirstResponder];
    
    [searchListTable removeFromSuperview];
    searchDatasource = nil;
    [searchListTable reloadData];
}


- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}

- (void)reload
{
    [self reload:YES];
}

- (void)reload:(BOOL)positionReset
{
    [tableView reloadData];
    
    if (positionReset)
    {
        [tableView setContentOffset:CGPointMake(0, 44)];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"記事一覧"];

    
    [searchListTable removeFromSuperview];
//    searchListTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 22+44+44, APP_WIDTH, APP_HEIGHT-(22+44+44-50))];
//    searchListTable.delegate = self;
//    searchListTable.dataSource = self;
//    searchListTable.tag = 999;
    searchDatasource = [NSArray array];
    
//    tableView.backgroundColor = [UIColor colorWithColorCode:BG_COLOR];

    selectedTabView.backgroundColor = [UIColor colorWithColorCode:[[TAB_COLORS objectAtIndex:0]intValue]];
    dataSource = @[@"生活", @"運動", @"食事", @"ツール"];
    dataSourceKey = @[@"life", @"activity", @"food", @"record"];
//    toolIcons = @[@"tooliconWeightTraining.png", @"tooliconScale.png"];
    toolIcons = @[@"tooliconWeightTraining.png"];
    toolMovieKeys = @[@"cK5QM4kcA74"];
    toolLabels = @[@"ウェイトトレーニングの管理表", @"身体測定の管理表"];

    
    // タイトルリストコンテナの初期化
//    [self initDataSources];
//    rowDataDictionary = [[NSMutableDictionary alloc]init];
//    for (int i=0; i<[dataSourceKey count]; i++)
//    {
//        NSMutableArray *titleDatas = [NSMutableArray array];
//        [rowDataDictionary setObject:titleDatas forKey:[dataSourceKey objectAtIndex:i]];
//    }
//    urlLists = [[NSMutableArray alloc]init];
    
//    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)];
//    [panGesture setDelaysTouchesBegan:YES];
//    [panGesture setDelaysTouchesEnded:YES];
//    [panGesture setCancelsTouchesInView:YES];
//    [tableView addGestureRecognizer:panGesture];
//
    UISwipeGestureRecognizer *swipeGestureLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGesture:)];
    [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeGestureLeft setCancelsTouchesInView:YES];
    [swipeGestureLeft setDelaysTouchesBegan:YES];
    [swipeGestureLeft setDelaysTouchesEnded:YES];
    [tableView addGestureRecognizer:swipeGestureLeft];
    
    UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGesture:)];
    [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [swipeGestureRight setCancelsTouchesInView:YES];
    [swipeGestureRight setDelaysTouchesBegan:YES];
    [swipeGestureRight setDelaysTouchesEnded:YES];
    [tableView addGestureRecognizer:swipeGestureRight];
    
    
//    tableView.blogListTableDelegate = self;

    

    // キャッシュから記事一覧を取得する
    [self reloadFromSQLite];
//    isNotFirstLaunch = NO;
//    NSArray* titles = [[SQLiteManager shardManager]articleTitles];
//    if ([titles count] > 0)
//    {
//        isNotFirstLaunch = YES;
//        for (int i=0; i<[titles count]; i++) {
//            NSString *type = [[titles objectAtIndex:i]objectForKey:@"type"];
//            NSString *title = [[titles objectAtIndex:i]objectForKey:@"title"];
//            NSString *url = [[titles objectAtIndex:i]objectForKey:@"url"];
//            [self setDataAtRowDataDictionaryWithType:type title:title url:url append:NO];
//        }
//        // ロード完了
//        [activityIndicator stopAnimating];
//        [self reload];
//    }
    
    
    [self updateArticle:nil];

    [PropertyDirector sharedDirector].adView = [[AdView alloc]initWithFrame:CGRectMake(0, APP_HEIGHT-50, APP_WIDTH, 50) parent:self.navigationController];
    [[PropertyDirector sharedDirector].adView updateAd];
    [self.view addSubview:[PropertyDirector sharedDirector].adView];
    
    
    if (![PropertyDirector sharedDirector].isNotFirstLaunch) {
        [PropertyDirector sharedDirector].isNotFirstLaunch = YES;
        
        UIImage *flickGuideBtnImage = [UIImage imagePath:@"flickGuide.png"];
        flickGuideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [flickGuideBtn setFrame:CGRectMake(0, 0, flickGuideBtnImage.size.width, flickGuideBtnImage.size.height)];
        [flickGuideBtn setImage:flickGuideBtnImage forState:UIControlStateNormal];
        [flickGuideBtn addTarget:self action:@selector(closeGuide:) forControlEvents:UIControlEventTouchUpInside];
        [[[UIApplication sharedApplication].windows objectAtIndex:0] addSubview:flickGuideBtn];
        
        self.view.userInteractionEnabled = NO;
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateArticle:) name:UPDATE_ARTICLE object:nil];
    
    
    // 初期表示の処理は、タブの生成完了時に行う
//    [self changeTab:[PropertyDirector sharedDirector].lastSelectedTabIndex];
}


- (void)closeGuide:(id)sender
{
    [flickGuideBtn removeFromSuperview];
    flickGuideBtn = nil;
    
    self.view.userInteractionEnabled = YES;
}


- (void)initDataSources
{
    // タイトルリストコンテナの初期化
    self.rowDataDictionary = [[NSMutableDictionary alloc]init];
    for (int i=0; i<[dataSourceKey count]; i++)
    {
        NSMutableArray *titleDatas = [NSMutableArray array];
        [self.rowDataDictionary setObject:titleDatas forKey:[dataSourceKey objectAtIndex:i]];
    }
    urlLists = [[NSMutableArray alloc]init];
}

- (void)reloadFromSQLite
{
    // キャッシュから記事一覧を取得する
    isNotFirstLaunch = NO;
    NSArray* titles = [[SQLiteManager shardManager]articleTitles];
    if ([titles count] > 0)
    {
        [self initDataSources];
        isNotFirstLaunch = YES;
        for (int i=0; i<[titles count]; i++) {
            NSString *type = [[titles objectAtIndex:i]objectForKey:@"type"];
            NSString *title = [[titles objectAtIndex:i]objectForKey:@"title"];
            NSString *url = [[titles objectAtIndex:i]objectForKey:@"url"];
            int percent = [[[titles objectAtIndex:i]objectForKey:@"percent"]intValue];
//            BOOL read = ([[[titles objectAtIndex:i]objectForKey:@"read"]intValue] == 1);
            
            [self setDataAtRowDataDictionaryWithType:type title:title url:url percent:percent append:NO];
        }
        // ロード完了
        [activityIndicator stopAnimating];
        [self reload];
        //        [tableView reloadData];
    }
}

- (void)updateArticle:(NSNotification*)notification
{
    // サーバーから記事リストを取得する
    NSNumber *lastdate = [[SQLiteManager shardManager]getlastDate];
    NSString *requestULR = [NSString stringWithFormat:ARTICLE_LIST_FROM_PHP, PHP_SERVER_HOST, PHP_SERVER_PORT, lastdate];
    NSURLRequest *listReqURL = [NSURLRequest requestWithURL:[NSURL URLWithString:requestULR]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:listReqURL];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error) {
            NSLog(@"fail");
        } else {
            NSArray *articleList = [json objectForKey:@"list"];
            if ([articleList count] > 0) {
                // 新作があった場合は、過去のnewをリセットする
                [[SQLiteManager shardManager]resetNewArticle];

                // リスト情報のキャッシュ登録
                for (NSDictionary *titleData in articleList)
                {
                    NSString *url = [titleData objectForKey:@"url"];
                    NSString *title = [titleData objectForKey:@"title"];
                    NSString *thumbnail = [titleData objectForKey:@"thumbnail"];
                    NSString *type = [titleData objectForKey:@"type"];
                    NSNumber *createTime = [titleData objectForKey:@"create_time"];
                    NSNumber *updateTime = [titleData objectForKey:@"update_time"];
                    int isDeleted = 0;
                    if (![[titleData objectForKey:@"valid"]boolValue]) {
                        isDeleted = 1;
                    }
                    [[SQLiteManager shardManager]insertArticleTitleWithURL:url title:title thumbnail:thumbnail type:type isNew:isNotFirstLaunch createTime:createTime updateTime:updateTime isDeleted:isDeleted];
                    
                    
                    if (isNotFirstLaunch)
                    {
                        if ([[SQLiteManager shardManager]articleWithURL:url] != nil) {
                            // タブにnewを出す
                            UIImageView *newIconImageView;
                            switch ([dataSourceKey indexOfObject:type]) {
                                case 0:
                                {
                                    if (tab0NewIcon == nil && selectedTabIndex != [dataSourceKey indexOfObject:type]) {
                                        tab0NewIcon = [[UIImageView alloc]init];
                                        newIconImageView = tab0NewIcon;
                                    }
                                }
                                    break;
                                case 1:
                                    if (tab1NewIcon == nil && selectedTabIndex != [dataSourceKey indexOfObject:type]) {
                                        tab1NewIcon = [[UIImageView alloc]init];
                                        newIconImageView = tab1NewIcon;
                                    }
                                    break;
                                case 2:
                                    if (tab2NewIcon == nil && selectedTabIndex != [dataSourceKey indexOfObject:type]) {
                                        tab2NewIcon = [[UIImageView alloc]init];
                                        newIconImageView = tab2NewIcon;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            
                            if (newIconImageView != nil) {
                                UIImage *newIconImage = [UIImage imagePath:@"tabNewIcon.png"];
                                [newIconImageView setImage:newIconImage];
                                int detY = 10;
                                if (selectedTabIndex == [dataSourceKey indexOfObject:type])
                                {
                                    detY = 0;
                                }
                                [newIconImageView setFrame:CGRectMake([dataSourceKey indexOfObject:type]*80, detY, newIconImage.size.width, newIconImage.size.height)];
                                [tabCollectionView addSubview:newIconImageView];
                            }
                        }
                    }
                }
            }
        }
        
        // ロード完了
        [activityIndicator stopAnimating];

        [self reloadFromSQLite];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"fail");
    }];
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [queue addOperation:operation];
    
    
    
//    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:ARTICLE_LIST_URL]];
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:urlReq];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSData *htmlData = (NSData*)responseObject;
//        NSString *htmlString = [[NSString alloc]initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
//        
//        // 解析開始
//        existNewArticle = NO;
//        
//        NSError *error = nil;
//        HTMLParser *parser = [[HTMLParser alloc]initWithString:htmlString error:&error];
//        HTMLNode *node = [parser body];
//        NSArray *LIs = [node findChildTags:@"li"];
//        
//        for (int i=0; i< [LIs count]; i++)
//        {
//            HTMLNode *node = [LIs objectAtIndex:i];
//            if ([[node getAttributeNamed:@"class"] isEqualToString:@"cat-item cat-item-4"])
//            {
//                // 直下がAタグのものが生活項目
//                if ([[node.firstChild tagName]isEqualToString:@"a"])
//                {
//                    // 生活
//                    [self stockListData:node type:@"life"];
//                }
//            }
//            else if ([[node getAttributeNamed:@"class"] isEqualToString:@"cat-item cat-item-2"])
//            {
//                // 運動
//                [self stockListData:node type:@"activity"];
//            }
//            else if ([[node getAttributeNamed:@"class"] isEqualToString:@"cat-item cat-item-3"])
//            {
//                // 食事
//                [self stockListData:node type:@"food"];
//            }
//        }
//        
//        // ロード完了
//        [activityIndicator stopAnimating];
//        
//        // 新作があった場合は、過去のnewをリセットする
//        if (existNewArticle)
//        {
//            [[SQLiteManager shardManager]resetNewArticle];
//        }
//        
//        // リスト情報のキャッシュ登録
//        NSArray* keys = [rowDataDictionary allKeys];
//        for (int i=0; i<[keys count]; i++)
//        {
//            NSArray *titles = [rowDataDictionary objectForKey:[keys objectAtIndex:i]];
//            for (int j=0; j<[titles count]; j++)
//            {
//                NSDictionary *titleData = [titles objectAtIndex:j];
//                NSString *title = [titleData objectForKey:@"title"];
//                NSString *url = [titleData objectForKey:@"url"];
//                [[SQLiteManager shardManager]insertArticleTitleWithURL:url title:title type:[keys objectAtIndex:i] isNew:isNotFirstLaunch];
//            }
//        }
//        
//        [self reload:NO];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"error");
//    }];
//    
//    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
//    [queue addOperation:operation];
}


/**** サーバー側にロジックを移したので、コメントアウト
- (void)stockListData:(HTMLNode*)node type:(NSString*)type
{
//    if ([rowDataDictionary objectForKey:type] != nil)
//    {
//        return;
//    }
    
//    NSMutableArray *array = [rowDataDictionary objectForKey:type];

    if ([[node findChildTags:@"ul"]count] == 0)
    {
        // ulタグを含まないものはブログ下部にあるナゾリンクなので無視
        return;
    }
    
    HTMLNode *UL = [[node findChildTags:@"ul"]objectAtIndex:0];
    NSArray *As = [UL findChildTags:@"a"];
    for (int j=0; j<[As count]; j++)
    {
        HTMLNode *A = [As objectAtIndex:j];
        NSString *title = [A getAttributeNamed:@"title"];
        NSString *url = [A getAttributeNamed:@"href"];

//        [array addObject:@{@"title":title, @"url":url}];
  
        [self setDataAtRowDataDictionaryWithType:type title:title url:url append:isNotFirstLaunch];
//        NSLog(@"%@ : %@::%@", type, title, url);
    }
//    [rowDataDictionary setObject:array forKey:type];
}
**/

- (void)setDataAtRowDataDictionaryWithType:(NSString*)type title:(NSString*)title url:(NSString*)url percent:(int)percent append:(BOOL)append
{
    if ([urlLists indexOfObject:url] == NSNotFound)
    {
        [urlLists addObject:url];

        NSMutableArray *array = [self.rowDataDictionary objectForKey:type];
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    title, @"title",
                                    url, @"url",
                                    @(percent), @"percent",
                                    nil];
        
        if (append)
        {
            [array insertObject:dic atIndex:0];
        }
        else
        {
            [array addObject:dic];
        }
        
//        existNewArticle = YES;
    }
}


//- (void)blogListTableSlideLeft:(float)diff touches:(NSSet *)touches
//{
//    tableView.scrollEnabled = NO;
//    NSLog(@"left %f", diff);
//    
//    CGPoint location = [[touches anyObject]locationInView:self.view];
//    CGRect frame = tableViewForScroll.frame;
//    frame.origin.x = location.x-APP_WIDTH;
//    tableViewForScroll.frame = frame;
//}
//- (void)blogListTableSlideRight:(float)diff touches:(NSSet *)touches
//{
//    tableView.scrollEnabled = NO;
//    
//    CGRect frame = tableView.frame;
//    frame.origin.x = diff;
//    tableView.frame = frame;
//    NSLog(@"right %f", diff);
//}
//- (void)blogListTableCompleteSlide
//{
//    tableView.scrollEnabled = YES;
//    NSLog(@"End");
//}


- (void)panGesture:(UIPanGestureRecognizer*)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:self.view];
    CGPoint diffPoint = [gestureRecognizer translationInView:self.view];
    

    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        startPanTableOffset = tableView.contentOffset;

        if (diffPoint.x > 0)
        {
            // 左のタブに移動する
            NSLog(@"左のタブに");
            changeTabType = kChangeTabLeft;
        }
        else
        {
            // 右のタブに移動する
            NSLog(@"右のタブに");
            changeTabType = kChangeTabRight;
        }
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGRect frame = tableView.frame;
        if (changeTabType == kChangeTabLeft && location.x > APP_WIDTH/2)
        {
            NSLog(@"左のタブに切り替える");
            frame.origin.x = APP_WIDTH;
        }
        else if (changeTabType == kChangeTabRight && location.x < APP_WIDTH/2)
        {
            NSLog(@"右のタブに切り替える");
            frame.origin.x = -APP_WIDTH;
        }
        else
        {
            frame.origin.x = 0;
        }
        
        [UIView animateWithDuration:0.2 animations:^{
//            tableView.frame = frame;
        }];

        return;
    }

    [tableView setContentOffset:CGPointMake(0, startPanTableOffset.y - diffPoint.y)];
    
    CGRect frame = tableView.frame;
    if (changeTabType == kChangeTabLeft)
    {
        frame.origin.x = location.x;
    }
    else if (changeTabType == kChangeTabRight)
    {
        frame.origin.x = location.x-APP_WIDTH;
    }
//    tableView.frame = frame;
}

- (void)swipeGesture:(UISwipeGestureRecognizer*)gestureRecognizer
{
    CGSize captureSize = CGSizeMake(APP_WIDTH, APP_HEIGHT);
    UIGraphicsBeginImageContextWithOptions(captureSize, NO, 0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    image = [image partialImageOfRect:CGRectMake(0, 22+39, APP_WIDTH, APP_HEIGHT-(22+39+50))];
//    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
//    CGRect imageFrame = imageView.frame;
//    imageFrame.origin.y = 22+39;
//    imageView.frame = imageFrame;
    
    NSInteger nextTabIndex = selectedTabIndex;
    if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if (selectedTabIndex < [TAB_COLORS count]-1)
        {
            nextTabIndex++;

            UIImageView *imageView = [self getTableScreenShot];
            [self.view addSubview:imageView];

            CGRect imageFrame = imageView.frame;
            imageFrame.origin.x = -APP_WIDTH;
            [UIView animateWithDuration:0.3 animations:^{
                imageView.frame = imageFrame;
            } completion:^(BOOL finished) {
                [imageView removeFromSuperview];
            }];
        }
    }
    else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if (selectedTabIndex > 0)
        {
            nextTabIndex--;

            UIImageView *imageView = [self getTableScreenShot];
            [self.view addSubview:imageView];
            [self.view addSubview:tableView];
            [self.view addSubview:selectedTabView];
            [self.view addSubview:settingBtn];
            [self.view addSubview:[PropertyDirector sharedDirector].adView];
        
            CGRect tableFrame = tableView.frame;
            tableFrame.origin.x = -APP_WIDTH;
            tableView.frame = tableFrame;
            
            CGRect tabViewFrame = selectedTabView.frame;
            tabViewFrame.origin.x = -APP_WIDTH;
            selectedTabView.frame = tabViewFrame;

            tableFrame.origin.x = 0;
            tabViewFrame.origin.x = 0;
            [UIView animateWithDuration:0.3 animations:^{
                tableView.frame = tableFrame;
                selectedTabView.frame = tabViewFrame;
            }completion:^(BOOL finished) {
                [imageView removeFromSuperview];
            }];
        }
    }
    
    [self changeTab:nextTabIndex];
}

- (UIImageView*)getTableScreenShot
{
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    image = [image partialImageOfRect:CGRectMake(0, 22+39, APP_WIDTH, APP_HEIGHT-(22+39+50))];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    CGRect imageFrame = imageView.frame;
    imageFrame.origin.y = 22+39;
    imageView.frame = imageFrame;

    return imageView;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[PropertyDirector sharedDirector].adView setFrame:CGRectMake(0, APP_HEIGHT-50, APP_WIDTH, 50)];
    [self.view addSubview:[PropertyDirector sharedDirector].adView];
    
    // NEW表示を更新するためにリロードをかける
    [tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self changeTab:[PropertyDirector sharedDirector].lastSelectedTabIndex];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 *
 * Table
 *
 */
- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    if (_tableView.tag == 999)
    {
        // 検索
        return [searchDatasource count];
    }
    else
    {
        if (selectedTabIndex == TOOL_TAB_INDEX)
        {
            return [toolIcons count];
        }
        else
        {
            NSString *type = [dataSourceKey objectAtIndex:selectedTabIndex];
            NSArray *titleDatas = [self.rowDataDictionary objectForKey:type];

            return [titleDatas count];
        }
    }
}

- (UITableViewCell*)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_tableView.tag == 999)
    {
        // 検索
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

        NSDictionary *searchData = [searchDatasource objectAtIndex:indexPath.row];
        cell.textLabel.text = [searchData objectForKey:@"title"];
        NSInteger index = [dataSourceKey indexOfObject:[searchData objectForKey:@"type"]];
        cell.detailTextLabel.text = [dataSource objectAtIndex:index];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0];
        cell.detailTextLabel.textColor = [UIColor colorWithColorCode:[[TAB_COLORS objectAtIndex:index]intValue]];
        
        return cell;
    }
    else
    {
        static NSString *TableCellIdentifier = @"articleCell";
        static NSString *ToolCellIdentifier = @"toolCell";
        
        
        
        NSString *type = [dataSourceKey objectAtIndex:selectedTabIndex];
        NSArray *titleDatas = [self.rowDataDictionary objectForKey:type];
        
        
        
        if (selectedTabIndex == TOOL_TAB_INDEX)
        {
            // ツール
            TrainingCell *cell = [_tableView dequeueReusableCellWithIdentifier:ToolCellIdentifier forIndexPath:indexPath];
            cell.titleLabel.text = [toolLabels objectAtIndex:indexPath.row];
            cell.titleLabel.textColor = [UIColor darkGrayColor];
            cell.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
            cell.movieKey = [toolMovieKeys objectAtIndex:indexPath.row];
            cell.articleImageView.image = [UIImage imagePath:[toolIcons objectAtIndex:indexPath.row]];
            cell.iconNew.hidden = YES;
            
            cell.parentView = self;
            
            return cell;
        }
        else
        {
            ArticleTableCellTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:TableCellIdentifier forIndexPath:indexPath];
            cell.titleLabel.textColor = [UIColor darkGrayColor];
            cell.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
            cell.articleImageView.image = nil;

            [cell dispReadIcon:NO];
            
            // 記事
            NSDictionary *articleData = [titleDatas objectAtIndex:indexPath.row];
            if ([[SQLiteManager shardManager]isNewWithURL:[articleData objectForKey:@"url"]])
            {
                cell.iconNew.hidden = NO;
            }
            else
            {
                cell.iconNew.hidden = YES;
            }


            if ([[articleData objectForKey:@"percent"]intValue] >= 100)
            {
                [cell dispReadIcon:YES];
                cell.persentLabel.text = @"";
            }
            else
            {
                [cell dispReadIcon:NO];
                if ([[articleData objectForKey:@"percent"]intValue] != 0)
                {
                    cell.persentLabel.text = [NSString stringWithFormat:@"%@%%", [articleData objectForKey:@"percent"]];
                }
                else
                {
                    cell.persentLabel.text = @"";
                }
            }

            
            UIImage *iconImage;
            NSString *articleTitle = [articleData objectForKey:@"title"];
            if ([[articleData objectForKey:@"title"]rangeOfString:@"【動画】"].location != NSNotFound)
            {
                articleTitle = [articleTitle stringByReplacingOccurrencesOfString:@"【動画】" withString:@""];
                iconImage = [UIImage imagePath:@"iconDouga.png"];
            }
            else if ([[articleData objectForKey:@"title"]rangeOfString:@"【告知】"].location != NSNotFound)
            {
                articleTitle = [articleTitle stringByReplacingOccurrencesOfString:@"【告知】" withString:@""];
                iconImage = [UIImage imagePath:@"iconKokuchi.png"];
            }
            else if ([[articleData objectForKey:@"title"]rangeOfString:@"【転記】"].location != NSNotFound)
            {
                articleTitle = [articleTitle stringByReplacingOccurrencesOfString:@"【転記】" withString:@""];
                iconImage = [UIImage imagePath:@"iconTenki.png"];
            }

            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:articleTitle];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:5.0f];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [articleTitle length])];
            cell.titleLabel.attributedText = attributedString;
            
            cell.iconImageView.image = iconImage;
            
    //        // 記事の最初の画像を調べてセットする
    //        [[PropertyDirector sharedDirector]setTopImageOfArticle:[articleData objectForKey:@"url"] imageView:cell.articleImageView];

            NSString *url = [articleData objectForKey:@"url"];
            CALayer *layer = [cell.articleImageView layer];
            [layer setMasksToBounds:YES];
//            [layer setBorderWidth:0.5];
//            [layer setBorderColor:[[UIColor lightGrayColor]CGColor]];
            
            UIImage *image = [[JMImageCache sharedCache]cachedImageForKey:url];
            if (image != nil)
            {
    //            NSLog(@"キャッシュ %@", url);
                cell.articleImageView.image = image;
            }
            else
            {
                NSString __block *imagePath = [[SQLiteManager shardManager]thumbnailPathWithURL:url];
                if (imagePath != nil && ![imagePath isEqual:[NSNull null]])
                {
                    image = [UIImage imagePath:imagePath];
                    if (image != nil)
                    {
                        cell.articleImageView.image = image;
                    } else if ([imagePath isEqualToString:BAD_IMAGE_PATH]
                        || ([imagePath rangeOfString:BAD_IMAGE_HOST].location != NSNotFound))
                    {
                        cell.articleImageView.image = [UIImage imageNamed:@"noimage.png"];
                        [[JMImageCache sharedCache]setImage:[UIImage imageNamed:@"noimage.png"] forKey:url];
                    }
                    else
                    {
                        NSURL *nsURL = [NSURL URLWithString:imagePath];
                        [[JMImageCache sharedCache]imageForURL:nsURL key:url completionBlock:^(UIImage *image) {
                            cell.articleImageView.image = image;
                        }];
                    }
                }
            }
            
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *titleDatas;
    if (_tableView.tag == 999)
    {
        // 検索
        titleDatas = searchDatasource;
        
        [self searchBarCancelButtonClicked:searchBar];
        [tableView setContentOffset:CGPointMake(0, 44)];
    }
    else
    {
        NSString *type = [dataSourceKey objectAtIndex:selectedTabIndex];
        titleDatas = [self.rowDataDictionary objectForKey:type];
    }
    
    if (selectedTabIndex == TOOL_TAB_INDEX)
    {
        WeightTrainingViewController *weightTrainingView = [self.storyboard instantiateViewControllerWithIdentifier:@"weightTrainingView"];

        [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"ツール" action:@"ツール" label:[toolLabels objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:weightTrainingView animated:YES];
    }
    else
    {
        BlogArticleViewController *articleView = [self.storyboard instantiateViewControllerWithIdentifier:@"blogArticleView"];
        articleView.url = [[titleDatas objectAtIndex:indexPath.row]objectForKey:@"url"];
        articleView.blogTitle = [[titleDatas objectAtIndex:indexPath.row]objectForKey:@"title"];
        articleView.blogListView = self;
        
        // 既読にする
//        NSMutableDictionary *titleData = [titleDatas objectAtIndex:indexPath.row];
//        if (![[titleData objectForKey:@"read"]boolValue])
//        {
//            [titleData setObject:@(YES) forKey:@"read"];
//            [tableView reloadData];
//        }
        
        
        [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"記事" action:@"閲覧" label:[[titleDatas objectAtIndex:indexPath.row]objectForKey:@"title"]];
        
        [self.navigationController pushViewController:articleView animated:YES];
    }
}


/*
 *
 * TAB
 *
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"blogCell";
    BlogTabCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.tabBtn setImage:[UIImage imagePath:[NSString stringWithFormat:@"tab%ldoff.png", (long)indexPath.row]] forState:UIControlStateNormal];
    [cell.tabBtn setImage:[UIImage imagePath:[NSString stringWithFormat:@"tab%ld.png", (long)indexPath.row]] forState:UIControlStateSelected];
    cell.titleLabel.text = [dataSource objectAtIndex:indexPath.row];

    cell.titleLabel.shadowOffset = CGSizeMake(0, -0.5);
    cell.titleLabel.shadowColor = [UIColor darkGrayColor];
    cell.titleLabel.backgroundColor = [UIColor clearColor];
    cell.titleLabel.textColor = [UIColor whiteColor];

    
    if (indexPath.row == 0)
    {
        // 初期表示は最初のタブが選択状態
        cell.tabBtn.selected = YES;
        CGRect frame = cell.titleLabel.frame;
        frame.origin.y = 0;
        cell.titleLabel.frame = frame;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeTab:indexPath.row];
}

- (void)changeTab:(NSInteger)index
{
    // TabのNew表示を選ばれたtabに合わせて位置を直す
    // tabを選んだらnew消すので、やっぱりいらないや
//    switch (index) {
//        case 0:
//            [tab0NewIcon setFrame:CGRectMake(tab0NewIcon.frame.origin.x, 0, tab0NewIcon.frame.size.width, tab0NewIcon.frame.size.height)];
//            [tab1NewIcon setFrame:CGRectMake(tab1NewIcon.frame.origin.x, 10, tab1NewIcon.frame.size.width, tab1NewIcon.frame.size.height)];
//            [tab2NewIcon setFrame:CGRectMake(tab2NewIcon.frame.origin.x, 10, tab2NewIcon.frame.size.width, tab2NewIcon.frame.size.height)];
//            [tab3NewIcon setFrame:CGRectMake(tab3NewIcon.frame.origin.x, 10, tab3NewIcon.frame.size.width, tab3NewIcon.frame.size.height)];
//            break;
//        case 1:
//            [tab0NewIcon setFrame:CGRectMake(tab0NewIcon.frame.origin.x, 10, tab0NewIcon.frame.size.width, tab0NewIcon.frame.size.height)];
//            [tab1NewIcon setFrame:CGRectMake(tab1NewIcon.frame.origin.x, 0, tab1NewIcon.frame.size.width, tab1NewIcon.frame.size.height)];
//            [tab2NewIcon setFrame:CGRectMake(tab2NewIcon.frame.origin.x, 10, tab2NewIcon.frame.size.width, tab2NewIcon.frame.size.height)];
//            [tab3NewIcon setFrame:CGRectMake(tab3NewIcon.frame.origin.x, 10, tab3NewIcon.frame.size.width, tab3NewIcon.frame.size.height)];
//            break;
//        case 2:
//            [tab0NewIcon setFrame:CGRectMake(tab0NewIcon.frame.origin.x, 10, tab0NewIcon.frame.size.width, tab0NewIcon.frame.size.height)];
//            [tab1NewIcon setFrame:CGRectMake(tab1NewIcon.frame.origin.x, 10, tab1NewIcon.frame.size.width, tab1NewIcon.frame.size.height)];
//            [tab2NewIcon setFrame:CGRectMake(tab2NewIcon.frame.origin.x, 0, tab2NewIcon.frame.size.width, tab2NewIcon.frame.size.height)];
//            [tab3NewIcon setFrame:CGRectMake(tab3NewIcon.frame.origin.x, 10, tab3NewIcon.frame.size.width, tab3NewIcon.frame.size.height)];
//            break;
//        case 3:
//            [tab0NewIcon setFrame:CGRectMake(tab0NewIcon.frame.origin.x, 10, tab0NewIcon.frame.size.width, tab0NewIcon.frame.size.height)];
//            [tab1NewIcon setFrame:CGRectMake(tab1NewIcon.frame.origin.x, 10, tab1NewIcon.frame.size.width, tab1NewIcon.frame.size.height)];
//            [tab2NewIcon setFrame:CGRectMake(tab2NewIcon.frame.origin.x, 10, tab2NewIcon.frame.size.width, tab2NewIcon.frame.size.height)];
//            [tab3NewIcon setFrame:CGRectMake(tab3NewIcon.frame.origin.x, 0, tab3NewIcon.frame.size.width, tab3NewIcon.frame.size.height)];
//            break;
//            
//        default:
//            break;
//    }
    switch (index) {
        case 0:
            [tab0NewIcon removeFromSuperview];
            tab0NewIcon = nil;
            break;
        case 1:
            [tab1NewIcon removeFromSuperview];
            tab1NewIcon = nil;
            break;
        case 2:
            [tab2NewIcon removeFromSuperview];
            tab2NewIcon = nil;
            break;
        case 3:
            [tab3NewIcon removeFromSuperview];
            tab3NewIcon = nil;
            break;
        default:
            break;
    }
    
    
    
    [PropertyDirector sharedDirector].lastSelectedTabIndex = index;
    
    // 検索窓も隠す
    [self searchBarCancelButtonClicked:searchBar];

    
    selectedTabIndex = index;
    selectedTabView.backgroundColor = [UIColor colorWithColorCode:[[TAB_COLORS objectAtIndex:index]intValue]];
    
    
    for (int i=0; i<[tabCollectionView numberOfItemsInSection:0]; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        BlogTabCell *tab = (BlogTabCell*)[tabCollectionView cellForItemAtIndexPath:indexPath];
        CGRect frame = tab.titleLabel.frame;
        if (i == selectedTabIndex)
        {
            tab.tabBtn.selected = YES;
            frame.origin.y = 0;
        }
        else
        {
            tab.tabBtn.selected = NO;
            frame.origin.y = 5;
        }
        tab.titleLabel.frame = frame;
    }
    
    tableView.contentOffset = CGPointZero;
    [self reload];
//    [tableView reloadData];
}



//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier]isEqualToString:@"dispArticle"]
//        || [[segue identifier]isEqualToString:@"dispArticle2"])
//    {
////        articleView = [segue destinationViewController];
////        NSString *type = [dataSourceKey objectAtIndex:selectedTabIndex];
////        NSArray *titleDatas = [rowDataDictionary objectForKey:type];
////        articleView.url = [[titleDatas objectAtIndex:selectedRowIndex]objectForKey:@"url"];
//    }
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedTabIndex == TOOL_TAB_INDEX)
    {
        return 140;
    }
    else
    {
        return 100;
    }
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    self.rowDataDictionary = nil;
}

@end
