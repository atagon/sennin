//
//  BlogArticleViewController.h
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "BlogListViewController.h"

@interface BlogArticleViewController : CommonViewController <UIWebViewDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, readwrite) NSString* url;
@property (strong, readwrite) NSString* blogTitle;
@property (assign, readwrite) BlogListViewController *blogListView;
@end
