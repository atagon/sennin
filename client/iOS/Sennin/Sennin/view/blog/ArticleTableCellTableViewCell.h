//
//  ArticleTableCellTableViewCell.h
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleTableCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *iconNew;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSString *imagePath;
@property (strong, nonatomic) IBOutlet UIImageView *articleImageView;

@property (strong, nonatomic) IBOutlet UIImageView *readedIcon;
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;

@property (strong, nonatomic) IBOutlet UILabel *persentLabel;

- (void)dispReadIcon:(BOOL)disp;


@end
