//
//  ArticleTableCellTableViewCell.m
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "ArticleTableCellTableViewCell.h"
#import "UIImage+imagePath.h"



@implementation ArticleTableCellTableViewCell
{
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)dispReadIcon:(BOOL)disp {
    if (disp) {
        self.readedIcon.alpha = 1.0;
    } else {
        self.readedIcon.alpha = 0.0;
    }
}


- (void)setImagePath:(NSString *)imagePath
{
    UIImage *image = [UIImage imagePath:imagePath];
    self.articleImageView.image = image;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
