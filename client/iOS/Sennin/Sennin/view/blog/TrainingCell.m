//
//  TrainingCell.m
//  Sennin
//
//  Created by abt on 2014/08/23.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "TrainingCell.h"
#import "UIImage+imagePath.h"
#import "XCDYouTubeVideoPlayerViewController.h"
#import "PropertyDirector.h"


@implementation TrainingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}


- (IBAction)dispExplainMovie:(id)sender {
    [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"ツール" action:@"解説動画" label:self.movieKey];

    // Youtube動画を再生する
    XCDYouTubeVideoPlayerViewController *youtubeView = [[XCDYouTubeVideoPlayerViewController alloc]initWithVideoIdentifier:self.movieKey];
    [self.parentView presentMoviePlayerViewControllerAnimated:youtubeView];

}


- (void)setImagePath:(NSString *)imagePath
{
    UIImage *image = [UIImage imagePath:imagePath];
    self.articleImageView.image = image;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
