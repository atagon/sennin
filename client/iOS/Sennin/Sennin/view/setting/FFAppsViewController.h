//
//  FFAppsViewController.h
//  QuickTodo+
//
//  Created by abt on 2014/03/15.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface FFAppsViewController : CommonViewController <UITableViewDelegate, UITableViewDataSource>

@end
