//
//  ConfirmMailMagazineViewController.h
//  Sennin
//
//  Created by abt on 2014/04/14.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmMailMagazineViewController : UIViewController <UIWebViewDelegate>
{
    NSString *_mailName;
    NSString *_mailAdress;
}

@property(assign, readwrite)NSString *mailName;
@property(assign, readwrite)NSString *mailAdress;
@end
