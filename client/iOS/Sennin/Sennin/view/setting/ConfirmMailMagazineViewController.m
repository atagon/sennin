//
//  ConfirmMailMagazineViewController.m
//  Sennin
//
//  Created by abt on 2014/04/14.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "ConfirmMailMagazineViewController.h"
#import "Config.h"
#import "Multipart.h"
#import "HTMLParser.h"
#import "AFNetworking.h"
#import "UIColor+WithColorCode.h"
#import "PropertyDirector.h"

@interface ConfirmMailMagazineViewController ()

@end



@implementation ConfirmMailMagazineViewController
{
    BOOL isRequestEnded;
    IBOutlet UIWebView *webView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

- (IBAction)back:(id)sender {
    if (isRequestEnded)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"%@", request.URL.host);
    if ([request.URL.host isEqualToString:HOST])
    {
        isRequestEnded = YES;
    }
    NSLog(@"%@", [NSString stringWithFormat:@"http://%@%@", request.URL.host, request.URL.path]);

    return YES;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"メルマガ登録確認"];
    isRequestEnded = NO;
    
//    @property(assign, readwrite)NSString *mailName;
//    @property(assign, readwrite)NSString *mailAdress;
//
//    NSString *mcode = @"UTF-8";
//    NSString *tno = @"131";
//    NSString *bak = @"-2";
//    NSString *fld40 = @"RES_TSG_BLG_RSR";
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", nil];
//    [manager POST:MAILMAGAZINE_REGISTER_URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFormData:[mcode dataUsingEncoding:NSUTF8StringEncoding] name:@"mcode"];
//        [formData appendPartWithFormData:[tno dataUsingEncoding:NSUTF8StringEncoding] name:@"tno"];
//        [formData appendPartWithFormData:[bak dataUsingEncoding:NSUTF8StringEncoding] name:@"bak"];
//        [formData appendPartWithFormData:[fld40 dataUsingEncoding:NSUTF8StringEncoding] name:@"fld40"];
//        [formData appendPartWithFormData:[nameTextField.text dataUsingEncoding:NSUTF8StringEncoding] name:@"名前"];
//
    Multipart *multiPart = [[Multipart alloc]init];
    [multiPart addString:@"UTF-8" forKey:@"mcode"];
//    [multiPart addString:@"131" forKey:@"tno"];
    [multiPart addString:@"133" forKey:@"tno"];
//    [multiPart addString:@"RES_TSG_BLG_RSR" forKey:@"fld40"];
    [multiPart addString:@"RES_TSG_IAP_RSR" forKey:@"fld40"];
    [multiPart addString:@"-2" forKey:@"bak"];
    [multiPart addString:self.mailName forKey:@"name1"];
    [multiPart addString:self.mailAdress forKey:@"email"];

    

    NSURL *url = [NSURL URLWithString:MAILMAGAZINE_REGISTER_URL];
    NSMutableURLRequest *request = [[NSMutableURLRequest  alloc]initWithURL:url];
    NSData *body = [multiPart body];

    [request setHTTPMethod:@"POST"];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    [request setTimeoutInterval:10.0];
    [request setHTTPBody:body];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[multiPart contentType] forHTTPHeaderField:@"Content-Type"];
    
    
    
    
    
//    [webView loadRequest:request];
//    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSData *htmlData = (NSData*)responseObject;
        NSString *htmlString = [[NSString alloc]initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
        
        // HTMLを改ざんして、スマフォに最適な表示に変える
        NSString *destHTMLParts = @"<style type=\"text/css\">\
                            #wrap{padding:0px 0; width:100%; margin:0;}\
                            TABLE{width:250px;}\
                            HR{width:100%;}\
                            TABLE TD {word-break: break-all;}\
        .btn {\
	width: 200px;\
    height: 50px;\
	margin: 0;\
	padding: 5px;\
	background: -webkit-gradient(linear, left top, left bottom, from(#FFAA33), to(#FF8800));\
	border: 1px #F27300 solid;\
	color: #FFF;\
        -webkit-appearance: none;\
        -webkit-border-radius: 10px;\
        -webkit-box-shadow: 0 2px 2px #CCC;\
        text-shadow: 1px 2px 3px #C45C00;\
    }\
                            </style>\
                            </head>";
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"</head>" withString:destHTMLParts];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<INPUT type=\"button\" class=\"btn\" name=\"bak\" value=\"戻る\" onClick=\"history.back()\">" withString:@""];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/jslib" withString:@"https://55auto.biz/jslib"];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/css/main.css" withString:@"https://55auto.biz/css/main.css"];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"responder.php" withString:MAILMAGAZINE_REGISTER_URL];
        
        
        [webView loadHTMLString:htmlString baseURL: nil];
        //                [webView loadHTMLString:[node rawContents] baseURL:nil];
        NSLog(@"%@", htmlString);
        [activityIndicator stopAnimating];
//        [self parseHTMLAndDispWithHTML:htmlString];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error");
    }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [queue addOperation:operation];
}

- (void)setMailAdress:(NSString *)mailAdress
{
    _mailAdress = mailAdress;
}
- (NSString*)mailAdress
{
    return _mailAdress;
}
- (void)setMailName:(NSString *)mailName
{
    _mailName = mailName;
}
- (NSString*)mailName
{
    return _mailName;
}

- (void)parseHTMLAndDispWithHTML:(NSString*)htmlString
{
    // 解析開始
    NSError *error = nil;
    HTMLParser *parser = [[HTMLParser alloc]initWithString:htmlString error:&error];
    HTMLNode *node = [parser body];
    NSArray *DIVs = [node findChildTags:@"div"];
    
    for (int i=0; i< [DIVs count]; i++)
    {
        HTMLNode *node = [DIVs objectAtIndex:i];
        if ([[node getAttributeNamed:@"id"] isEqualToString:@"main"])
        {
            NSString *contentHTML = [node rawContents];
            
            // iframeがあったら、srcをUIWebView用に書き換える
            NSArray *IFRAMEs = [node findChildTags:@"iframe"];
            for (int i=0; i<[IFRAMEs count]; i++)
            {
                HTMLNode *IFRAME = [IFRAMEs objectAtIndex:i];
                NSString *IFRAME_src = [IFRAME getAttributeNamed:@"src"];
                NSString *videoID = [[IFRAME_src componentsSeparatedByString:@"/"]lastObject];
                NSString *newIFRAME_src = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@?showinfo=0", videoID];
                
                NSString *IFRAME_width = [NSString stringWithFormat:@"width=\"%@\"",[IFRAME getAttributeNamed:@"width"]];
                NSString *newIFRAME_width = [NSString stringWithFormat:@"width=\"%d\"",280];
                contentHTML = [contentHTML stringByReplacingOccurrencesOfString:IFRAME_src withString:newIFRAME_src];
                contentHTML = [contentHTML stringByReplacingOccurrencesOfString:IFRAME_width withString:newIFRAME_width];
            }
            
            // コメント内容の幅を少し縮める
            //            contentHTML = [contentHTML stringByReplacingOccurrencesOfString:@"cols=\"50\"" withString:@"cols=\"47\""];
            
            // 記事以外の余計な部分を削除する
            //                contentHTML = [[contentHTML componentsSeparatedByString:@"<p>例によって良ければ帰る前にこちらをクリックしてやって下さい。</p>"]objectAtIndex:0];
            
            // コメント欄を消す
            contentHTML = [[contentHTML componentsSeparatedByString:@"<h3 id=\"respond\">"]objectAtIndex:0];
            
            //            NSString *html = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">#main h2{font-size:18px;margin-bottom: 10px;padding: 0 15px 0 45px;line-height:25px;background:url(http://urban-ascetic.com/wp/wp-content/themes/cloudtpl_787/images/heading.png) no-repeat 0 50%%;border-bottom:1px solid #1d1d1d;}#date {text-align: right;}</style></head><body width=\"320px\">%@</div></body></html>", contentHTML];
            NSString *html = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">\
                              p{margin:10;}\
                              #main p{font-size:13.5pt; color:#524942; padding:0 10 0 10; line-height:150%%;}\
                              #main h2{height:auto !important; padding:20; font-size:18px; color:#F5F5F5; background:#f8f7ed url(http://urban-ascetic.com/wp/wp-content/themes/cloudtpl_787/images/bg_top.png) repeat-x;}\
                              #date {text-align:right; padding:0 10 0 0; color:#524942;}\
                              body{margin:0; overflow-x:hidden;}\
                              </style>\
                              </head>\
                              <body width=\"320px\">%@</div></body>\
                              </html>", contentHTML];
            
            
            webView.backgroundColor = [UIColor colorWithColorCode:BG_COLOR];
            [webView loadHTMLString:html baseURL: nil];
            //                [webView loadHTMLString:[node rawContents] baseURL:nil];
            
            [activityIndicator stopAnimating];
            break;
        }
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
