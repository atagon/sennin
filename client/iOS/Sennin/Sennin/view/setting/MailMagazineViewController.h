//
//  MailMagazineViewController.h
//  Sennin
//
//  Created by abt on 2014/04/13.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface MailMagazineViewController : CommonViewController <UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
