//
//  LibraryViewController.m
//  QuickTodo+
//
//  Created by abt on 2014/03/20.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "LibraryViewController.h"
#import "PropertyDirector.h"

@interface LibraryViewController ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation LibraryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleLabel.text = self.title;
    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"ライブラリ"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
