//
//  MailViewController.m
//  QuickTodo+
//
//  Created by abt on 2014/03/10.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "MailViewController.h"
#import "Config.h"
#import "UIColor+WithColorCode.h"
#import "UIImage+imagePath.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "MailTopicViewController.h"
#import "PropertyDirector.h"


@interface MailViewController ()
@property (strong, nonatomic) IBOutlet UIButton *sendMailBtn;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong, readwrite) NSArray *dataSource;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation MailViewController

@synthesize topic, detail, device, iOS, appName, version;

static int selectedTopicIndex = 0;


+(int)selectedTopicIndex
{
    return selectedTopicIndex;
}

+(void)setSelectedTopicIndex:(int)index
{
    selectedTopicIndex = index;
}


- (IBAction)sendMail:(id)sender {
    // タイトル
    NSString *title = [NSString stringWithFormat:@"%@(v.%@):[ %@ ]", self.appName, self.version, self.topic];
    // 送信先
    NSString *to = SUPPORT_MAIL;
    // 内容
    NSString* mailDetail = [NSString stringWithFormat:@"%@\n\n\ndevice: %@\niOS: %@\nappName: %@ %@", self.detail, self.device, self.iOS, self.appName, self.version];
    
    if (self.detail == nil || [self.detail isEqualToString:@""])
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"詳細内容を記入してください" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    
    if (![MFMailComposeViewController canSendMail])
    {
        if (![self launchGmailWithTitle:title body:mailDetail to:to])
        {
            // メーラが利用できない端末
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"メーラーを起動できませんでした。\nメールの設定を確認してください" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            return;
        }
        else
        {
            // Gmailで送信するので画面は閉じる
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
    }
    
    MFMailComposeViewController* picker = [[MFMailComposeViewController alloc]init];
    if (picker == nil)
    {
        // メーラが利用できない端末
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil message:@"メーラーを起動できませんでした。\nメールの設定を確認してください" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    picker.mailComposeDelegate = self;
    // タイトル
    [picker setSubject:title];
    // 送信先
    [picker setToRecipients:[NSArray arrayWithObject:SUPPORT_MAIL]];
    // 内容
    //    NSString* mailDetail = [NSString stringWithFormat:@"%@\n\n\ndevice: %@\niOS: %@\nappName: %@ %@", self.detail, self.device, self.iOS, self.appName, self.version];
    [picker setMessageBody:mailDetail isHTML:NO];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [UIView commitAnimations];
    
    
    // メーラーの起動
    [self presentViewController:picker animated:YES completion:^{
        //
    }];
}



-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"Mail"];

    
    self.titleLabel.text = self.title;
    self.tableView.userInteractionEnabled = YES;
    self.tableView.multipleTouchEnabled = YES;
//    self.tableView.scrollEnabled = NO;

//    UIBarButtonItem* sendBtn = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"mailButtonTitle", nil) style:UIBarButtonItemStylePlain target:self action:@selector(send:)];
//    self.navigationItem.rightBarButtonItem = sendBtn;
//    if ([sendBtn respondsToSelector:@selector(setTintColor:)])
//    {
//        sendBtn.tintColor = [UIColor blueColor];
//    }

    // 端末
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = (char*)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding: NSUTF8StringEncoding];
    free(machine);
    self.device = platform;
    
    // iOS
    UIDevice *dev = [UIDevice currentDevice];
    self.iOS = dev.systemVersion;
    
    // アプリ名
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *infoDictionary = [bundle localizedInfoDictionary];
    NSString* _appName = [[infoDictionary count] ? infoDictionary : [bundle infoDictionary] objectForKey:@"CFBundleDisplayName"];
    self.appName = _appName;
    
    // バージョン
    self.version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"];
    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"お問い合わせ"];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // セクション数
    return 2;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"入力内容";
    }
    else
    {
        NSArray *langs = [NSLocale preferredLanguages];
        NSString *currentLanguage = [langs objectAtIndex:0];
        if ([currentLanguage isEqualToString:@"ja"])
        {
            return @"注意点";
        }
        else
        {
            return @"動作環境の情報";
        }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // 行数
    if (section == 0)
    {
        return 2;
    }
    else {
        NSArray *langs = [NSLocale preferredLanguages];
        NSString *currentLanguage = [langs objectAtIndex:0];
        if ([currentLanguage isEqualToString:@"ja"])
        {
            return 1;
        }
        else
        {
            return 4;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0)
    {
        // 問い合わせ内容
        if (indexPath.row == 0)
        {
            // トピック
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"topic"];
            cell.textLabel.text = @"トピック";
            
            switch ([MailViewController selectedTopicIndex]) {
                case 0:
                    cell.detailTextLabel.text = @"質問";
                    break;
                case 1:
                    cell.detailTextLabel.text = @"リクエスト";
                    break;
                case 2:
                    cell.detailTextLabel.text = @"バグ報告";
                    break;
                case 3:
                    cell.detailTextLabel.text = @"その他";
                    break;
            }
            
            // > 記号
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            // 選択色
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            // 文字色
            cell.textLabel.textColor = [UIColor darkGrayColor];
            
            
            self.topic = cell.detailTextLabel.text;
        }
        else {
            // 詳細内容
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            
            if (APP_HEIGHT>480)
            {
                cellTextView = [[UITextView alloc]initWithFrame:CGRectMake(10, 0, 290, 170)];
            }
            else
            {
                cellTextView = [[UITextView alloc]initWithFrame:CGRectMake(10, 0, 290, 90)];
            }
            [cellTextView setFont:[UIFont systemFontOfSize:16.0]];
            cellTextView.backgroundColor = [UIColor clearColor];
            cellTextView.delegate = self;
            
            
            if (self.detail == nil || [self.detail isEqualToString:@""])
            {
                cellTextView.text = @"詳細内容を記入してください";
                cellTextView.textColor = [UIColor lightGrayColor];
            }
            else {
                cellTextView.text = self.detail;
                //                cellTextView.textColor = [UIColor colorWithColorCode:BASE_COLOR];
            }
            [cell addSubview:cellTextView];
            
            // 選択色
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    }
    else {
        NSArray *langs = [NSLocale preferredLanguages];
        NSString *currentLanguage = [langs objectAtIndex:0];
        if ([currentLanguage isEqualToString:@"ja"])
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 0, 300, 177)];
            textView.editable = NO;
            [textView setFont:[UIFont systemFontOfSize:16.0]];
            textView.backgroundColor = [UIColor clearColor];
            textView.text = @"携帯電話で迷惑メール対策の設定をしている場合、こちらから返信ができません。\n設定の変更をお願いいたします｡\n\n「urban-ascetic.com」\nドメイン指定受信を設定している方は、上のドメインを指定してください。";
            textView.textColor = [UIColor orangeColor];
            [cell addSubview:textView];
            
            // 選択色
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        else
        {
            // 端末情報
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
            // 選択色
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            // 文字色
            //            cell.textLabel.textColor = [UIColor colorWithColorCode:BASE_COLOR];
            
            switch (indexPath.row) {
                case 0:
                {
                    // 端末
                    cell.textLabel.text = @"端末";
                    cell.detailTextLabel.text = self.device;
                }
                    break;
                case 1:
                {
                    // iOS
                    cell.textLabel.text = @"iOS";
                    cell.detailTextLabel.text = self.iOS;
                }
                    break;
                case 2:
                {
                    // アプリ名
                    cell.textLabel.text = @"アプリ名";
                    cell.detailTextLabel.text = self.appName;
                }
                    break;
                case 3:
                {
                    // バージョン
                    cell.textLabel.text = @"バージョン";
                    cell.detailTextLabel.text = self.version;
                }
                    break;
            }
        }
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1)
    {
        if (APP_HEIGHT>480)
        {
            return 170;
        }
        else
        {
            return 90;
        }
    }
    else {
        NSArray *langs = [NSLocale preferredLanguages];
        NSString *currentLanguage = [langs objectAtIndex:0];
        if (indexPath.section == 1 && [currentLanguage isEqualToString:@"ja"])
        {
            return 177;
        }
        else
        {
            return 44;
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (![PropertyDirector sharedDirector].isOverOS7)
    //    {
    //        [UIView beginAnimations:nil context:nil];
    //        [UIView setAnimationDuration:0.2];
    //        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    //        [UIView commitAnimations];
    //    }
    
    // 選択状態の解除
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    if ([cell.textLabel.text isEqualToString:@"トピック"])
    {
        MailTopicViewController* supportTopic = [self.storyboard instantiateViewControllerWithIdentifier:@"settingMailTopic"];
        [self.navigationController pushViewController:supportTopic animated:YES];
    }
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //    NSLog(@"%d", [scrollView.superview isKindOfClass:[UIViewController class]]);
    //    [cellTextView resignFirstResponder];
}


-(BOOL)launchGmailWithTitle:(NSString*)title body:(NSString*)mailDetail to:(NSString*)to
{
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"googlegmail://"]]) {
        return NO;
    }
    
    NSString *url = [NSString stringWithFormat:@"googlegmail:///co?subject=%@&body=%@&to=%@",
                     (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(
                                                                                          kCFAllocatorDefault,
                                                                                          (CFStringRef)title,
                                                                                          NULL,
                                                                                          (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                          kCFStringEncodingUTF8),
                     (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(
                                                                                          kCFAllocatorDefault,
                                                                                          (CFStringRef)mailDetail,
                                                                                          NULL,
                                                                                          (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                          kCFStringEncodingUTF8),
                     to];
    NSURL *mailURL = [NSURL URLWithString:url];
    [[UIApplication sharedApplication] openURL:mailURL];
    
    return YES;
}


-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    [UIView commitAnimations];
    
    if ([textView.text isEqualToString:@"詳細内容を記入してください"])
    {
        textView.text = @"";
    }
    //    textView.textColor = [UIColor colorWithColorCode:BASE_COLOR];
    return YES;
}


-(void)textViewDidChange:(UITextView *)textView
{
    self.detail = textView.text;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
