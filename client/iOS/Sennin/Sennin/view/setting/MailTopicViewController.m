//
//  MailTopicViewController.m
//  QuickTodo+
//
//  Created by 倫広 安宅 on 12/05/02.
//  Copyright (c) 2012年 flickfrog. All rights reserved.
//

#import "MailTopicViewController.h"
//#import "TableListViewBg.h"
#import "MailViewController.h"
#import "Config.h"
#import "UIColor+WithColorCode.h"
#import "PropertyDirector.h"

@implementation MailTopicViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleLabel.text = @"トピック";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"質問";
            break;
        case 1:
            cell.textLabel.text = @"リクエスト";
            break;
        case 2:
            cell.textLabel.text = @"バグ報告";
            break;
        case 3:
            cell.textLabel.text = @"その他";
            break;           
    }
    
    
    if (indexPath.row == [MailViewController selectedTopicIndex])
    {
        NSIndexPath* path = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionTop];
    }
    else {
        cell.textLabel.textColor = [UIColor darkGrayColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [MailViewController setSelectedTopicIndex:(int)indexPath.row];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
