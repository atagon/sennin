//
//  SettingViewController.m
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "SettingViewController.h"
#import "UIImage+imagePath.h"
#import "MailMagazineViewController.h"
#import "MailViewController.h"
#import "FFAppsViewController.h"
#import "LibraryViewController.h"
#import "PropertyDirector.h"


@interface SettingViewController ()
@property (strong, readwrite) NSArray *sectionDataSource;
@property (strong, readwrite) NSArray *rowDataSource;
@property (strong, readwrite) NSArray *rowIconSource;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SettingViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGesture:)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeGesture];
    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"インフォメーション"];
}

- (void)swipeGesture:(UISwipeGestureRecognizer*)recognizer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateData
{
    // アップグレード済みの人は、アップグレード欄が無くなる
    self.sectionDataSource = @[
                               @"さらに濃い情報を！",
                               @"サポート",
                               @"ライブラリ",
                               ];
    self.rowDataSource = @[
                           @[@"メルマガ登録"],
                           @[@"お問い合わせ", @"おすすめアプリ"],
                           @[@"ライセンス"],
                           ];
    self.rowIconSource = @[
                           @[@"support.png"],
                           @[@"help.png", @"app.png"],
                           @[@""],
                           @[@""],
                           ];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateData];
    [self.tableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionDataSource count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.rowDataSource objectAtIndex:section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *textKey = [[self.rowDataSource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    cell.textLabel.text = textKey;
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *icon = [[self.rowIconSource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    if (![icon isEqualToString:@""])
    {
        cell.imageView.image = [UIImage imagePath:icon];
    }
    else
    {
        cell.imageView.image = nil;
    }
    
    return cell;
}


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *textKey = [self.sectionDataSource objectAtIndex:section];
    return NSLocalizedString(textKey, nil);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *textKey = [[self.rowDataSource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    
    if ([textKey isEqualToString:@"メルマガ登録"])
    {
        MailMagazineViewController *mailMagazine = [self.storyboard instantiateViewControllerWithIdentifier:@"mailMagazine"];
        mailMagazine.title = textKey;
        [self.navigationController pushViewController:mailMagazine animated:YES];
    }
    else if ([textKey isEqualToString:@"お問い合わせ"])
    {
        // メールサポート
        MailViewController *mailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingMail"];
        mailViewController.title = textKey;
        [self.navigationController pushViewController:mailViewController animated:YES];
    }
    else if ([textKey isEqualToString:@"おすすめアプリ"])
    {
        // 関連アプリ
        MailViewController *appsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingApps"];
        appsViewController.title = textKey;
        [self.navigationController pushViewController:appsViewController animated:YES];
    }
    else if ([textKey isEqualToString:@"ライセンス"])
    {
        // ライセンス
        LibraryViewController *libraryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingLicense"];
        libraryViewController.title = textKey;
        [self.navigationController pushViewController:libraryViewController animated:YES];
    }

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
