//
//  MailViewController.h
//  QuickTodo+
//
//  Created by abt on 2014/03/10.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "CommonViewController.h"

@interface MailViewController : CommonViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate, UIScrollViewDelegate>
{
    UITextView* cellTextView;
}

@property(strong, readwrite) NSString* topic;
@property(strong, readwrite) NSString* detail;
@property(strong, readwrite) NSString* device;
@property(strong, readwrite) NSString* iOS;
@property(strong, readwrite) NSString* appName;
@property(strong, readwrite) NSString* version;


+(int)selectedTopicIndex;
+(void)setSelectedTopicIndex:(int)index;
-(BOOL)launchGmailWithTitle:(NSString*)title body:(NSString*)mailDetail to:(NSString*)to;

@end
