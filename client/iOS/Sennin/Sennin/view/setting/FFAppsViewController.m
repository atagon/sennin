//
//  FFAppsViewController.m
//  QuickTodo+
//
//  Created by abt on 2014/03/15.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "FFAppsViewController.h"
#import "UIImage+imagePath.h"
#import "PropertyDirector.h"


#define HEAD_HEIGHT 32

@interface FFAppsViewController ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, readwrite) NSArray *sectionDataSource;

@end

@implementation FFAppsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"Apps"];
    
    /* ルール
     * アプリ名(セクションタイトル): apps + [APP_NAME]
     * 説明(セル文言): apps + [APP_NAME] + Explain
     * アイコン: icon + [APP_NAME] + .png
     * クリックの飛び先リンク: apps + [APP_NAME] + URL
     */
    self.sectionDataSource = @[
                               @"QuickTodo",
                               @"QuickMemo",
                               @"MSArtbook",
                               ];

    self.titleLabel.text = self.title;
    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"関連アプリ"];    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionDataSource count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *appName = [self.sectionDataSource objectAtIndex:indexPath.section];
    NSString *textKey = [NSString stringWithFormat:@"apps%@Explain", appName];
    cell.textLabel.text = NSLocalizedString(textKey, nil);
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont systemFontOfSize:13.0];
    //    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.imageView.image = [UIImage imagePath:[NSString stringWithFormat:@"icon%@.png", appName]];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, HEAD_HEIGHT-22, [UIScreen mainScreen].bounds.size.width-15, 22)];
    NSString *appName = [self.sectionDataSource objectAtIndex:section];
    NSString *textKey = [NSString stringWithFormat:@"apps%@", appName];
    label.text = NSLocalizedString(textKey, nil);
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, HEAD_HEIGHT)];
    [view addSubview:label];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEAD_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 20.0f;
}


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *textKey = [NSString stringWithFormat:@"apps%@", [self.sectionDataSource objectAtIndex:section]];
    return NSLocalizedString(textKey, nil);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *textKey = [NSString stringWithFormat:@"apps%@URL", [self.sectionDataSource objectAtIndex:indexPath.section]];
    NSString *URLstr = NSLocalizedString(textKey, nil);
    NSURL *URL = [NSURL URLWithString:URLstr];
    
    [[UIApplication sharedApplication] openURL:URL];
    
    NSString *label = [NSString stringWithFormat:@"Apps: %@", [self.sectionDataSource objectAtIndex:indexPath.section]];
    [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"インフォメーション" action:@"関連アプリ" label:label];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
