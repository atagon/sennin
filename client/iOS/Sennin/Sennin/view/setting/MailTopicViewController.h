//
//  MailTopicViewController.h
//  QuickTodo+
//
//  Created by 倫広 安宅 on 12/05/02.
//  Copyright (c) 2012年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface MailTopicViewController : CommonViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
