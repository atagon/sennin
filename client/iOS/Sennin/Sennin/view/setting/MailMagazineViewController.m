//
//  MailMagazineViewController.m
//  Sennin
//
//  Created by abt on 2014/04/13.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "MailMagazineViewController.h"
#import "PropertyDirector.h"
#import "AFHTTPRequestOperationManager.h"
#import "Config.h"
#import "ConfirmMailMagazineViewController.h"


@interface MailMagazineViewController ()

@end

@implementation MailMagazineViewController
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    IBOutlet UITextField *nameTextField;
    IBOutlet UITextField *mailadressTextField;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

//- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
//{
//    NSLog(@"First view return action invoked.");
//}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)registerMailmagazine:(id)sender {
    // name1
    if ([[[nameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@"　" withString:@""] isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"「名前」を入力して下さい。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }

    // email
    if ([[[mailadressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@"　" withString:@""] isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"「メールアドレス」を入力して下さい。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    [nameTextField resignFirstResponder];
    [mailadressTextField resignFirstResponder];
    
    [scrollView setContentOffset:CGPointMake(0, 110) animated:YES];


    ConfirmMailMagazineViewController *confirmView = [self.storyboard instantiateViewControllerWithIdentifier:@"confirmMailMagazine"];
    confirmView.mailName = nameTextField.text;
    confirmView.mailAdress = mailadressTextField.text;
    [self.navigationController pushViewController:confirmView animated:YES];
    
    
//    activityIndicator.hidden = NO;
//    [activityIndicator startAnimating];
//    self.view.userInteractionEnabled = NO;
//
//    NSString *mcode = @"UTF-8";
//    NSString *tno = @"131";
//    NSString *bak = @"-2";
//    NSString *fld40 = @"RES_TSG_BLG_RSR";
//
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", nil];
//    [manager POST:MAILMAGAZINE_REGISTER_URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFormData:[mcode dataUsingEncoding:NSUTF8StringEncoding] name:@"mcode"];
//        [formData appendPartWithFormData:[tno dataUsingEncoding:NSUTF8StringEncoding] name:@"tno"];
//        [formData appendPartWithFormData:[bak dataUsingEncoding:NSUTF8StringEncoding] name:@"bak"];
//        [formData appendPartWithFormData:[fld40 dataUsingEncoding:NSUTF8StringEncoding] name:@"fld40"];
//        [formData appendPartWithFormData:[nameTextField.text dataUsingEncoding:NSUTF8StringEncoding] name:@"名前"];
//        [formData appendPartWithFormData:[mailadressTextField.text dataUsingEncoding:NSUTF8StringEncoding] name:@"メールアドレス"];
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        // 送信成功
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"メルマガの登録リクエストを送信しました。" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        
//        [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"メルマガ" action:@"登録" label:@"メルマガ登録成功"];
//        
//        [activityIndicator stopAnimating];
//        self.view.userInteractionEnabled = YES;
//
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        // 送信失敗
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"メルマガの登録リクエストの送信に失敗しました。\n大変申し訳ありませんが、しばらく待ってからもう一度お試しください。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        
//        [[PropertyDirector sharedDirector]sendGoogleAnaliticsActionCategory:@"メルマガ" action:@"登録" label:@"メルマガ登録失敗"];
//        
//        [activityIndicator stopAnimating];
//        self.view.userInteractionEnabled = YES;
//
//    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointMake(0, 290) animated:YES];
    scrollView.scrollEnabled = NO;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    scrollView.scrollEnabled = YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [nameTextField resignFirstResponder];
    [mailadressTextField resignFirstResponder];

    [scrollView setContentOffset:CGPointMake(0, 110) animated:YES];
    
    return YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleLabel.text = self.title;
    
    scrollView.contentSize = contentView.frame.size;
    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"メルマガ"];
    activityIndicator.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
