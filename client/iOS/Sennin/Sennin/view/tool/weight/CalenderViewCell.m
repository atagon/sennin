//
//  CalenderViewCell.m
//  Sennin
//
//  Created by abt on 2014/07/03.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "CalenderViewCell.h"
#import "UIImage+imagePath.h"

@implementation CalenderViewCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _notificationView = [[UIImageView alloc]initWithImage:[UIImage imagePath:@"trainedIcon.png"]];
        [_notificationView setHidden:YES];
        [self.contentView addSubview:_notificationView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize viewSize = self.contentView.frame.size;
    
    [[self notificationView] setCenter:CGPointMake(viewSize.width/2, viewSize.height/2)];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [[self notificationView] setHidden:YES];
}


@end
