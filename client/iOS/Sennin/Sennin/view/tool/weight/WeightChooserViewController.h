//
//  WeightChooserViewController.h
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

#define WeightChooserTypeParts 0
#define WeightChooserTypeMenu 1
#define WeightChooserTypeOption 2
#define WeightChooserTypeSetNum 3

@interface WeightChooserViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(assign, readwrite) int weightChooserType;
@property(assign, readwrite) int partsId;

@end
