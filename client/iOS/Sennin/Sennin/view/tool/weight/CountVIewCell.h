//
//  CountVIewCell.h
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountVIewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) IBOutlet UILabel *unitLabel;

@end
