//
//  CountViewController.h
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountViewController : UITableViewController

@property(assign, readwrite) NSString *unit;
@property(retain, readwrite) NSIndexPath *indexPath;
@end
