//
//  CalenderViewController.m
//  Sennin
//
//  Created by abt on 2014/07/03.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "CalenderViewController.h"
#import "RDVCalendarView.h"
#import "UIImage+imagePath.h"
#import "Config.h"
#import "SQLiteManager.h"
#import "PropertyDirector.h"
#import "Notification.h"
#import "CalenderViewCell.h"

@interface CalenderViewController ()

@end

@implementation CalenderViewController
{
    NSArray *dataSource;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.calendarView.backButton setImage:[UIImage imagePath:@"prevBtn.png"] forState:UIControlStateNormal];
    [self.calendarView.backButton setTitle:@"" forState:UIControlStateNormal];
    
    [self.calendarView.forwardButton setImage:[UIImage imagePath:@"nextBtn.png"] forState:UIControlStateNormal];
    [self.calendarView.forwardButton setTitle:@"" forState:UIControlStateNormal];
    
    self.calendarView.separatorColor = [UIColor colorWithPatternImage:[UIImage imagePath:@"separater.png"]];
    self.calendarView.selectedDayColor = [UIColor lightGrayColor];
    
    self.calendarView.currentDayColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.05];
    
    dataSource = [[SQLiteManager shardManager]trainingDaysWithYear:self.calendarView.month.year month:self.calendarView.month.month];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.calendarView.backButton setFrame:CGRectMake(70, 12, 44, 44)];
    [self.calendarView.forwardButton setFrame:CGRectMake(206, 12, 44, 44)];
    [self.calendarView.monthLabel setCenter:CGPointMake(APP_WIDTH/2, 12+44/2)];
    self.calendarView.monthLabel.textColor = [UIColor darkGrayColor];
}


- (void)calendarView:(RDVCalendarView *)calendarView didSelectDate:(NSDate *)date;
{
    NSString *notificationName;
    if (self.calenderType == kCalenderForDisplayTraining)
    {
        notificationName = CHOOSE_CALENDER_DATE;
    }
    else if (self.calenderType == kCalenderForChooseTrainingMenu)
    {
        notificationName = CHOOSE_CALENDER_MENU_DATE;
    }
    
    NSDictionary *userInfo = @{@"date":@([date timeIntervalSince1970])};
    [[NSNotificationCenter defaultCenter]postNotificationName:notificationName object:nil userInfo:userInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)calendarView:(RDVCalendarView *)calendarView didChangeMonth:(NSDateComponents *)month;
{
    dataSource = [[SQLiteManager shardManager]trainingDaysWithYear:month.year month:month.month];
}

- (void)calendarView:(RDVCalendarView *)calendarView configureDayCell:(RDVCalendarDayCell *)dayCell
             atIndex:(NSInteger)index {

    if ([dataSource indexOfObject:@(index+1)] == NSNotFound)
    {
        ((CalenderViewCell*)dayCell).notificationView.hidden = YES;
    }
    else
    {
        ((CalenderViewCell*)dayCell).notificationView.hidden = NO;
    }
}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
- (void)viewDidLoad
{
    [super viewDidLoad];

    [[self calendarView] registerDayCellClass:[CalenderViewCell class]];
}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
