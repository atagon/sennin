//
//  WeightChooserViewController.m
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "WeightChooserViewController.h"
#import "WeightTypeViewController.h"
#import "SQLiteManager.h"

@interface WeightChooserViewController ()

@end

@implementation WeightChooserViewController
{
    WeightTypeViewController *weightTypeViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.weightChooserType) {
        case WeightChooserTypeParts:
            return [[[SQLiteManager shardManager]bodyParts]count];
            break;
        case WeightChooserTypeMenu:
            return [[[SQLiteManager shardManager]trainingMenuWithPartsId:self.partsId]count];
            break;
        case WeightChooserTypeOption:
            return [[[SQLiteManager shardManager]trainingOption]count];
            break;
        case WeightChooserTypeSetNum:
            return 100;
            break;
    }
    
    return 0;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"ChooserCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    switch (self.weightChooserType) {
        case WeightChooserTypeParts:
            cell.textLabel.text =  [[[[SQLiteManager shardManager]bodyParts] objectAtIndex:indexPath.row]objectForKey:@"name"];
            break;
        case WeightChooserTypeMenu:
            cell.textLabel.text =  [[[[SQLiteManager shardManager]trainingMenuWithPartsId:self.partsId] objectAtIndex:indexPath.row]objectForKey:@"name"];
            break;
        case WeightChooserTypeOption:
            cell.textLabel.text = [[[[SQLiteManager shardManager]trainingOption] objectAtIndex:indexPath.row]objectForKey:@"name"];
            break;
        case WeightChooserTypeSetNum:
            cell.textLabel.text =  [NSString stringWithFormat:@"%d 回", indexPath.row+1];
            break;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.weightChooserType) {
        case WeightChooserTypeParts:
            weightTypeViewController.partsId = [[[[[SQLiteManager shardManager]bodyParts] objectAtIndex:indexPath.row]objectForKey:@"parts_id"]intValue];
            weightTypeViewController.menuId = -1;
            break;
        case WeightChooserTypeMenu:
            weightTypeViewController.menuId = [[[[[SQLiteManager shardManager]trainingMenuWithPartsId:self.partsId] objectAtIndex:indexPath.row]objectForKey:@"menu_id"]intValue];
            break;
        case WeightChooserTypeOption:
            weightTypeViewController.optionId = [[[[[SQLiteManager shardManager]trainingOption] objectAtIndex:indexPath.row]objectForKey:@"option_id"]intValue];
            break;
        case WeightChooserTypeSetNum:
            weightTypeViewController.setNum = indexPath.row+1;
        default:
            break;
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    weightTypeViewController = [segue destinationViewController];
}

@end
