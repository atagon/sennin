//
//  WeightTypeViewController.h
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeightTrainingViewController.h"
#import "CommonViewController.h"

@interface WeightTypeViewController : CommonViewController <UITableViewDataSource, UITableViewDelegate>

@property (assign, readwrite) int partsId;
@property (assign, readwrite) int menuId;
@property (assign, readwrite) int optionId;

@property (assign, readwrite) int setNum;

@property (assign, readwrite) WeightTrainingViewController *weightView;
@property (assign, readwrite) NSTimeInterval setDateTime;
@end
