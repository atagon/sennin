//
//  UIPickerViewController.h
//  Sennin
//
//  Created by abt on 2014/07/04.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPickerViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (assign, readwrite) NSString *unit;
@property (assign, readwrite) NSString *num;
@property(retain, readwrite) NSIndexPath *indexPath;

@end
