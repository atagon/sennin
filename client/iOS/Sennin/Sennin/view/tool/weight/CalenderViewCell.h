//
//  CalenderViewCell.h
//  Sennin
//
//  Created by abt on 2014/07/03.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "RDVCalendarDayCell.h"

@interface CalenderViewCell : RDVCalendarDayCell

@property (nonatomic) UIImageView *notificationView;

@end
