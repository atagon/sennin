//
//  WeightTrainingViewController.m
//  Sennin
//
//  Created by abt on 2014/06/30.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "WeightTrainingViewController.h"
#import "PropertyDirector.h"
#import "Config.h"
#import "WeightTrainingCell.h"
#import "AddWeightSetCell.h"
#import "WeightTypeViewController.h"
#import "UIImage+imagePath.h"
#import "Notification.h"
#import "CountViewController.h"
#import "UIPickerViewController.h"
#import "SQLiteManager.h"
#import "UIColor+WithColorCode.h"
#import "CalenderViewController.h"

#define HEADER_HEIGHT 44

@interface WeightTrainingViewController ()

@end

@implementation WeightTrainingViewController
{
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *setDateLabel;
    
    NSArray *dataSource;
    IBOutlet UIButton *addSameMenuBtn;
    
    IBOutlet UIButton *editSectionBtn;
    BOOL isSectionEditing;
}

//- (IBAction)addSameMenu:(id)sender {
//    [[SQLiteManager shardManager]addSameTrainingMenuAtDate:self.setDateTime];
//    [self reload];
//}


- (CGFloat)tableView:(UITableView *)_tableView heightForHeaderInSection:(NSInteger)section
{
    if (isSectionEditing)
    {
        return 0;
    }
    else
    {
        return HEADER_HEIGHT;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView = [[UIView alloc]init];
    sectionView.backgroundColor = [UIColor colorWithColorCode:0x00aaeb];
    [sectionView setFrame:CGRectMake(0, 0, APP_WIDTH, HEADER_HEIGHT)];
    
    UILabel *sectionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, APP_WIDTH-20, HEADER_HEIGHT)];
    sectionLabel.textColor = [UIColor whiteColor];
    sectionLabel.font = [UIFont boldSystemFontOfSize:12];
    sectionLabel.numberOfLines = 2;
    sectionLabel.adjustsFontSizeToFitWidth = YES;
    
    NSDictionary *sectionData = [[dataSource objectAtIndex:section]objectAtIndex:0];
    NSString *partsName = [[SQLiteManager shardManager]bodyNameWithMenuId:[[sectionData objectForKey:@"menu_id"]intValue]];
    NSString *menuName = [[SQLiteManager shardManager]trainingMenuNameWithMenuId:[[sectionData objectForKey:@"menu_id"]intValue]];
    NSString *optionName = ([[sectionData objectForKey:@"option_id"]intValue] == 1) ? @"" : [NSString stringWithFormat:@" -%@", [[SQLiteManager shardManager]trainingOptionNameWithOptionId:[[sectionData objectForKey:@"option_id"]intValue]]];

    sectionLabel.text = [NSString stringWithFormat:@"%@ : %@ %@", partsName, menuName, optionName];
    
    [sectionView addSubview:sectionLabel];
    
    return sectionView;
}


- (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if (isSectionEditing)
        {
            NSDictionary *sectionData = [[dataSource objectAtIndex:indexPath.row]objectAtIndex:0];
            [[SQLiteManager shardManager]deleteTraining:[[sectionData objectForKey:@"training_id"]intValue]];
            dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];
            
            // セクションの削除
            [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            NSArray *sectionData = [dataSource objectAtIndex:indexPath.section];
            int rowNum = [sectionData count];
            rowNum--;
            
            // トレーニング詳細の削除
            [[SQLiteManager shardManager]deleteTrainingDetail:[[[sectionData objectAtIndex:indexPath.row]objectForKey:@"detail_id"]intValue]];
            
            if (rowNum == 0)
            {
                // トレーニングの削除
                [[SQLiteManager shardManager]deleteTraining:[[[sectionData lastObject]objectForKey:@"training_id"]intValue]];
                dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];
                
                // セクションの削除
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
                [_tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            }
            else
            {
                dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];
                for (int i=indexPath.row; i<rowNum+1; i++)
                {
                    // セット番号の再採番
                    NSIndexPath *cellIndexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
                    WeightTrainingCell *cell = (WeightTrainingCell*)[_tableView cellForRowAtIndexPath:cellIndexPath];
                    if (cell != nil)
                    {
                        cell.setNumLabel.text = [NSString stringWithFormat:@"#%d", i];
                    }
                }
                [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)_tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isSectionEditing)
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
    {
        if ([[dataSource objectAtIndex:indexPath.section] count] == indexPath.row)
        {
            return UITableViewCellEditingStyleNone;
        }
        else
        {
            return UITableViewCellEditingStyleDelete;
        }
    }
}

- (BOOL)tableView:(UITableView *)_tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isSectionEditing)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)tableView:(UITableView *)_tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    if (fromIndexPath.row != toIndexPath.row)
    {
        NSDictionary *fromData = [[dataSource objectAtIndex:fromIndexPath.row]objectAtIndex:0];
        NSDictionary *toData = [[dataSource objectAtIndex:toIndexPath.row]objectAtIndex:0];
        
        [[SQLiteManager shardManager]exchangeTrainingWithFromTrainingId:[[fromData objectForKey:@"training_id"]intValue]
                                                              fromOrder:[[fromData objectForKey:@"training_disp_order"]intValue]
                                                           toTrainingId:[[toData objectForKey:@"training_id"]intValue]
                                                            toOrder:[[toData objectForKey:@"training_disp_order"]intValue]];
        
        dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    if (isSectionEditing)
    {
        return 1;
    }
    else
    {
        return [dataSource count];
    }
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSectionEditing)
    {
        return [dataSource count];
    }
    else
    {
        return [[dataSource objectAtIndex:section] count]+1;
    }
}

//- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return [dataSource objectAtIndex:section];
//}

- (UITableViewCell*)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"weightMenuCell";
    NSString *CellIdentifier2 = @"addWeightSetCell";
    NSString *EditCellIdentifier = @"Cell";

    UITableViewCell *cell;
    if (isSectionEditing)
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:EditCellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:EditCellIdentifier];
        }
        
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:13];
        cell.backgroundColor = [UIColor colorWithColorCode:0x00aaeb];
        
        NSDictionary *sectionData = [[dataSource objectAtIndex:indexPath.row]objectAtIndex:0];
        NSString *partsName = [[SQLiteManager shardManager]bodyNameWithMenuId:[[sectionData objectForKey:@"menu_id"]intValue]];
        NSString *menuName = [[SQLiteManager shardManager]trainingMenuNameWithMenuId:[[sectionData objectForKey:@"menu_id"]intValue]];
        NSString *optionName = ([[sectionData objectForKey:@"option_id"]intValue] == 1) ? @"" : [NSString stringWithFormat:@"[%@]", [[SQLiteManager shardManager]trainingOptionNameWithOptionId:[[sectionData objectForKey:@"option_id"]intValue]]];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ : %@ %@", partsName, menuName, optionName];
        
        return cell;
    }

    
    
    
    if ([[dataSource objectAtIndex:indexPath.section] count] == indexPath.row)
    {
        AddWeightSetCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier2 forIndexPath:indexPath];
        cell.indexPath = indexPath;

        return cell;
    }
    else
    {
        WeightTrainingCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.setNumLabel.text = [NSString stringWithFormat:@"#%d", indexPath.row+1];
        if (indexPath.row == 0)
        {
            cell.separatorView.backgroundColor = [UIColor clearColor];
        }
        else
        {
            cell.separatorView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imagePath:@"separater.png"]];
        }
        
        cell.indexPath = indexPath;
        
        NSDictionary *rowData = [[dataSource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        [cell setWeight:[[rowData objectForKey:@"kg"]floatValue]];
        [cell setCount:[[rowData objectForKey:@"times"]intValue]];

        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[dataSource objectAtIndex:indexPath.section] count] == indexPath.row)
    {
        return 30;
    }
    else
    {
        return 44;
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reload];
}
- (void)reload
{
    setDateLabel.text = [[PropertyDirector sharedDirector]getDateStringWithTimeInterval:self.setDateTime];
    dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];
    
    if ([dataSource count] > 0)
    {
        addSameMenuBtn.hidden = YES;
    }
    else
    {
        addSameMenuBtn.hidden = NO;
    }
    

    [self cancelEditing];
    [tableView reloadData];
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [[PropertyDirector sharedDirector]sendGoogleAnalitics:@"Tool ウェイトトレーニング"];
    
    NSDate *setDate = [NSDate date];
    self.setDateTime = [[PropertyDirector sharedDirector]getDateNumber:setDate];
    setDateLabel.text = [[PropertyDirector sharedDirector]getDateStringWithTimeInterval:self.setDateTime];

    tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imagePath:@"waightTrainingViewBg.png"]];
    
    [[PropertyDirector sharedDirector].adView setFrame:CGRectMake(0, APP_HEIGHT-50-44, APP_WIDTH, 50)];
    [self.view addSubview:[PropertyDirector sharedDirector].adView];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addSetNum:) name:ADD_SET_NUM object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(chooseCount:) name:CHOOSE_COUNT object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setTrainingNum:) name:SET_TRAINING_NUM object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(chooseCalenderDate:) name:CHOOSE_CALENDER_DATE object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(chooseCalenderMenuDate:) name:CHOOSE_CALENDER_MENU_DATE object:nil];

    
}

- (void)chooseCalenderDate:(NSNotification*)notification
{
    NSDictionary *userInfo = notification.userInfo;
    self.setDateTime = [[userInfo objectForKey:@"date"]longValue];
    
    [self reload];
}

- (void)chooseCalenderMenuDate:(NSNotification*)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval fromCopyDateTime = [[userInfo objectForKey:@"date"]longValue];
 
    [[SQLiteManager shardManager]addSameTrainingMenuFromDate:fromCopyDateTime toDate:self.setDateTime];

    [self reload];
}


- (void)chooseCount:(NSNotification*)notification
{
    NSDictionary *userInfo = notification.userInfo;
    UIPickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"trainingPickerView"];
    pickerViewController.unit = [userInfo objectForKey:@"unit"];
    pickerViewController.num = [userInfo objectForKey:@"num"];
    pickerViewController.indexPath = [userInfo objectForKey:@"indexPath"];
    
    [self.navigationController pushViewController:pickerViewController animated:YES];
}

- (void)setTrainingNum:(NSNotification*)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSIndexPath *indexPath = [userInfo objectForKey:@"indexPath"];
    NSString *unit = [userInfo objectForKey:@"unit"];
    
    WeightTrainingCell *cell = (WeightTrainingCell*)[tableView cellForRowAtIndexPath:indexPath];
    int detailId = [[[[dataSource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row]objectForKey:@"detail_id"]intValue];
    if ([unit isEqualToString:@"kg"])
    {
        [[SQLiteManager shardManager]setTrainingDetailKg:[[userInfo objectForKey:@"num"]floatValue] detailId:detailId];
        [cell setWeight:[[userInfo objectForKey:@"num"]floatValue]];
    }
    else if ([unit isEqualToString:@"回"])
    {
        [[SQLiteManager shardManager]setTrainingDetailTimes:[[userInfo objectForKey:@"num"]intValue] detailId:detailId];
        [cell setCount:[[userInfo objectForKey:@"num"]intValue]];
    }
    
    dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];
}

- (void)addSetNum:(NSNotification*)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSIndexPath *indexPath = [userInfo objectForKey:@"indexPath"];
    
    NSDictionary *lastRowData = [[dataSource objectAtIndex:indexPath.section]lastObject];
    [[SQLiteManager shardManager]addTrainingDetailWithTrainingId:[[lastRowData objectForKey:@"training_id"]intValue]];
    dataSource = [[SQLiteManager shardManager]traingDataAtTime:self.setDateTime];

    NSIndexPath *addIndexPath = [NSIndexPath indexPathForRow:[[dataSource objectAtIndex:indexPath.section]count] -1 inSection:indexPath.section];
    [tableView insertRowsAtIndexPaths:@[addIndexPath] withRowAnimation:UITableViewRowAnimationFade];
}


- (IBAction)editSection:(id)sender {
    if (!tableView.editing && [dataSource count] == 0)
    {
        return;
    }

    tableView.editing = !tableView.editing;
    isSectionEditing = tableView.editing;
    if (tableView.editing)
    {
        [editSectionBtn setImage:[UIImage imagePath:@"endEditBtn.png"] forState:UIControlStateNormal];
    }
    else
    {
        [editSectionBtn setImage:[UIImage imagePath:@"editBtn.png"] forState:UIControlStateNormal];
    }
    [tableView reloadData];
}

- (void)cancelEditing
{
    isSectionEditing = NO;
    tableView.editing = NO;
    [editSectionBtn setImage:[UIImage imagePath:@"editBtn.png"] forState:UIControlStateNormal];
}

- (IBAction)prevDate:(id)sender {
    self.setDateTime -= 60*60*24;
    
    [self reload];
}

- (IBAction)nextDate:(id)sender {
    self.setDateTime += 60*60*24;
    
    [self reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}

- (void)dealloc
{
    dataSource = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self cancelEditing];

    if ([[segue destinationViewController]isKindOfClass:[CalenderViewController class]])
    {
        CalenderViewController *calenderView = [segue destinationViewController];
        if ([((UIButton*)sender).titleLabel.text isEqualToString:@"前回と同じメニューをセットする"])
        {
            calenderView.calenderType = kCalenderForChooseTrainingMenu;
        }
        else
        {
            calenderView.calenderType = kCalenderForDisplayTraining;
        }
    }
    else if ([[segue destinationViewController]isKindOfClass:[WeightTypeViewController class]])
    {
        WeightTypeViewController *typeView = [segue destinationViewController];
        typeView.weightView = self;
        typeView.setDateTime = self.setDateTime;
    }
}


@end
