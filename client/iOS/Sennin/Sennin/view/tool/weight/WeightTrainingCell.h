//
//  WeightTrainingCell.h
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeightTrainingCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *setNumLabel;
@property (strong, nonatomic) IBOutlet UIView *separatorView;

@property (retain, readwrite) NSIndexPath *indexPath;


- (void)setWeight:(float)weight;
- (void)setCount:(int)count;
@end
