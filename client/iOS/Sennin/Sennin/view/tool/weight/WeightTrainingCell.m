//
//  WeightTrainingCell.m
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "WeightTrainingCell.h"
#import "Notification.h"
#import "UIColor+WithColorCode.h"

@implementation WeightTrainingCell
{
    BOOL isDraggingBtn;

    IBOutlet UIButton *setWeightBtn;
    IBOutlet UIButton *setCountBtn;
    IBOutlet UILabel *weightLabel;
    IBOutlet UILabel *countLabel;
}

- (IBAction)chooseWeight:(id)sender {
    if (!isDraggingBtn)
    {
        NSDictionary *userInfo = @{@"indexPath":self.indexPath, @"unit":@"kg", @"num":weightLabel.text};
        [[NSNotificationCenter defaultCenter]postNotificationName:CHOOSE_COUNT object:nil userInfo:userInfo];
    }
}
- (IBAction)chooseCount:(id)sender {
    if (!isDraggingBtn)
    {
        NSDictionary *userInfo = @{@"indexPath":self.indexPath, @"unit":@"回", @"num":countLabel.text};
        [[NSNotificationCenter defaultCenter]postNotificationName:CHOOSE_COUNT object:nil userInfo:userInfo];
    }
}
- (IBAction)touchDownWeightBtn:(id)sender {
    isDraggingBtn = NO;
}
- (IBAction)touchDownCountBtn:(id)sender {
    isDraggingBtn = NO;
}
- (IBAction)movingWeightBtn:(id)sender {
    isDraggingBtn = YES;
}
- (IBAction)movingCountBtn:(id)sender {
    isDraggingBtn = YES;
}


- (void)setWeight:(float)weight
{
    weightLabel.text = [NSString stringWithFormat:@"%3.2f", weight];
    if (weight == 0)
    {
        setWeightBtn.backgroundColor = [UIColor colorWithColorCode:0xF0F0F0];
    }
    else
    {
        setWeightBtn.backgroundColor = [UIColor clearColor];
    }
}


- (void)setCount:(int)count
{
    countLabel.text = [NSString stringWithFormat:@"%d", count];
    if (count == 0)
    {
        setCountBtn.backgroundColor = [UIColor colorWithColorCode:0xF0F0F0];
    }
    else
    {
        setCountBtn.backgroundColor = [UIColor clearColor];
    }
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    self.indexPath = nil;
}

@end
