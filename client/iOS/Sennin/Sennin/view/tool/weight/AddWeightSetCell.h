//
//  AddWeightSetCell.h
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddWeightSetCell : UITableViewCell

@property (retain, readwrite) NSIndexPath *indexPath;
@end
