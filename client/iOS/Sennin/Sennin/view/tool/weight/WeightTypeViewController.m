//
//  WeightTypeViewController.m
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "WeightTypeViewController.h"
#import "WeightChooserViewController.h"
#import "Config.h"
#import "UIColor+WithColorCode.h"
#import "WeightTrainingViewController.h"
#import "SQLiteManager.h"
#import "PropertyDirector.h"


@interface WeightTypeViewController ()

@end

@implementation WeightTypeViewController
{
    WeightChooserViewController *chooserView;

    IBOutlet UITableView *tableView;
    IBOutlet UILabel *setDateLabel;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.partsId = -1;
    self.menuId = -1;
    self.optionId = 1;
    self.setNum = 5;
    
    setDateLabel.text = [[PropertyDirector sharedDirector]getDateStringWithTimeInterval:self.setDateTime];
}

- (IBAction)addMenu:(id)sender
{
    if (self.partsId == -1)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"部位を選択して下さい" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    else if (self.menuId == -1)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"メニューを選択して下さい" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

        return;
    }
    
    
    [[SQLiteManager shardManager]addTrainingMenuAtDate:self.setDateTime menuId:self.menuId optionId:self.optionId times:self.setNum];
//    
//    NSString *partsName = [[SQLiteManager shardManager]bodyNameWithPartsId:self.partsId];
//    NSString *menuName = [[SQLiteManager shardManager]trainingMenuNameWithMenuId:self.menuId];
//    NSString *optionName = (self.optionId == 1) ? @"" : [NSString stringWithFormat:@"[%@]", [[SQLiteManager shardManager]trainingOptionNameWithOptionId:self.optionId]];

//    [self.weightView.sectionDataSource addObject:[NSString stringWithFormat:@"%@ : %@ %@", partsName, menuName, optionName]];
//    [self.weightView.rowDataSource addObject:@(self.setNum)];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        // メニューの種類
        return 3;
    }
    else
    {
        // セット数
        return 1;
    }
}

- (UITableViewCell*)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"WeightTypeCell";
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.userInteractionEnabled = YES;
    cell.detailTextLabel.textColor = [UIColor colorWithColorCode:BASE_COLOR];
    
    if (indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 0:
                if (self.partsId == -1)
                {
                    cell.detailTextLabel.textColor = [UIColor redColor];
                }

                cell.textLabel.text = @"部位";
                cell.detailTextLabel.text = [[SQLiteManager shardManager]bodyNameWithPartsId:self.partsId];;
                break;
            case 1:
                cell.textLabel.text = @"メニュー";
                if (self.partsId == -1)
                {
                    cell.userInteractionEnabled = NO;
                    cell.detailTextLabel.text = @"「部位」を選んで下さい";
                    cell.detailTextLabel.textColor = [UIColor redColor];
                }
                else if (self.menuId == -1)
                {
                    cell.detailTextLabel.textColor = [UIColor redColor];
                    cell.detailTextLabel.text = [[SQLiteManager shardManager]trainingMenuNameWithMenuId:self.menuId];
                }
                else
                {
                    cell.detailTextLabel.text = [[SQLiteManager shardManager]trainingMenuNameWithMenuId:self.menuId];
                }
                break;
            case 2:
            {
                cell.textLabel.text = @"備考";
                NSString *optionName = [[SQLiteManager shardManager]trainingOptionNameWithOptionId:self.optionId];
                cell.detailTextLabel.text = optionName;
                break;
            }
            default:
                break;
        }
    }
    else
    {
        cell.textLabel.text = @"セット数";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d 回", self.setNum];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;

    int weightChooserType;
    if (indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 0:
                // 部位
                weightChooserType = WeightChooserTypeParts;
                break;
            case 1:
                // メニュー
                weightChooserType = WeightChooserTypeMenu;
                break;
            case 2:
                // 備考
                weightChooserType = WeightChooserTypeOption;
                break;
            default:
                break;
        }
    }
    else
    {
        // セット数
        weightChooserType = WeightChooserTypeSetNum;
    }
    
    chooserView.weightChooserType = weightChooserType;
    chooserView.partsId = self.partsId;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController]isKindOfClass:[WeightChooserViewController class]])
    {
        chooserView = [segue destinationViewController];
    }
}


@end
