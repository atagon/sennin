//
//  UIPickerViewController.m
//  Sennin
//
//  Created by abt on 2014/07/04.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "UIPickerViewController.h"
#import "Notification.h"

@interface UIPickerViewController ()

@end

@implementation UIPickerViewController
{
    IBOutlet UILabel *unitLabel;
    IBOutlet UILabel *numberLabel;
    IBOutlet UIPickerView *pickerView;
}

- (IBAction)clickOK:(id)sender {
    NSDictionary *userInfo = @{@"indexPath":self.indexPath,
                               @"unit":unitLabel.text,
                               @"num": numberLabel.text,
                               };
    [[NSNotificationCenter defaultCenter]postNotificationName:SET_TRAINING_NUM object:nil userInfo:userInfo];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    unitLabel.text = self.unit;
    numberLabel.text = self.num;
    
    if ([self.unit isEqualToString:@"kg"])
    {
        int row = (int)([numberLabel.text floatValue]/0.25);
        [pickerView selectRow:row inComponent:0 animated:NO];
    }
    else
    {
        int row = [numberLabel.text intValue];
        [pickerView selectRow:row inComponent:0 animated:NO];
    }
}


// UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([unitLabel.text isEqualToString:@"kg"])
    {
        return 1200;
    }
    else
    {
        return 1000;
    }
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([unitLabel.text isEqualToString:@"kg"])
    {
        return [NSString stringWithFormat:@"%3.2f", row * 0.25];
    }
    else
    {
        return [NSString stringWithFormat:@"%d", row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([unitLabel.text isEqualToString:@"kg"])
    {
        numberLabel.text = [NSString stringWithFormat:@"%3.2f", row * 0.25];
    }
    else
    {
        numberLabel.text = [NSString stringWithFormat:@"%d", row];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
