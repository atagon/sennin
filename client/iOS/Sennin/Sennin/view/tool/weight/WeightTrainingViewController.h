//
//  WeightTrainingViewController.h
//  Sennin
//
//  Created by abt on 2014/06/30.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"


@interface WeightTrainingViewController : CommonViewController <UITableViewDelegate, UITableViewDataSource>

@property(assign, readwrite) NSTimeInterval setDateTime;

@end
