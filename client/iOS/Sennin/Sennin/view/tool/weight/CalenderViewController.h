//
//  CalenderViewController.h
//  Sennin
//
//  Created by abt on 2014/07/03.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVCalendarViewController.h"

#define kCalenderForDisplayTraining 1
#define kCalenderForChooseTrainingMenu 2


@interface CalenderViewController : RDVCalendarViewController

@property(assign, readwrite) int calenderType;

@end
