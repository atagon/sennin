//
//  AddWeightSetCell.m
//  Sennin
//
//  Created by abt on 2014/07/01.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "AddWeightSetCell.h"
#import "Notification.h"

@implementation AddWeightSetCell
{
    
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (IBAction)addSet:(id)sender
{
    NSDictionary *userInfo = @{@"indexPath":self.indexPath};
    [[NSNotificationCenter defaultCenter]postNotificationName:ADD_SET_NUM object:nil userInfo:userInfo];
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    self.indexPath = nil;
}
@end
