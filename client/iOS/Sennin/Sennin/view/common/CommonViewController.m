//
//  CommonViewController.m
//  Sennin
//
//  Created by abt on 2014/04/13.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController ()

@end

@implementation CommonViewController


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGesture:)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeGesture];
}

- (void)swipeGesture:(UISwipeGestureRecognizer*)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
