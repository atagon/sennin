//
//  AppDelegate.h
//  Sennin
//
//  Created by abt on 2014/03/29.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FelloPush/IKonectNotificationsCallback.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, IKonectNotificationsCallback>

@property (strong, nonatomic) UIWindow *window;

@end
