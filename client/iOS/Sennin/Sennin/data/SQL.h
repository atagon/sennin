//
//  SQL.h
//  Sennin
//
//  Created by abt on 2014/04/07.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#ifndef Sennin_SQL_h
#define Sennin_SQL_h

#define GET_SYSTEM_VERSION @"SELECT version FROM SYSTEMINFO WHERE id = 1;"
#define VACUUM @"VACUUM;"
#define UPDATE_VERSION @"UPDATE SYSTEMINFO SET version = ?"
#define INSERT_FIRST_SYSTEMINFO @"INSERT INTO SYSTEMINFO(version) VALUES(2.0);"
// ALTER TABLE
#define ALTER_1_1_0 @"ALTER TABLE WEBCACHE ADD COLUMN percent INTEGER DEFAULT 0;"
#define ALTER_1_2_0 @"ALTER TABLE WEBCACHE ADD COLUMN is_deleted INTEGER DEFAULT 0;"

// TRAINING TOOL
#define SELECT_BODY_PARTS @"SELECT name, parts_id FROM BODY_PARTS ORDER BY disp_order;"
#define SELECT_BODY_NAME @"SELECT name FROM BODY_PARTS WHERE parts_id=?;"
#define SELECT_BODY_NAME_WITH_MENU_ID @"SELECT A.name FROM BODY_PARTS A, TRAINING_MENU B WHERE B.menu_id=? AND A.parts_id = B.parts_id;"
#define SELECT_TRAINING_MENU @"SELECT parts_id, menu_id, name FROM TRAINING_MENU WHERE parts_id=? AND is_my_menu=0 ORDER BY disp_order;"
#define SELECT_TRAINING_MENU_WITH_DATE @"SELECT * FROM USER_TRAINING WHERE year = ? AND month = ? AND day = ? ORDER BY year desc, month desc, day desc, disp_order;"
#define SELECT_MENU_NAME @"SELECT name FROM TRAINING_MENU WHERE menu_id=?;"
#define SELECT_TRAINING_OPTION @"SELECT name, option_id FROM TRAINING_OPTION WHERE is_my_option=0 ORDER BY disp_order;"
#define SELECT_OPTION_NAME @"SELECT name FROM TRAINING_OPTION WHERE option_id=?;"
#define SELECT_MAX_TRAINING_ORDER @"SELECT COALESCE(max(disp_order), 0) as disp_order FROM USER_TRAINING;"
#define INSERT_USER_TRAINING @"INSERT INTO USER_TRAINING(menu_id, option_id, disp_order, year, month, day) VALUES(?,?,?,?,?,?);"
#define SELECT_PREV_TRAINING_MENU @"SELECT * FROM USER_TRAINING WHERE year <= ? AND month <= ? ORDER BY year desc, month desc, day desc, disp_order limit 30;"
#define SELECT_MAX_TRAINING_DETAIL_ORDER @"SELECT COALESCE(max(disp_order), 0) as disp_order FROM USER_TRAINING_DETAIL WHERE training_id=?;"
#define SELECT_TRAINING_ID @"SELECT max(training_id) as training_id FROM USER_TRAINING WHERE year=? AND month=? AND day=?;"
#define INSERT_USER_TRAINING_DETAIL @"INSERT INTO USER_TRAINING_DETAIL(training_id, disp_order, kg) VALUES(?, ?, ?);"
#define SELECT_TRAINING_DATA @"SELECT A.training_id, A.menu_id, A.option_id,  B.detail_id, B.kg, B.times, A.disp_order as training_disp_order, B.disp_order as detail_disp_order FROM USER_TRAINING A, USER_TRAINING_DETAIL B WHERE A.year=? AND A.month=? AND A.day=? AND A.training_id = B.training_id ORDER BY A.disp_order, B.disp_order;"
#define SELECT_TRAINING_DETAIL_DATA_WITH_TRAINING_ID @"SELECT * FROM USER_TRAINING_DETAIL WHERE training_id=? ORDER BY disp_order;"
#define DELETE_TRAINING_DETAIL @"DELETE FROM USER_TRAINING_DETAIL WHERE detail_id=?;"
#define DELETE_TRAINING @"DELETE FROM USER_TRAINING WHERE training_id=?;"
#define DELETE_TRAINING_DETAIL_WITH_TRAINING_ID @"DELETE FROM USER_TRAINING_DETAIL WHERE training_id=?;"
#define UPDATE_TRAINING_DETAIL_KG @"UPDATE USER_TRAINING_DETAIL SET kg=? WHERE detail_id=?;"
#define UPDATE_TRAINING_DETAIL_TIMES @"UPDATE USER_TRAINING_DETAIL SET times=? WHERE detail_id=?;"
#define SELECT_TRAINING_DAYS @"SELECT day FROM USER_TRAINING WHERE year=? AND month=? AND day between 1 AND 31;"
#define UPDATE_TRAINING_ORDER @"UPDATE USER_TRAINING SET disp_order = ? WHERE training_id=?;"

// BLOG
#define RESET_BLOG_DATA @"DELETE FROM WEBCACHE;"
#define IS_EXIST_URL @"SELECT COUNT(url) as count FROM WEBCACHE WHERE url=?;"
#define DELETE_ARTICLE_WITH_URL @"DELETE FROM WEBCACHE WHERE url=?;"
#define SELECT_LASTDATE @"SELECT COALESCE(max(update_time), 0) as lastdate FROM WEBCACHE;"
#define UPDATE_ARTICLE_TITILE @"UPDATE WEBCACHE set title=?, thumbnail=?, type=?, update_time=?, create_time=?, is_deleted=?, html='', contents='' WHERE url=?;"
#define INSERT_ARTICLE_TITILE @"INSERT INTO WEBCACHE(url, title, thumbnail, type, new, create_time, update_time, is_deleted) VALUES(?,?,?,?,?,?,?,?);"
#define SELECT_ARTICLE_WITH_URL @"SELECT * FROM WEBCACHE WHERE url like ?;"
#define REGISTER_ARTICLE @"UPDATE WEBCACHE SET html=?, contents=? WHERE url=?;"
#define SELECT_ARTICLE_TITLES @"SELECT title, type, url, percent FROM WEBCACHE WHERE is_deleted = 0 AND title is not NULL ORDER BY create_time desc;"
#define SELECT_THUMBNAIL @"SELECT thumbnail FROM WEBCACHE WHERE url=? AND thumbnail is not NULL;"

#define SEARCH_ARTICLE @"SELECT title, type, url FROM WEBCACHE WHERE contents like ?;"

#define SET_READ @"UPDATE WEBCACHE SET read=1 WHERE url like ?;"
#define SET_UNNEW @"UPDATE WEBCACHE SET new=0 WHERE url like ?;"
#define IS_NEW @"SELECT new FROM WEBCACHE WHERE url=?;"

#define RESET_NEW @"UPDATE WEBCACHE SET new=0;"
#define SET_READ_PROGRESS @"UPDATE WEBCACHE SET percent=? WHERE url=?;"
#endif
