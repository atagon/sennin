//
//  Notification.h
//  Sennin
//
//  Created by abt on 2014/04/13.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#ifndef Sennin_Notification_h
#define Sennin_Notification_h

#define UPDATE_ARTICLE @"UPDATE_ARTICLE"
#define ADD_SET_NUM @"ADD_SET_NUM"
#define CHOOSE_COUNT @"CHOOSE_COUNT"
#define SET_TRAINING_NUM @"SET_TRAINING_NUM"
#define CHOOSE_CALENDER_DATE @"CHOOSE_CALENDER_DATE"
#define CHOOSE_CALENDER_MENU_DATE @"CHOOSE_CALENDER_MENU_DATE"
#endif
