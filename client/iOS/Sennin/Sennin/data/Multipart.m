//
//  Multipart.m
//  Sennin
//
//  Created by abt on 2014/04/14.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "Multipart.h"

@implementation Multipart

- (id)init
{
	self = [super init];
	if (self) {
		items = [[NSMutableDictionary alloc] init];
		bound = @"----------bound";
	}
	return self;
}

// Data追加
- (void)addData:(NSData *)data forKey:(NSString *)key
{
	[items setObject:data forKey:key];
}

// String追加
- (void)addString:(NSString *)string forKey:(NSString *)key
{
	[self addData:[string dataUsingEncoding:NSUTF8StringEncoding] forKey:key];
}

// itemが1個以上あるか
- (BOOL)hasItems
{
	return 0<[items count];
}

// ContentType取得
- (NSString *)contentType
{
	return [NSString stringWithFormat:@"multipart/form-data; boundary=%@", bound];
}

// PostBody取得
- (NSData *)body
{
	NSMutableData *data = [NSMutableData data];
	for (id key in items) {
		NSData *value = [items objectForKey:key];
		[data appendData:[[NSString stringWithFormat:@"--%@\r\n", bound] dataUsingEncoding:NSUTF8StringEncoding]];
		[data appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
		[data appendData:[[NSString stringWithFormat:@"Content-Length: %d\r\n", [value length]] dataUsingEncoding:NSUTF8StringEncoding]];
		[data appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
		[data appendData:value];
		[data appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	}
	[data appendData:[[NSString stringWithFormat:@"--%@--\r\n", bound] dataUsingEncoding:NSUTF8StringEncoding]];
	return data;
}


@end
