//
//  SQLiteManager.h
//  Sennin
//
//  Created by abt on 2014/04/07.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#define DB_PATH @"Sennin.sqlite"

@interface SQLiteManager : NSObject

+(SQLiteManager*)shardManager;

// システムバージョンを取得する
-(int)getSystemVersion;
-(void)updateSystemVersion:(int)version;
-(void)alterTable1_1;
-(void)alterTable1_2;
-(void)DBUpdate_ver4;

-(FMDatabase*)openDB;
-(NSArray*)select:(NSString*)sql;
-(NSArray*)select:(NSString*)sql arguments:(NSArray*)arguments;
-(BOOL)update:(NSString*)sql arguments:(NSArray*)arguments;
-(BOOL)update:(NSString*)sql;

// TOOL:TRAINING
- (NSArray*)bodyParts;
- (NSString*)bodyNameWithMenuId:(int)menuId;
- (NSString*)bodyNameWithPartsId:(int)partsId;
- (NSArray*)trainingMenuWithPartsId:(int)partsId;
- (NSString*)trainingMenuNameWithMenuId:(int)menuId;
- (NSArray*)trainingOption;
- (NSString*)trainingOptionNameWithOptionId:(int)optionId;
- (void)addTrainingMenuAtDate:(NSTimeInterval)time menuId:(int)menuId optionId:(int)optionId times:(int)times;
- (void)addSameTrainingMenuAtDate:(NSTimeInterval)time;
- (void)addSameTrainingMenuFromDate:(NSTimeInterval)fromDate toDate:(NSTimeInterval)toDate;
- (NSArray*)traingDataAtTime:(NSTimeInterval)time;
- (void)deleteTrainingDetail:(int)detailId;
- (void)deleteTraining:(int)trainingId;
- (void)addTrainingDetailWithTrainingId:(int)trainingId;
- (void)setTrainingDetailKg:(float)kg detailId:(int)detailId;
- (void)setTrainingDetailTimes:(int)times detailId:(int)detailId;
- (NSArray*)trainingDaysWithYear:(int)year month:(int)month;
- (void)exchangeTrainingWithFromTrainingId:(int)fromTrainingId fromOrder:(int)fromOrder toTrainingId:(int)toTrainingId toOrder:(int)toOrder;

// BLOG
- (BOOL)isExistURL:(NSString*)url;
- (NSNumber*)getlastDate;
- (NSArray*)articleTitles;
- (void)updateReadPercentWithURL:(NSString*)url percent:(int)percent;
- (NSDictionary*)articleWithURL:(NSString*)url;
- (void)insertArticleTitleWithURL:(NSString*)url title:(NSString*)title thumbnail:(NSString*)thumbnail type:(NSString*)type isNew:(BOOL)isNew createTime:(NSNumber*)createTime updateTime:(NSNumber*)updateTime isDeleted:(int)isDeleted;
- (void)registerArticleHTML:(NSString*)html contents:(NSString*)contents url:(NSString*)url;
- (NSString*)thumbnailPathWithURL:(NSString*)url;

- (NSArray*)searchTodoWithKeyword:(NSString*)keyword;
- (void)setReadWithURL:(NSString*)url;
- (void)setUnNewWithURL:(NSString*)url;

- (BOOL)isNewWithURL:(NSString*)url;

// 記事のnewを全てリセットする
- (void)resetNewArticle;
@end
