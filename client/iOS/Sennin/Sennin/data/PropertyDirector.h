//
//  PropertyDirector.h
//  Sennin
//
//  Created by abt on 2014/04/06.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdView.h"

@interface PropertyDirector : NSObject

+(PropertyDirector*)sharedDirector;

- (void)setTopImageOfArticle:(NSString*)url imageView:(UIImageView*)imageView;

// GoogleAnalitics
-(void)sendGoogleAnalitics:(NSString*)screenName;
-(void)sendGoogleAnaliticsActionCategory:(NSString*)category action:(NSString*)action label:(NSString*)label;

-(NSDictionary*)timeParametersByInterval:(NSTimeInterval)timer;
-(NSString*)getDateStringWithTimeInterval:(NSTimeInterval)time;
-(NSString*)getDateStringWithDate:(NSDate*)date;
-(long)getDateNumber:(NSDate*)date;

@property(retain, readwrite) AdView *adView;
@property(assign, readwrite) NSString *uuid;
@property(assign, readwrite) BOOL isNotFirstLaunch;
@property(assign, readwrite) int lastSelectedTabIndex;
@end
