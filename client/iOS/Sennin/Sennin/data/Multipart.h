//
//  Multipart.h
//  Sennin
//
//  Created by abt on 2014/04/14.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Multipart : NSObject {
	NSString *bound;
	NSMutableDictionary *items;
}

- (void)addData:(NSData *)data forKey:(NSString *)key;
- (void)addString:(NSString *)string forKey:(NSString *)key;

- (BOOL)hasItems;
- (NSString *)contentType;
- (NSData *)body;

@end
