//
//  SQLiteManager.m
//  Sennin
//
//  Created by abt on 2014/04/07.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "SQLiteManager.h"
#import "SQL.h"
#import "Config.h"
#import "PropertyDirector.h"
#import "JMImageCache.h"

static SQLiteManager* manager;

@implementation SQLiteManager

+(SQLiteManager*)shardManager
{
    if (manager == nil)
    {
        manager = [[self alloc]init];
    }
    
    return manager;
}


-(FMDatabase*)openDB
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSError* error;
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* writableDBPath = [documentsDirectory stringByAppendingPathComponent:DB_PATH];
    BOOL success = [fileManager fileExistsAtPath:writableDBPath];
    if (!success)
    {
        NSString* defaultDBPath = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:DB_PATH];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    }
    if (!success)
    {
        NSAssert1(0, @"failed to create writable db file with message '%@'.", [error localizedDescription]);
    }
    
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if (![db open])
    {
        NSLog(@"DB Open Error!");
        return nil;
    }
    else {
        return db;
    }
}


-(NSArray*)select:(NSString*)sql
{
    return [self select:sql arguments:nil];
}


-(NSArray*)select:(NSString*)sql arguments:(NSArray*)arguments
{
    FMDatabase* db = [self openDB];
    NSMutableArray* resultArray;
    if (db == nil)
    {
        return nil;
    }
    else
    {
        FMResultSet* result;
        if (arguments != nil)
        {
            result = [db executeQuery:sql withArgumentsInArray:arguments];
        }
        else
        {
            result = [db executeQuery:sql];
        }
        
        resultArray = [NSMutableArray array];
        while ([result next]) {
            [resultArray addObject:[result resultDictionary]];
        }
        [result close];
        [db close];
    }
    
    return resultArray;
}


-(BOOL)update:(NSString*)sql
{
    return [self update:sql arguments:nil];
}

-(BOOL)update:(NSString*)sql arguments:(NSArray*)arguments
{
    FMDatabase* db = [self openDB];
    BOOL result;
    if (db == nil)
    {
        result = NO;
    }
    else
    {
        if (arguments == nil)
        {
            result = [db executeUpdate:sql];
        }
        else
        {
            result = [db executeUpdate:sql withArgumentsInArray:arguments];
        }
        [db close];
    }
    
    return result;
}



// システムバージョンを取得する
-(int)getSystemVersion
{
    NSArray* result = [self select:GET_SYSTEM_VERSION];
    if ([result count] > 0)
    {
        return [[[result objectAtIndex:0]objectForKey:@"version"]intValue];
    }
    else
    {
        return 1;
    }
}
// システムのバージョンを更新する
-(void)updateSystemVersion:(int)version
{
    NSArray* arguments = [NSArray arrayWithObjects:
                          [NSNumber numberWithInt:version],
                          nil];
    [self update:UPDATE_VERSION arguments:arguments];
}


-(void)alterTable1_1
{
    // SYSTEMINFOにpercentを追加する
    [[SQLiteManager shardManager]update:ALTER_1_1_0];
    
    // VACCUME
    [[SQLiteManager shardManager]update:VACUUM arguments:nil];
    
    // システムバージョンを2に更新
    [[SQLiteManager shardManager]update:INSERT_FIRST_SYSTEMINFO];
}

-(void)alterTable1_2
{
    // SYSTEMINFOにpercentを追加する
    [[SQLiteManager shardManager]update:ALTER_1_2_0];
    
    // VACCUME
    [[SQLiteManager shardManager]update:VACUUM arguments:nil];
    
    // 間違ったキャッシュを消す
    [[SQLiteManager shardManager]update:DELETE_ARTICLE_WITH_URL arguments:@[@"http://urban-ascetic.com/exercise/wood%e2%80%90chopping/"]];

    // システムバージョンを3に更新
    [[SQLiteManager shardManager]update:UPDATE_VERSION arguments:@[@(3)]];
}


-(void)DBUpdate_ver4
{
    // 記事データを全て消す
    [[SQLiteManager shardManager]update:RESET_BLOG_DATA];
    
    // システムバージョンを4に更新
    [[SQLiteManager shardManager]update:UPDATE_VERSION arguments:@[@(4)]];
}


- (BOOL)isExistURL:(NSString *)url
{
    NSArray* result = [self select:IS_EXIST_URL arguments:@[url]];
    if ([result count] > 0)
    {
        int count = [[[result objectAtIndex:0]objectForKey:@"count"]intValue];
        return (count > 0);
    } else {
        return NO;
    }
}


// TOOL:TRAINING
- (NSArray*)bodyParts
{
    return [self select:SELECT_BODY_PARTS];
}
- (NSString*)bodyNameWithPartsId:(int)partsId
{
    NSArray *arguments = @[@(partsId)];
    NSArray *result = [self select:SELECT_BODY_NAME arguments:arguments];
    if ([result count] > 0)
    {
        return [[result objectAtIndex:0]objectForKey:@"name"];
    }
    else
    {
        return @"未選択";
    }
}
- (NSString*)bodyNameWithMenuId:(int)menuId
{
    NSArray *arguments = @[@(menuId)];
    NSArray *result = [self select:SELECT_BODY_NAME_WITH_MENU_ID arguments:arguments];
    if ([result count] > 0)
    {
        return [[result objectAtIndex:0]objectForKey:@"name"];
    }
    else
    {
        return @"エラー";
    }
}


- (NSArray*)trainingMenuWithPartsId:(int)partsId
{
    NSArray *arguments = @[@(partsId)];
    return [self select:SELECT_TRAINING_MENU arguments:arguments];
}
- (NSString*)trainingMenuNameWithMenuId:(int)menuId
{
    NSArray *arguments = @[@(menuId)];
    NSArray *result = [self select:SELECT_MENU_NAME arguments:arguments];
    if ([result count] > 0)
    {
        return [[result objectAtIndex:0]objectForKey:@"name"];
    }
    else
    {
        return @"未選択";
    }
}

- (NSArray*)trainingOption
{
    return [self select:SELECT_TRAINING_OPTION];
}
- (NSString*)trainingOptionNameWithOptionId:(int)optionId
{
    NSArray *arguments = @[@(optionId)];
    NSArray *result = [self select:SELECT_OPTION_NAME arguments:arguments];
    if ([result count] > 0)
    {
        return [[result objectAtIndex:0]objectForKey:@"name"];
    }
    else
    {
        return @"なし";
    }
}

- (void)addTrainingMenuAtDate:(NSTimeInterval)time menuId:(int)menuId optionId:(int)optionId times:(int)times
{
    int trainingOrder = [[[[self select:SELECT_MAX_TRAINING_ORDER]objectAtIndex:0]objectForKey:@"disp_order"]intValue]+1;
    // USER_TRAININGへのinsert
    NSDictionary *dateParams = [[PropertyDirector sharedDirector]timeParametersByInterval:time];
    NSArray *arguments = @[@(menuId),@(optionId),@(trainingOrder),[dateParams objectForKey:@"year"],[dateParams objectForKey:@"month"],[dateParams objectForKey:@"day"]];
    [self update:INSERT_USER_TRAINING arguments:arguments];
    
    
    int trainingId = [[[[self select:SELECT_TRAINING_ID arguments:@[[dateParams objectForKey:@"year"],[dateParams objectForKey:@"month"],[dateParams objectForKey:@"day"]]]objectAtIndex:0]objectForKey:@"training_id"]intValue];
    // USER_TRAINING_DETAILへのinsert
    for (int i=0; i<times; i++)
    {
        int trainingDetailOrder = [[[[self select:SELECT_MAX_TRAINING_DETAIL_ORDER arguments:@[@(trainingId)]]objectAtIndex:0]objectForKey:@"disp_order"]intValue]+1;
        NSArray *arguments = @[@(trainingId), @(trainingDetailOrder), @(0)];
        [self update:INSERT_USER_TRAINING_DETAIL arguments:arguments];
    }
}

//- (void)addSameTrainingMenuAtDate:(NSTimeInterval)time
//{
//    NSDictionary *dateParams = [[PropertyDirector sharedDirector]timeParametersByInterval:time];
//    NSArray *arguments = @[[dateParams objectForKey:@"year"],[dateParams objectForKey:@"month"]];
//    NSArray *prevDataResult = [self select:SELECT_PREV_TRAINING_MENU arguments:arguments];
//    
//    if ([prevDataResult count] > 0)
//    {
//        int i=0;
//        NSDictionary *prevTrainingData = [prevDataResult objectAtIndex:i++];
//        int year = [[prevTrainingData objectForKey:@"year"]intValue];
//        int month = [[prevTrainingData objectForKey:@"month"]intValue];
//        int day = [[prevTrainingData objectForKey:@"day"]intValue];
//        
//        while ((year == [[prevTrainingData objectForKey:@"year"]intValue])
//               && (month == [[prevTrainingData objectForKey:@"month"]intValue])
//               && (day == [[prevTrainingData objectForKey:@"day"]intValue]))
//        {
//            NSArray *arguments = @[
//                                   [prevTrainingData objectForKey:@"menu_id"],
//                                   [prevTrainingData objectForKey:@"option_id"],
//                                   [prevTrainingData objectForKey:@"disp_order"],
//                                   [dateParams objectForKey:@"year"],
//                                   [dateParams objectForKey:@"month"],
//                                   [dateParams objectForKey:@"day"],
//                                   ];
//            [self update:INSERT_USER_TRAINING arguments:arguments];
//
//            
//            NSArray *arguments2 = @[[prevTrainingData objectForKey:@"training_id"]];
//            NSArray *prevTrainingDetailResult = [self select:SELECT_TRAINING_DETAIL_DATA_WITH_TRAINING_ID arguments:arguments2];
//            
//            int trainingId = [[[[self select:SELECT_TRAINING_ID arguments:@[[dateParams objectForKey:@"year"],[dateParams objectForKey:@"month"],[dateParams objectForKey:@"day"]]]objectAtIndex:0]objectForKey:@"training_id"]intValue];
//            for (int j=0; j<[prevTrainingDetailResult count]; j++)
//            {
//                NSDictionary *prevDetailData = [prevTrainingDetailResult objectAtIndex:j];
//                NSArray *arguments = @[@(trainingId), [prevDetailData objectForKey:@"disp_order"], [prevDetailData objectForKey:@"kg"]];
//                [self update:INSERT_USER_TRAINING_DETAIL arguments:arguments];
//            }
//            
//            if (i < [prevDataResult count])
//            {
//                prevTrainingData = [prevDataResult objectAtIndex:i++];
//            }
//            else
//            {
//                break;
//            }
//        }
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"過去のトレーニングデータがありませんでした。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    
//}

- (void)addSameTrainingMenuFromDate:(NSTimeInterval)fromDate toDate:(NSTimeInterval)toDate
{
    NSDictionary *fromDateParams = [[PropertyDirector sharedDirector]timeParametersByInterval:fromDate];
    NSArray *arguments = @[[fromDateParams objectForKey:@"year"],[fromDateParams objectForKey:@"month"],[fromDateParams objectForKey:@"day"]];
    NSArray *fromDataResult = [self select:SELECT_TRAINING_MENU_WITH_DATE arguments:arguments];

    NSDictionary *toDateParams = [[PropertyDirector sharedDirector]timeParametersByInterval:toDate];
    for (int i=0; i<[fromDataResult count]; i++)
    {
        NSDictionary *fromTrainingData = [fromDataResult objectAtIndex:i];
        NSArray *arguments = @[
                               [fromTrainingData objectForKey:@"menu_id"],
                               [fromTrainingData objectForKey:@"option_id"],
                               [fromTrainingData objectForKey:@"disp_order"],
                               [toDateParams objectForKey:@"year"],
                               [toDateParams objectForKey:@"month"],
                               [toDateParams objectForKey:@"day"],
                               ];
        [self update:INSERT_USER_TRAINING arguments:arguments];
        
        
        NSArray *arguments2 = @[[fromTrainingData objectForKey:@"training_id"]];
        NSArray *fromTrainingDetailResult = [self select:SELECT_TRAINING_DETAIL_DATA_WITH_TRAINING_ID arguments:arguments2];
        
        int trainingId = [[[[self select:SELECT_TRAINING_ID arguments:@[[toDateParams objectForKey:@"year"],[toDateParams objectForKey:@"month"],[toDateParams objectForKey:@"day"]]]objectAtIndex:0]objectForKey:@"training_id"]intValue];
        for (int j=0; j<[fromTrainingDetailResult count]; j++)
        {
            NSDictionary *fromDetailData = [fromTrainingDetailResult objectAtIndex:j];
            NSArray *arguments = @[@(trainingId), [fromDetailData objectForKey:@"disp_order"], [fromDetailData objectForKey:@"kg"]];
            [self update:INSERT_USER_TRAINING_DETAIL arguments:arguments];
        }
    }
}



- (void)addTrainingDetailWithTrainingId:(int)trainingId
{
    int trainingDetailOrder = [[[[self select:SELECT_MAX_TRAINING_DETAIL_ORDER arguments:@[@(trainingId)]]objectAtIndex:0]objectForKey:@"disp_order"]intValue]+1;
    NSArray *arguments = @[@(trainingId), @(trainingDetailOrder), @(0)];
    [self update:INSERT_USER_TRAINING_DETAIL arguments:arguments];
}


- (NSArray*)traingDataAtTime:(NSTimeInterval)time
{
    NSDictionary *dateParams = [[PropertyDirector sharedDirector]timeParametersByInterval:time];
    NSArray *arguments = @[[dateParams objectForKey:@"year"],[dateParams objectForKey:@"month"],[dateParams objectForKey:@"day"]];
    NSArray *result = [self select:SELECT_TRAINING_DATA arguments:arguments];
    
    NSMutableArray *resultArray = [NSMutableArray array];
    int trainingId = -1;
    NSMutableArray *rowDataArray;
    for (int i=0; i<[result count]; i++)
    {
        if (trainingId != [[[result objectAtIndex:i]objectForKey:@"training_id"]intValue])
        {
            rowDataArray = [NSMutableArray array];
            [resultArray addObject:rowDataArray];
        }
        [rowDataArray addObject:[result objectAtIndex:i]];
        
        trainingId = [[[result objectAtIndex:i]objectForKey:@"training_id"]intValue];
    }

    return resultArray;
}

- (void)deleteTrainingDetail:(int)detailId
{
    NSArray *arguments = @[@(detailId)];
    [self update:DELETE_TRAINING_DETAIL arguments:arguments];
}

- (void)deleteTraining:(int)trainingId
{
    NSArray *arguments = @[@(trainingId)];
    [self update:DELETE_TRAINING arguments:arguments];
    [self update:DELETE_TRAINING_DETAIL_WITH_TRAINING_ID arguments:arguments];
}

- (void)setTrainingDetailKg:(float)kg detailId:(int)detailId
{
    NSArray *arguments = @[@(kg),@(detailId)];
    [self update:UPDATE_TRAINING_DETAIL_KG arguments:arguments];
}

- (void)setTrainingDetailTimes:(int)times detailId:(int)detailId
{
    NSArray *arguments = @[@(times),@(detailId)];
    [self update:UPDATE_TRAINING_DETAIL_TIMES arguments:arguments];
}

- (NSArray*)trainingDaysWithYear:(int)year month:(int)month
{
    NSArray *arguments = @[@(year),@(month)];
    NSArray *result = [self select:SELECT_TRAINING_DAYS arguments:arguments];

    NSMutableArray *returnVal = [[NSMutableArray alloc]init];
    for (int i=0; i<[result count]; i++)
    {
        NSDictionary *dateData = [result objectAtIndex:i];
        [returnVal addObject:[dateData objectForKey:@"day"]];
    }
    
    return returnVal;
}

- (void)exchangeTrainingWithFromTrainingId:(int)fromTrainingId fromOrder:(int)fromOrder toTrainingId:(int)toTrainingId toOrder:(int)toOrder
{
    NSArray *argument1 = @[@(toOrder), @(fromTrainingId)];
    [self update:UPDATE_TRAINING_ORDER arguments:argument1];
    
    NSArray *argument2 = @[@(fromOrder), @(toTrainingId)];
    [self update:UPDATE_TRAINING_ORDER arguments:argument2];
}



// BLOG
- (NSArray*)articleTitles
{
    NSArray *results = [self select:SELECT_ARTICLE_TITLES];
    return results;
}

- (void)updateReadPercentWithURL:(NSString*)url percent:(int)percent
{
    NSDictionary *articleData = [self articleWithURL:url];
    int dbPercent = [[articleData objectForKey:@"percent"]intValue];
    
    if (dbPercent < percent)
    {
        int setPercent = (percent > 100) ? 100 : percent;
        NSArray *argument = @[@(setPercent), [NSString stringWithFormat:@"%@/", url]];
        [self update:SET_READ_PROGRESS arguments:argument];
    }
}


- (NSDictionary*)articleWithURL:(NSString *)url
{
    NSString *likeKeyword = [NSString stringWithFormat:@"%@%@", url, @"%%"];
    NSArray *params = @[likeKeyword];
    NSArray *results = [self select:SELECT_ARTICLE_WITH_URL arguments:params];
    if ([results count] > 0)
    {
        return [results objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}


- (void)insertArticleTitleWithURL:(NSString*)url title:(NSString*)title thumbnail:(NSString*)thumbnail type:(NSString*)type isNew:(BOOL)isNew createTime:(NSNumber*)createTime updateTime:(NSNumber*)updateTime isDeleted:(int)isDeleted
{
    NSArray *checkResults = [self select:SELECT_ARTICLE_WITH_URL arguments:@[url]];
    if ([checkResults count] > 0)
    {
        // 更新
        NSArray *params = @[
                            title,
                            thumbnail,
                            type,
                            updateTime,
                            createTime,
                            @(isDeleted),
                            url
                            ];
        [self update:UPDATE_ARTICLE_TITILE arguments:params];
        
        if (![[[checkResults objectAtIndex:0]objectForKey:@"thumbnail"]isEqualToString:thumbnail])
        {
            // サムネイルが変わっているので削除しておく
            [[JMImageCache sharedCache]removeImageForKey:url];
        }
    } else {
        int isNewArticle = (isNew)?1:0;
        NSArray *params = @[
                            url,
                            title,
                            thumbnail,
                            type,
                            @(isNewArticle),
                            createTime,
                            updateTime,
                            @(isDeleted)
                            ];
        [self update:INSERT_ARTICLE_TITILE arguments:params];
    }
}


- (void)registerArticleHTML:(NSString*)html contents:(NSString*)contents url:(NSString*)url
{
    NSArray *params = @[
                        html,
                        contents,
                        url,
                        ];
    [self update:REGISTER_ARTICLE arguments:params];
}

- (NSString*)thumbnailPathWithURL:(NSString*)url
{
    NSArray *params = @[url];
    NSArray *results = [self select:SELECT_THUMBNAIL arguments:params];
    if ([results count] > 0)
    {
        return [[results objectAtIndex:0]objectForKey:@"thumbnail"];
    }
    else
    {
        return nil;
    }
}


- (NSArray*)searchTodoWithKeyword:(NSString*)keyword
{
    if ([[[keyword stringByReplacingOccurrencesOfString:@" " withString:@""]stringByReplacingOccurrencesOfString:@"　" withString:@""] isEqualToString:@""])
    {
        // 空文字は無視
        return [NSArray array];
    }

    NSString *likeKeyword = [NSString stringWithFormat:@"%@%@%@", @"%%", keyword, @"%%"];
    NSArray *arguments = @[likeKeyword];
    return [self select:SEARCH_ARTICLE arguments:arguments];
}

- (void)setReadWithURL:(NSString*)url
{
    NSString *likeKeyword = [NSString stringWithFormat:@"%@%@%@", @"%%", url, @"%%"];
    NSArray *arguments = @[likeKeyword];

    [self update:SET_READ arguments:arguments];
}

- (void)setUnNewWithURL:(NSString*)url
{
    NSString *likeKeyword = [NSString stringWithFormat:@"%@%@%@", @"%%", url, @"%%"];
    NSArray *arguments = @[likeKeyword];

    [self update:SET_UNNEW arguments:arguments];
}

- (BOOL)isNewWithURL:(NSString*)url
{
    NSArray *params = @[url];
    NSArray *results = [self select:IS_NEW arguments:params];
    if ([[[results objectAtIndex:0]objectForKey:@"new"]intValue] == 0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)resetNewArticle
{
    [self update:RESET_NEW];
}

- (NSNumber*)getlastDate
{
    NSDictionary *resultData = [[self select:SELECT_LASTDATE]objectAtIndex:0];
    return [resultData objectForKey:@"lastdate"];
}
@end
