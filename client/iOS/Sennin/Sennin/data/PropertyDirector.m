//
//  PropertyDirector.m
//  Sennin
//
//  Created by abt on 2014/04/06.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "PropertyDirector.h"
#import "AFNetworking.h"
#import "HTMLParser.h"
#import "Config.h"
#import "JMImageCache.h"
#import "SQLiteManager.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+WithColorCode.h"


static PropertyDirector* director;

@implementation PropertyDirector


+(PropertyDirector*)sharedDirector
{
    if (director == nil)
    {
        director = [[self alloc]init];
    }
    return director;
}

/*
- (void)setTopImageOfArticle:(NSString*)url imageView:(UIImageView*)imageView
{
    CALayer *layer = [imageView layer];
    [layer setMasksToBounds:YES];
    [layer setBorderWidth:0.5];
    [layer setBorderColor:[[UIColor lightGrayColor]CGColor]];

    UIImage *image = [[JMImageCache sharedCache]cachedImageForKey:url];
    if (image != nil)
    {
//        NSLog(@"キャッシュ %@", url);
        imageView.image = image;
        return;
    }

    NSString __block *imagePath = [[SQLiteManager shardManager]thumbnailPathWithURL:url];
    if (imagePath != nil)
    {
//        NSLog(@"キャッシュ2 %@", url);
        if ([imagePath isEqualToString:BAD_IMAGE_PATH]
            || ([imagePath rangeOfString:BAD_IMAGE_HOST].location != NSNotFound))
        {
            imageView.image = [UIImage imageNamed:@"noimage.png"];
            [[JMImageCache sharedCache]setImage:[UIImage imageNamed:@"noimage.png"] forKey:url];
        }
        else
        {
            NSURL *nsURL = [NSURL URLWithString:imagePath];
            [[JMImageCache sharedCache]imageForURL:nsURL key:url completionBlock:^(UIImage *image) {
                imageView.image = image;
            }];
        }
        
        return;
    }
    
    
    
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:urlReq];
    

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *htmlData = (NSData*)responseObject;
        NSString *htmlString = [[NSString alloc]initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
        
        // 解析開始
        NSError *error = nil;
        HTMLParser *parser = [[HTMLParser alloc]initWithString:htmlString error:&error];
        HTMLNode *node = [parser body];
        NSArray *DIVs = [node findChildTags:@"div"];
        
        for (int i=0; i< [DIVs count]; i++)
        {
            HTMLNode *node = [DIVs objectAtIndex:i];
            if ([[node getAttributeNamed:@"id"] isEqualToString:@"main"])
            {
                NSArray *IMGs = [node findChildTags:@"img"];
                if ([IMGs count] > 0)
                {
                    HTMLNode *IMG = [IMGs objectAtIndex:0];
                    imagePath = [IMG getAttributeNamed:@"src"];
                    
                    if ([imagePath isEqualToString:BAD_IMAGE_PATH]
                        || ([imagePath rangeOfString:BAD_IMAGE_HOST].location != NSNotFound))
                    {
                        imageView.image = [UIImage imageNamed:@"noimage.png"];
                        [[JMImageCache sharedCache]setImage:[UIImage imageNamed:@"noimage.png"] forKey:url];
                    }
                    else
                    {
                        NSURL *nsURL = [NSURL URLWithString:imagePath];
                        [[JMImageCache sharedCache]imageForURL:nsURL key:url completionBlock:^(UIImage *image) {
                            imageView.image = image;
                        }];
                    }
                }
                else
                {
                    [[JMImageCache sharedCache]setImage:[UIImage imageNamed:@"noimage.png"] forKey:url];
                    imageView.image = [UIImage imageNamed:@"noimage.png"];
                }
                
                
                // キャッシュに登録
                [[SQLiteManager shardManager]registerArticleHTML:htmlString contents:[node allContents] thumbnail:imagePath url:url];

                break;
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error");
    }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [queue addOperation:operation];
}
 */

-(NSDictionary*)timeParametersByInterval:(NSTimeInterval)timer
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timer];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit |
                                   NSMonthCalendarUnit |
                                   NSDayCalendarUnit
                                              fromDate:date];
    
    return @{@"year":@(dateComps.year), @"month":@(dateComps.month), @"day":@(dateComps.day)};
}

-(NSString*)getDateStringWithTimeInterval:(NSTimeInterval)time
{
    return [self getDateStringWithDate:[NSDate dateWithTimeIntervalSince1970:time]];
}
-(NSString*)getDateStringWithDate:(NSDate*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit |
                                   NSMonthCalendarUnit |
                                   NSDayCalendarUnit |
                                   NSWeekdayCalendarUnit
                                              fromDate:date];
    
    int weekDayIndex = dateComps.weekday;
    NSString* array[] = {nil, @"日", @"月", @"火", @"水", @"木", @"金", @"土"};
    return [NSString stringWithFormat:@"%d月%d日 (%@)", dateComps.month, dateComps.day, array[weekDayIndex]];
}


-(long)getDateNumber:(NSDate*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *setDateComps = [calendar components:NSYearCalendarUnit |
                                   NSMonthCalendarUnit |
                                   NSDayCalendarUnit |
                                   NSWeekdayCalendarUnit
                                              fromDate:date];

    
    NSDateComponents *comps = [[NSDateComponents alloc]init];
    [comps setYear:setDateComps.year];
    [comps setMonth:setDateComps.month];
    [comps setDay:setDateComps.day];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];

    NSDate *returnDate = [calendar dateFromComponents:comps];
    return [returnDate timeIntervalSince1970];
}



-(int)lastSelectedTabIndex
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:USER_DEFAULT_LAST_SELECTED_TAB_INDEX];
}
-(void)setLastSelectedTabIndex:(int)lastSelectedTabIndex
{
    [[NSUserDefaults standardUserDefaults] setInteger:lastSelectedTabIndex forKey:USER_DEFAULT_LAST_SELECTED_TAB_INDEX];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(NSString*)uuid
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:USER_DEFAULT_UUID];
}
-(void)setUuid:(NSString *)uuid
{
    [[NSUserDefaults standardUserDefaults] setValue:uuid forKey:USER_DEFAULT_UUID];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


-(BOOL)isNotFirstLaunch
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:USER_DEFAULT_IS_NOT_FIRST_LAUNCH];
}

-(void)setIsNotFirstLaunch:(BOOL)isNotFirstLaunch
{
    [[NSUserDefaults standardUserDefaults] setBool:isNotFirstLaunch forKey:USER_DEFAULT_IS_NOT_FIRST_LAUNCH];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}



-(void)sendGoogleAnalitics:(NSString*)screenName
{
    // GoogleAnalitics
    id<GAITracker> tracker = [[GAI sharedInstance]defaultTracker];
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView]build]];
}


-(void)sendGoogleAnaliticsActionCategory:(NSString*)category action:(NSString*)action label:(NSString*)label
{
    id<GAITracker> tracker = [[GAI sharedInstance]defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category     // Event category (required)
                                                          action:action  // Event action (required)
                                                           label:label          // Event label
                                                           value:nil] build]];    // Event value
}

@end
