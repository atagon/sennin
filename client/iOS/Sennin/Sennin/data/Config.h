//
//  Config.h
//  Sennin
//
//  Created by abt on 2014/04/02.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#ifndef Sennin_Config_h
#define Sennin_Config_h

#define APP_WIDTH [UIScreen mainScreen].bounds.size.width
#define APP_HEIGHT [UIScreen mainScreen].bounds.size.height

#define BITLY_LOGIN_ID @"o_74pdgh23hq"
#define BITLY_APIKEY @"R_b2e0f37bb107378b807c96034beacb5e"

#define SUPPORT_MAIL @"kurohane@urban-ascetic.com"
#define HOST @"55auto.biz"
#define MAILMAGAZINE_REGISTER_URL @"https://55auto.biz/kurohane/responder.php"

#define TAB_COLORS @[@(0xe95849),@(0x43a0de),@(0x3fd07d),@(0xb83bc3)]

#define BG_COLOR 0xf3ecdc
#define BASE_COLOR 0x675a43

#define SENNIN_HOST @"urban-ascetic.com"
#define SENNIN_MAILFORM_HOST @"55auto.biz"
#define GAE_HOST @"http://seninnews.appspot.com"
//#define GAE_HOST @"http://localhost:8888"


#define SERVER_DEBUG false

#if SERVER_DEBUG
#define PHP_SERVER_HOST @"127.0.0.1"
#define PHP_SERVER_PORT @":8888"
#else
#define PHP_SERVER_HOST @"www.flickfrog.com"
#define PHP_SERVER_PORT @""
#endif


#define ARTICLE_LIST_FROM_PHP @"http://%@%@/sennin/mst_blogs/articleList?lastdate=%@"
#define ARTICLE_DETAIL_FROM_PHP @"http://%@%@/sennin/mst_blogs/detail?url=%@"

#define ARTICLE_LIST_FROM_GAE @"%@/articlelist/?lastdate=%@"
#define ARTICLE_WATCH_LOG_GAE @"%@/watchlog?uuid=%@&url=%@/"
#define ARTICLE_LIST_URL @"http://urban-ascetic.com/sitemap/"
#define BAD_IMAGE_PATH @"http://image.with2.net/img/banner/banner_good.gif"
#define BAD_IMAGE_HOST @"lh4.googleusercontent.com"

#define USER_DEFAULT_LAST_SELECTED_TAB_INDEX @"USER_DEFAULT_LAST_SELECTED_TAB_INDEX"
#define USER_DEFAULT_UUID @"USER_DEFAULT_UUID"
#define USER_DEFAULT_IS_NOT_FIRST_LAUNCH @"USER_DEFAULT_IS_NOT_FIRST_LAUNCH"
#endif
