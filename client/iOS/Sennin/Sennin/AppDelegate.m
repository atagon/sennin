//
//  AppDelegate.m
//  Sennin
//
//  Created by abt on 2014/03/29.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "AppDelegate.h"
#import "JMImageCache.h"
#import <FelloPush/KonectNotificationsAPI.h>
#import "Notification.h"
#import "PropertyDirector.h"
#import "VersionManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSString *appid = @"10769";
//    [KonectNotificationsAPI initialize:self launchOptions:launchOptions appId:appid];
    [KonectNotificationsAPI initialize:nil launchOptions:launchOptions appId:appid];
    
    // バッジをリセット
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance]logger]setLogLevel:kGAILogLevelError];
    id<GAITracker> tracker = [[GAI sharedInstance]trackerWithTrackingId:@"UA-44635206-4"];

    if ([PropertyDirector sharedDirector].uuid == nil) {
        NSString *uuid = [[NSUUID UUID] UUIDString];
        [PropertyDirector sharedDirector].uuid = uuid;
    }
    
    
    // バージョンチェックと、環境の更新
    [VersionManager checkVersionAndUpdateEnvironment];

    
    return YES;
}

// デバイストークンを受信した際の処理
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // 渡ってきたデバイストークンを渡す
    [KonectNotificationsAPI setupNotifications:deviceToken];
}

// プッシュ通知を受信した場合の処理
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // 渡ってきたuserinfoを渡す
    [KonectNotificationsAPI processNotifications:userInfo];
}

// このメソッドで、プッシュ通知からの起動後の処理を行う事ができる
- (void)onLaunchFromNotification:(NSString *)notificationsId message:(NSString *)message extra:(NSDictionary *)extra
{
    NSLog(@"ここでextraの中身に紐づいたインセンティブの付与などを行う事ができます");
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // バッジをリセット
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    [[NSNotificationCenter defaultCenter]postNotificationName:UPDATE_ARTICLE object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
