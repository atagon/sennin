//
//  NSString+stringByUrlEncoding.m
//  Sennin
//
//  Created by abt on 2014/04/14.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "NSString+stringByUrlEncoding.h"

@implementation NSString(stringByUrlEncoding)
- (NSString *)stringByUrlEncoding
{
    CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self, NULL, (CFStringRef)@"!*'""();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8);
                                                                    NSString *returnString = (__bridge NSString *)newString;
                                                                    CFRelease(newString);
                                                                    return returnString;
}
@end
