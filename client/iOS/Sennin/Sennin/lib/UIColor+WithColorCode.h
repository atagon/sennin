//
//  UIColor+WithColorCode.h
//  GoalPlus
//
//  Created by 倫広 安宅 on 12/01/05.
//  Copyright (c) 2012年 flickfrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (WithColorCode)

+(UIColor*) colorWithColorCode:(int)colorCode;

+(float)redFromColorCode:(int)colorCode;
+(float)greenFromColorCode:(int)colorCode;
+(float)blueFromColorCode:(int)colorCode;

@end
