//
//  UIImage+imagePath.h
//  SPaint
//
//  Created by new something on 05/03/13.
//  Copyright (c) 2013 com.flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (imagePath)

+ (UIImage*)imagePath:(NSString*)path;

@end
