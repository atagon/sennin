//
//  UIImage+Custom.h
//  Sennin
//
//  Created by abt on 2014/04/10.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (partialImageOfRect)

- (UIImage *)partialImageOfRect:(CGRect)rect;

@end
