//
//  AdView.h
//  QuickMemo
//
//  Created by abt on 2014/01/24.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleMobileAds/GoogleMobileAds.h>
#import "NADView.h"

//@interface AdView : UIView <GADBannerViewDelegate, NADViewDelegate>
@interface AdView : UIView <NADViewDelegate>

- (id)initWithFrame:(CGRect)frame parent:(UIViewController*)parentView;

- (void)updateAd;

@property(weak, readwrite)UIViewController *viewController;

@end
