//
//  UIImage+Custom.m
//  Sennin
//
//  Created by abt on 2014/04/10.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "UIImage+Custom.h"

@implementation  UIImage (partialImageOfRect)

- (UIImage *)partialImageOfRect:(CGRect)rect
{
    CGPoint originDrawPoint = CGPointMake(rect.origin.x * -1, rect.origin.y * -1);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    [self drawAtPoint:originDrawPoint];
    UIImage* partialImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return partialImage;
}

@end
