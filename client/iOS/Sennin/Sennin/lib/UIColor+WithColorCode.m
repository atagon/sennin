//
//  UIColor+WithColorCode.m
//  GoalPlus
//
//  Created by 倫広 安宅 on 12/01/05.
//  Copyright (c) 2012年 flickfrog. All rights reserved.
//

#import "UIColor+WithColorCode.h"

@implementation UIColor (WithColorCode)

+(UIColor*) colorWithColorCode:(int)colorCode
{
    float red = colorCode >> 16;
    float green = colorCode >> 8 & 0x0000FF;
    float blue = colorCode & 0x0000FF;
    return [UIColor colorWithRed:(float)red/255.f green:(float)green/255.f blue:(float)blue/255.f alpha:1.f];
}

+(float)redFromColorCode:(int)colorCode
{
    float red = colorCode >> 16;
    return red/255.f;
}

+(float)greenFromColorCode:(int)colorCode
{
    float green = colorCode >> 8 & 0x0000FF;
    return green/255.f;    
}

+(float)blueFromColorCode:(int)colorCode
{
    float blue = colorCode & 0x0000FF;
    return blue/255.f;
}

@end
