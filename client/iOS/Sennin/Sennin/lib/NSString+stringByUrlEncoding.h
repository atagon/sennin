//
//  NSString+stringByUrlEncoding.h
//  Sennin
//
//  Created by abt on 2014/04/14.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(stringByUrlEncoding)
- (NSString *)stringByUrlEncoding;
@end
