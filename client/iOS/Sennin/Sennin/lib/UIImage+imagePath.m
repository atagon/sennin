//
//  UIImage+imagePath.m
//  SPaint
//
//  Created by new something on 05/03/13.
//  Copyright (c) 2013 com.flickfrog. All rights reserved.
//

#import "UIImage+imagePath.h"

@implementation  UIImage (imagePath)

+ (UIImage*)imagePath:(NSString*)path
{
    UIImage* image;

    // ユーザーフォルダの画像チェック
    NSString* documentDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString* extension = [[path componentsSeparatedByString:@"."]objectAtIndex:1];
    NSString* file = [[path componentsSeparatedByString:@"."]objectAtIndex:0];
    
    NSString* filePath = [NSString stringWithFormat:@"%@/%@@2x.%@", documentDir,file,extension];
    NSFileManager* fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:filePath])
    {
        // ユーザーフォルダを見に行く
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    else
    {
        // アプリフォルダを見に行く
        NSString* imagePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@@2x", file] ofType:extension];
        image = [UIImage imageWithContentsOfFile:imagePath];

//        NSString* imagePath = [NSString stringWithFormat:@"%@.%@", file,extension];
//        image = [UIImage imageNamed:imagePath];
    }

    return image;
}

@end
