//
//  VersionManager.h
//  QuickTodo+
//
//  Created by abt on 2014/06/06.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionManager : NSObject

+(void)checkVersionAndUpdateEnvironment;

@end
