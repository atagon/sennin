//
//  VersionManager.m
//  QuickTodo+
//
//  Created by abt on 2014/06/06.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "VersionManager.h"
#import "SQLiteManager.h"


@implementation VersionManager

+(void)checkVersionAndUpdateEnvironment
{
    int version = [[SQLiteManager shardManager]getSystemVersion];
    if (version < 2)
    {
        // CATEGORY_GROUPテーブルを追加する
        [[SQLiteManager shardManager]alterTable1_1];
    }
    
    if (version < 3)
    {
        // is_deletedカラムを追加する
        [[SQLiteManager shardManager]alterTable1_2];
    }
    
    if (version < 4)
    {
        // 記事のキャッシュを一旦全部消す
        [[SQLiteManager shardManager]DBUpdate_ver4];
    }
}

@end
