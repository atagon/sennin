//
//  AdView.m
//  QuickMemo
//
//  Created by abt on 2014/01/24.
//  Copyright (c) 2014年 flickfrog. All rights reserved.
//

#import "AdView.h"
#import "UIColor+WithColorCode.h"



#define ADMOB @"ca-app-pub-9643402266626913/3547305589"


@implementation AdView {
//    GADBannerView *adMobBannerView;
    NADView* adView;
}

- (id)initWithFrame:(CGRect)frame parent:(UIViewController*)parentView
{
    self = [super initWithFrame:frame];
    if (self) {
        self.viewController = parentView;
//        adMobBannerView = nil;
    }
    return self;
}


- (void)updateAd
{
    adView.delegate = nil;
    [adView removeFromSuperview];
    adView = nil;
    
    
//    // 前回表示していたものはいったん消す
//    adMobBannerView.rootViewController = nil;
//    [adMobBannerView removeFromSuperview];
//    adMobBannerView = nil;
//    adMobBannerView.delegate = nil;

    [self initAdMob];
}


- (void)initAdMob
{
    adView = [[NADView alloc]initWithFrame:CGRectMake(0, 50, 320, 50)];
    [adView setIsOutputLog:NO];
    [adView setNendID:@"352fd2dc2059020ad715a4ad08855dec04cc86d4" spotID:@"162131"];
    [adView setDelegate:self];
    adView.alpha = 0.0;
    [adView load];
    [self addSubview:adView];
    
    
//    // AdMobを手前に表示
//    adMobBannerView = [[GADBannerView alloc]initWithFrame:CGRectMake(0, 50, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)];
//    adMobBannerView.adUnitID = ADMOB;
//    adMobBannerView.rootViewController = self.viewController;
//    adMobBannerView.delegate = self;
//    adMobBannerView.alpha = 0.0;
//    [self addSubview:adMobBannerView];
//    // 本番データのリクエスト
//    GADRequest* request = [GADRequest request];
//    request.testDevices = @[@"39dedea24576f28b39795e48bf4060bd19e15308"];
//    [adMobBannerView loadRequest:request];
}

- (void)nadViewDidFinishLoad:(NADView *)_adView
{
    CGRect frame = _adView.frame;
    frame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        _adView.frame = frame;
        _adView.alpha = 1.0;
    }];
}

//- (void)adViewDidReceiveAd:(GADBannerView *)view
//{
//    CGRect frame = adMobBannerView.frame;
//    frame.origin.y = 0;
//    [UIView animateWithDuration:0.3 animations:^{
//        adMobBannerView.frame = frame;
//        adMobBannerView.alpha = 1.0;
//    }];
//}
//
//- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
//{
//    NSLog(@"error %@", error);
//}
//
//- (void)dealloc
//{
//    adMobBannerView.delegate = nil;
//}


@end
