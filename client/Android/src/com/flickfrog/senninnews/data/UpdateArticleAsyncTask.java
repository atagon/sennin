package com.flickfrog.senninnews.data;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.os.AsyncTask;

public class UpdateArticleAsyncTask extends AsyncTask<Void, Void, String> {
	private String GAE_HOST = "http://seninnews.appspot.com";
//	private String GAE_HOST = "http://127.0.0.1:8888";
	private String ARTICLE_LIST_FROM_GAE = "/articlelist/?lastdate=";
	private Context context;
//	private boolean needUpdate;
	
    //Activiyへのコールバック用interface
    public interface AsyncTaskCallback {
        void postExecute(String result);
    }
	
    private AsyncTaskCallback callback = null;
    
    
	public UpdateArticleAsyncTask(Context context, AsyncTaskCallback callback) {
		super();
		
		this.context = context;
		this.callback = callback;
	}

	@Override
	protected String doInBackground(Void... params) {
		SQLiteManager sqlite = new SQLiteManager(this.context);
		long lastDate = sqlite.getlastDate();

		URI uri = null;
		try {
			uri = new URI(GAE_HOST + ARTICLE_LIST_FROM_GAE + lastDate);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		String ret = "no response";
		HttpPost request = new HttpPost(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			ret = httpClient.execute(request, new ResponseHandler<String>() {

				@Override
				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {
					
					if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						return EntityUtils.toString(response.getEntity(), "UTF-8");
					}
					
					return null;
				}
			});
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		
//		needUpdate = new SQLiteManager(this.context).updateArticles(ret);		
		return ret;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		callback.postExecute(result);
	}
//	protected void onPostExecute(Void result) {
//		super.onPostExecute(result);
//		callback.postExecute(needUpdate);
//	}
}
