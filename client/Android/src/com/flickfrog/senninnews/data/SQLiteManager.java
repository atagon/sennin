package com.flickfrog.senninnews.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class SQLiteManager extends SQLiteOpenHelper {
	static final private String DB_NAME = "Sennin.sqlite";
//	static final private int VERSION = 2;
	static final private int VERSION = 300900;
	private final Context context;
	private final File dbPathFile;
	SQLiteDatabase _dataBase = null;
	
	private final int SQLITE_VERSION = 2;
	
	
	public SQLiteManager(Context context) {
		super(context, DB_NAME, null, VERSION);
		
		this.context = context;
		dbPathFile = context.getDatabasePath(DB_NAME);
		
		try {
			createEmptyDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
//	@Override
//	public void onCreate(SQLiteDatabase db) {
//		super.onOpen(db);
//		this.createDatabase = true;
//	}
//
//	@Override
//	public SQLiteDatabase getWritableDatabase() {
//		SQLiteDatabase database = super.getWritableDatabase();
//		if (this.createDatabase) {
//			
//		}
//		// TODO Auto-generated method stub
//		return super.getWritableDatabase();
//	}
	
	public int version() {
		SQLiteDatabase dataBase = getReadableDatabase();
		
		
		SQLiteStatement statement = dataBase.compileStatement("SELECT version FROM SYSTEMINFO WHERE id = 1;");
		int version = (int)statement.simpleQueryForLong();
		dataBase.close();
		
		return version;
	}
	
	public long getlastDate() {
		SQLiteDatabase dataBase = getReadableDatabase();

		Cursor cursor = dataBase.rawQuery("SELECT COALESCE(max(update_time), 0) as lastdate FROM WEBCACHE;", null);
		long lastDate = 0;
		if (cursor.moveToFirst()) {
			lastDate = cursor.getLong(cursor.getColumnIndex("lastdate"));
		}
		dataBase.close();
		
		return lastDate;
	}
	
	public ArrayList<HashMap<String, Object>> articleTitles() {
		SQLiteDatabase dataBase = getReadableDatabase();

		Cursor cursor = dataBase.rawQuery("SELECT new, thumbnail, title, type, url, percent FROM WEBCACHE WHERE title is not NULL ORDER BY create_time desc;", null);
		ArrayList<HashMap<String, Object>> articleTitles = new ArrayList<HashMap<String, Object>>();
		boolean eol = cursor.moveToFirst();
		while (eol) {
			HashMap<String, Object> articleTitle = new HashMap<String, Object>();
			articleTitle.put("new", cursor.getInt(cursor.getColumnIndex("new")));
			articleTitle.put("thumbnail", cursor.getString(cursor.getColumnIndex("thumbnail")));
			articleTitle.put("title", cursor.getString(cursor.getColumnIndex("title")));
			articleTitle.put("type", cursor.getString(cursor.getColumnIndex("type")));
			articleTitle.put("url", cursor.getString(cursor.getColumnIndex("url")));
			articleTitle.put("percent", cursor.getString(cursor.getColumnIndex("percent")));
			
			articleTitles.add(articleTitle);
			
			eol = cursor.moveToNext();
		}
		dataBase.close();

		return articleTitles;
	}
	
//	public void resetNewArticle() {
//		SQLiteDatabase dataBase = getReadableDatabase();
//		
//		ContentValues cv = new ContentValues();
//		cv.put("new", 0);
//		dataBase.update("WEBCACHE", cv, null, null);
//		
//		dataBase.close();
//	}
	
	
	public boolean updateArticles(String json) {
		boolean needUpdate = false;

		ArrayList<HashMap<String, Object>> datas = articleTitles();
		int isNew = (datas.size() == 0) ? 0:1;

		
		SQLiteDatabase dataBase = getReadableDatabase();
		try {
			JSONObject articleJSON = new JSONObject(json);
			JSONArray articleArrayJSON = articleJSON.getJSONArray("list");

			if (articleArrayJSON.length() > 0) {
				// reset new.
				ContentValues cv = new ContentValues();
				cv.put("new", 0);
				dataBase.update("WEBCACHE", cv, null, null);
			}
			
			for (int i=0; i< articleArrayJSON.length(); i++) {
				JSONObject articleObject = articleArrayJSON.getJSONObject(i);				
				Cursor cursor = dataBase.rawQuery("SELECT url FROM WEBCACHE WHERE url = \"" + articleObject.getString("url") + "\"", null);

				if (!cursor.moveToFirst()) {
					// insert
					ContentValues cv = new ContentValues();
					cv.put("url", articleObject.getString("url"));
					cv.put("title", articleObject.getString("title"));
					cv.put("thumbnail", articleObject.getString("thumbnail"));
					cv.put("type", articleObject.getString("type"));
					cv.put("new", isNew);
					cv.put("create_time", articleObject.getString("createdDate"));
					cv.put("update_time", articleObject.getString("updatedDate"));

					dataBase.insert("WEBCACHE", null, cv);
				} else {
					// update
					ContentValues cv = new ContentValues();
					cv.put("title", articleObject.getString("title"));
					cv.put("thumbnail", articleObject.getString("thumbnail"));
					cv.put("type", articleObject.getString("type"));
					cv.put("update_time", articleObject.getString("updatedDate"));
					cv.put("html", "");
					cv.put("contents", "");
					
					String[] whereArgs = {articleObject.getString("url")};

					dataBase.update("WEBCACHE", cv, "url=?", whereArgs);
				}
				
				needUpdate = true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			dataBase.close();
		}
		
		
		return needUpdate;
	}
	
	
	public void updateReadPercentWithURL(String url, int percent)
	{
		SQLiteDatabase dataBase = getReadableDatabase();
		
		int setPercent = (percent > 100) ? 100 : percent;
		
		ContentValues cv = new ContentValues();
		cv.put("percent", setPercent);

		String[] whereArgs = {url, String.valueOf(setPercent)};
		dataBase.update("WEBCACHE", cv, "url=? AND percent < ?", whereArgs);
		
		dataBase.close();
	}
	
	public HashMap<String, Object> getArticleData(String url) {
		SQLiteDatabase dataBase = getReadableDatabase();
		
		Cursor cursor = dataBase.rawQuery("SELECT html, new, thumbnail, title, type, url, percent FROM WEBCACHE WHERE url=\"" + url + "\"", null);
		HashMap<String, Object> articleTitle = new HashMap<String, Object>();
		
		if (cursor.moveToFirst()) {
			articleTitle.put("html", cursor.getString(cursor.getColumnIndex("html")));
			articleTitle.put("new", cursor.getInt(cursor.getColumnIndex("new")));
			articleTitle.put("thumbnail", cursor.getString(cursor.getColumnIndex("thumbnail")));
			articleTitle.put("title", cursor.getString(cursor.getColumnIndex("title")));
			articleTitle.put("type", cursor.getString(cursor.getColumnIndex("type")));
			articleTitle.put("url", cursor.getString(cursor.getColumnIndex("url")));
			articleTitle.put("percent", cursor.getString(cursor.getColumnIndex("percent")));
			
			dataBase.close();
			
			return articleTitle;
		} else {
			dataBase.close();
			
			return null;
		}
	}
	
	public void registerArticleHTML(String html, String contents, String url) {
		SQLiteDatabase dataBase = getReadableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put("html", html);
		cv.put("contents", contents);
		
		String[] whereArgs = {url};
		dataBase.update("WEBCACHE", cv, "url=?", whereArgs);
		
		dataBase.close();
//		UPDATE WEBCACHE SET html=?, contents=? WHERE url=?;
	}
	
	public void setUnNewWithURL(String url) {
		SQLiteDatabase dataBase = getReadableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put("new", 0);
		
		String[] whereArgs = {url};
		dataBase.update("WEBCACHE", cv, "url=?", whereArgs);

		dataBase.close();
	}

//------------------------------ settings -------------
	public void updateDB() {
		SQLiteDatabase dataBase = getReadableDatabase();
		
		Cursor cursor = dataBase.rawQuery("SELECT version FROM SYSTEMINFO WHERE id=1", null);
		if (cursor.moveToFirst()) {
			int currentVersion = cursor.getInt(cursor.getColumnIndex("version"));
			
			if (currentVersion < SQLITE_VERSION) {
//				dataBase.execSQL("alter table WEBCACHE add test INTEGER DEFAULT 999");
			}
		}
		
		dataBase.close();
	}
	
	
	private void createEmptyDataBase() throws IOException {
		if (!isExistDB()) {
			Log.i("##DB exist check", "DBがない");
//			Log.i("CHECK!!!", "before getReadableDatabase 3");
			getReadableDatabase();
			try {
				InputStream inputStream = context.getAssets().open(DB_NAME);
				OutputStream outputStream = new FileOutputStream(dbPathFile);
		
				
				byte[] buffer = new byte[1024];
				int size;
				while ((size = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, size);
				}
				outputStream.flush();
				outputStream.close();
				inputStream.close();
				
				SQLiteDatabase checkDB = null; 
				try {
					checkDB = SQLiteDatabase.openDatabase(dbPathFile.getAbsolutePath(), null, SQLiteDatabase.OPEN_READWRITE);
				} catch (SQLiteException e) {
				}
				
				if (checkDB != null) {
					Log.i("CHECK !!!", "@createEmptyDataBase version=" + checkDB.getVersion());
					checkDB.close();
				} else {
				}
			} catch (IOException e) {
				throw new Error("Error copying db");
			}
		}
	}
	
	private boolean isExistDB() {
		SQLiteDatabase checkDB = null;
		try {
			checkDB = SQLiteDatabase.openDatabase(
					dbPathFile.getAbsolutePath(),
					null,
					SQLiteDatabase.OPEN_READONLY);
		} catch (SQLiteException e) {
		}
		
		if (checkDB == null) {
			return false;
		} else {
//			Log.i("CHECK !!!", "@isExistDB version=" + checkDB.getVersion());
			if (VERSION < checkDB.getVersion()) {
//				checkDB.setVersion(checkDB.getVersion());
			}
			checkDB.close();
			return true;
		}
	}
	
    @Override  
    public void onCreate(SQLiteDatabase db) {  
    	Log.i("SQLiteManager#onCreate", "version=" + db.getVersion());
    } 
	

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		Log.i("SQLiteManager#onUpgrade", "version=" + arg0.getVersion() + " oldV=" + arg1 + " > newV=" + arg2);
	}
}
