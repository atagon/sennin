package com.flickfrog.senninnews;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class MailMagazineReg extends Activity {
	private EditText nameText;
	private EditText mailText;
	private int REQUEST_CODE_MAILMAGAZINE = 1111;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_mailmagazine);
		
		nameText = (EditText)findViewById(R.id.mailmagazineName);
		mailText = (EditText)findViewById(R.id.mailmagazineMail);
		
		Button regBtn = (Button)findViewById(R.id.registMailMagazineBtn);
		regBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (nameText.getText().toString().equals("")) {
					AlertDialog.Builder alert = new AlertDialog.Builder(MailMagazineReg.this);
					alert.setTitle("確認");
					alert.setMessage("「名前」を入力して下さい。");
					alert.setPositiveButton("OK", null);
					alert.show();
				} else if(mailText.getText().toString().equals("")) {
					AlertDialog.Builder alert = new AlertDialog.Builder(MailMagazineReg.this);
					alert.setTitle("確認");
					alert.setMessage("「メールアドレス」を入力して下さい。");
					alert.setPositiveButton("OK", null);
					alert.show();
				} else {
					Intent intent = new Intent(MailMagazineReg.this, com.flickfrog.senninnews.MailMagazineConfirm.class);
					intent.putExtra("name", nameText.getText().toString());
					intent.putExtra("mail", mailText.getText().toString());
					startActivityForResult(intent, REQUEST_CODE_MAILMAGAZINE);
				}
			}
		});
	}
}
