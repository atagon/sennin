package com.flickfrog.senninnews.common;

import org.json.JSONObject;

import android.util.Log;

import com.unicon_ltd.konect.sdk.IKonectNotificationsCallback;

public class NotificationsCallback implements IKonectNotificationsCallback{

	@Override
	public void onCloseAd(String arg0, String arg1) {
	}

	@Override
	public void onCompleteAdRequest(String arg0, boolean arg1) {
	}

	@Override
	public void onLaunchFromNotification(String arg0, String arg1,
			JSONObject arg2) {
		Log.d("notification", "通知を検知");
		
	}

	@Override
	public void onShowAd(String arg0) {
	}

}
