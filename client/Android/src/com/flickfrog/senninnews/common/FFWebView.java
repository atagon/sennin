package com.flickfrog.senninnews.common;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class FFWebView extends WebView {

    private OnScrollChangedCallback2 mOnScrollChangedCallback;

    public FFWebView(final Context context)
    {
        super(context);
    }

    public FFWebView(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
    }

    public FFWebView(final Context context, final AttributeSet attrs, final int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt)
    {
        super.onScrollChanged(l, t, oldl, oldt);
        if(mOnScrollChangedCallback != null) mOnScrollChangedCallback.onScroll(l, t);
    }

    public OnScrollChangedCallback2 getOnScrollChangedCallback()
    {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback2(final OnScrollChangedCallback2 onScrollChangedCallback)
    {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    public static interface OnScrollChangedCallback2
    {
        public void onScroll(int l, int t);
    }
}
