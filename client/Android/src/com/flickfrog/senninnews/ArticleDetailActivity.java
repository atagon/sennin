package com.flickfrog.senninnews;

import java.io.IOException;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.flickfrog.senninnews.common.FFWebView;
import com.flickfrog.senninnews.common.FFWebView.OnScrollChangedCallback2;
import com.flickfrog.senninnews.data.Config;
import com.flickfrog.senninnews.data.PropertyDirector;
import com.flickfrog.senninnews.data.SQLiteManager;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class ArticleDetailActivity extends Activity {
	private String SENNIN_HOST = "http://urban-ascetic.com";
	private String SENNIN_MAILFORM_HOST = "https://55auto.biz";
	private String MAILMAGAZINE_REGISTER_URL = "https://55auto.biz/kurohane/responder.php";
	private FFWebView webView;
	private ProgressBar progressBar;
	private Button resumeButton;
	private TextView scrollPercentText;
	private float INIT_SCALE = 1.3f;
	private String YOUTUBE_API_KEI = "AIzaSyC5doLM1UOHzKPvSsJp6AMMaencrcAc_Kk";
	
	private Handler webviewHandler = null;
	private String analizeURL = null;
	private String analizeHTML = null;
	private int currentDBPercent = 0;
	private AdView adView;
	private LinearLayout layoutAd;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_detail);
		
		
		adView = new AdView(this);
		adView.setAdUnitId(Config.ADMOB_UNITID);
		adView.setAdSize(AdSize.BANNER);
		layoutAd = (LinearLayout)findViewById(R.id.detailActivityAd);
		layoutAd.addView(adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
		
		
		scrollPercentText = (TextView)findViewById(R.id.scrollPercent);
		
		progressBar = (ProgressBar)findViewById(R.id.articleDetailProgressBar);
		
		webView = (FFWebView)findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
//		if (Build.VERSION.SDK_INT > 7) {
//			webView.getSettings().setPluginState(PluginState.ON);
//		} else {
//			webView.getSettings().setP
//		}
		webView.setVerticalScrollbarOverlay(true);
		
		webView.setInitialScale((int)(INIT_SCALE * 100));

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
//				Log.i("shouldOverrideUrlLoading", "url=" + url);

				return super.shouldOverrideUrlLoading(view, url);
			}
			
			
			@Override
			public void onLoadResource(WebView view, String url) {
//				Log.i("onLoadResource", "url=" + url);
				
				if (url.startsWith("http://www.youtube.com/"))
				{
//					Intent videoClient = new Intent(Intent.ACTION_VIEW);
//					videoClient.setData(Uri.parse(url));
//					videoClient.setClassName("com.google.android.youtube", "com.google.android.youtube.WatchActivity");
//					startActivity(videoClient);
					
					if (url.split("v=").length < 2) {
						Log.i("なんぞ？？？", url);
						return;
					}
					
					String videoID = url.split("v=")[1];
					Intent intent = null;
					if (YouTubeIntents.isYouTubeInstalled(getApplicationContext()))
					{
						if (YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getApplicationContext()) == YouTubeInitializationResult.SUCCESS) {
							intent = YouTubeStandalonePlayer.createVideoIntent(ArticleDetailActivity.this, YOUTUBE_API_KEI, videoID, 0, true, false);
						} else if (YouTubeIntents.canResolvePlayVideoIntent(getApplicationContext())) {
							intent = YouTubeIntents.createPlayVideoIntent(getApplicationContext(), videoID);
						}
					} else {
						intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(url));
						intent.setClassName("com.google.android.youtube", "com.google.android.youtube.WatchActivity");
					}
					
					if (intent != null)
						startActivity(intent);
					
				} else if (url.startsWith(SENNIN_MAILFORM_HOST) && url.endsWith("responder.php")) {
					super.onLoadResource(view, url);
//					analizeMailForm(url); // なんかエラーでるからとりあえずそのまま表示しとく
				} else if (url.startsWith(SENNIN_HOST) && url.endsWith("/")) {
					analizeURL(url);
				} else {
//					Log.i("OTHERS===", "url=" + url);
//					super.onLoadResource(view, url);
				}				
			}
		});

		
		
		webView.setOnScrollChangedCallback2(new OnScrollChangedCallback2() {
			@Override
			public void onScroll(int l, int t) {
				double rate = (double)webView.getScrollY() / (double)((float)(webView.getContentHeight()-webView.getHeight()) * INIT_SCALE);
				int percent = (int)(rate * 100 + 0.5);
				if (percent > 100)
				{
					percent = 100;
				}
				scrollPercentText.setText(percent + "%");
				Intent intent = getIntent();
				String url = intent.getStringExtra("url");
				new SQLiteManager(ArticleDetailActivity.this).updateReadPercentWithURL(url, percent);
				
				
				if (percent == 100)
				{
					scrollPercentText.setTextColor(Color.parseColor("#FFA500"));
					if (resumeButton != null) {
						RelativeLayout layout = (RelativeLayout)findViewById(R.id.detailLayout);
						layout.removeView(resumeButton);
						resumeButton = null;
					}
				} else {
					scrollPercentText.setTextColor(Color.parseColor("#CCCCCC"));
				}
				
				float alpha = 1f - (float)percent / 5;
				if (alpha < 0f)
					alpha = 0f;
				AlphaAnimation alphaUp = new AlphaAnimation(alpha, alpha);
				alphaUp.setFillAfter(true);
				if (resumeButton != null)
				{
					resumeButton.setAnimation(alphaUp);
				}
//				if (alpha == 0f) {
//					if (resumeButton != null) {
//						RelativeLayout layout = (RelativeLayout)findViewById(R.id.detailLayout);
//						layout.removeView(resumeButton);
//						resumeButton = null;
//					}
//				}
			}
		});
		
		Intent intent = this.getIntent();
		String url = intent.getStringExtra("url");
		
		// newを消す
		new SQLiteManager(this).setUnNewWithURL(url);
		
		HashMap<String, Object> articleData = new SQLiteManager(ArticleDetailActivity.this).getArticleData(url);
		new PropertyDirector().sendGoogleAnalitics("記事: " + articleData.get("title"), this);
		
		currentDBPercent = Integer.parseInt(articleData.get("percent").toString());
		resumeButton = (Button)findViewById(R.id.resumeButton);
		resumeButton.setEnabled(false);
		if (currentDBPercent == 0 || currentDBPercent == 100) {
			RelativeLayout layout = (RelativeLayout)findViewById(R.id.detailLayout);
			layout.removeView(resumeButton);
		} else {
			resumeButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					float scale = (float)currentDBPercent / 100;
					int destScrollY = (int)((float)(webView.getContentHeight()-webView.getHeight()) * INIT_SCALE  * scale);
					webView.scrollTo(0, destScrollY);
					
					RelativeLayout layout = (RelativeLayout)findViewById(R.id.detailLayout);
					layout.removeView(resumeButton);
					resumeButton = null;
				}
			});
		}
		
//		webView.loadUrl(url);
		analizeURL(url);
	}

	private void analizeMailForm(String url) {
		progressBar.setVisibility(View.VISIBLE);
		analizeURL = url;
		
		webviewHandler = new Handler();
		new Thread( new Runnable() {
			@Override
			public void run() {				
				try {
					Document document = Jsoup.connect(analizeURL).get();
			        String html = document.toString();

			        
			        // HTMLを改ざんして、スマフォに最適な表示に変える
			        String destHTMLParts = "<style type=\"text/css\">" +
			        "#wrap{padding:0px 0; width:100%25; margin:0;}" +
			        "TABLE{width:250px;}" +
			        "HR{width:100%25;}" +
			        "TABLE TD {word-break: break-all;}" +
			        ".btn {" +
			        "width: 200px;" +
			        "height: 50px;" +
			        "margin: 0;" +
			        "padding: 5px;" +
			        "background: -webkit-gradient(linear, left top, left bottom, from(#FFAA33), to(#FF8800));" +
			        "border: 1px #F27300 solid;" +
			        "color: #FFF;" +
			        "-webkit-appearance: none;" +
			        "-webkit-border-radius: 10px;" +
			        "-webkit-box-shadow: 0 2px 2px #CCC;" +
			        "text-shadow: 1px 2px 3px #C45C00;" +
			        "}" +
			        "</style>" +
			        "</head>";
			        
			        html = html.replace("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\" />" 
, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
			        html = html.replace("</head>", destHTMLParts);
			        html = html.replace("<INPUT type=\"button\" class=\"btn\" name=\"bak\" value=\"戻る\" onClick=\"history.back()\">", "");
			        html = html.replace("/jslib", "https://55auto.biz/jslib");
			        html = html.replace("/css/main.css", "https://55auto.biz/css/main.css");
			        html = html.replace("responder.php", MAILMAGAZINE_REGISTER_URL);
					
					
					analizeHTML = html;
					webviewHandler.post(new Runnable() {
						@Override
						public void run() {
							webView.loadData(analizeHTML, "text/html; charset=utf-8", "utf-8");
							progressBar.setVisibility(View.INVISIBLE);
						}					
					});
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}).start();
	}
/*
        NSData *htmlData = (NSData*)responseObject;
        NSString *htmlString = [[NSString alloc]initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
        
        // HTMLを改ざんして、スマフォに最適な表示に変える
        NSString *destHTMLParts = @"<style type=\"text/css\">\
        #wrap{padding:0px 0; width:100%; margin:0;}\
        TABLE{width:250px;}\
        HR{width:100%;}\
        TABLE TD {word-break: break-all;}\
        .btn {\
        width: 200px;\
        height: 50px;\
        margin: 0;\
        padding: 5px;\
        background: -webkit-gradient(linear, left top, left bottom, from(#FFAA33), to(#FF8800));\
        border: 1px #F27300 solid;\
        color: #FFF;\
        -webkit-appearance: none;\
        -webkit-border-radius: 10px;\
        -webkit-box-shadow: 0 2px 2px #CCC;\
        text-shadow: 1px 2px 3px #C45C00;\
        }\
        </style>\
        </head>";
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"</head>" withString:destHTMLParts];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<INPUT type=\"button\" class=\"btn\" name=\"bak\" value=\"戻る\" onClick=\"history.back()\">" withString:@""];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/jslib" withString:@"https://55auto.biz/jslib"];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/css/main.css" withString:@"https://55auto.biz/css/main.css"];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"responder.php" withString:MAILMAGAZINE_REGISTER_URL];		
 */

	
	private void analizeURL(String url) {
		progressBar.setVisibility(View.VISIBLE);
		analizeURL = url;
		
		webviewHandler = new Handler();
		new Thread( new Runnable() {
			@Override
			public void run() {				
				try {
					String contentHTML = null;
					Document document = null;
					Element element = null;
					
					HashMap<String, Object> articleData = new SQLiteManager(ArticleDetailActivity.this).getArticleData(analizeURL);
					if (articleData != null
							&& articleData.get("html") != null
							&& !articleData.get("html").equals("")) {
						contentHTML = articleData.get("html").toString();
						document = Jsoup.parse(contentHTML);
						element = document.getElementById("main");
					} else {
						document = Jsoup.connect(analizeURL).get();
						element = document.getElementById("main");
						
						contentHTML = element.toString();
						
						new SQLiteManager(ArticleDetailActivity.this).registerArticleHTML(contentHTML, document.text(), analizeURL);
					}
//					Log.i("contentHTML", contentHTML);
					
					
					Elements IFRAMEs = element.getElementsByTag("iframe");
					for (int i=0; i<IFRAMEs.size(); i++) {
						Element IFRAME = IFRAMEs.get(i);
						String IFRAME_src = IFRAME.attr("src");
						
						if (IFRAME_src.indexOf("youtube") == -1)
						{
							contentHTML = contentHTML.replace(IFRAME.toString(), "");
						} else {
							String[] videoIDtemp = IFRAME_src.split("/");
							String videoID = videoIDtemp[videoIDtemp.length-1];
							String videoThumbnailPath = "http://img.youtube.com/vi/" + videoID + "/0.jpg"; 
			//				String A_href = "http://www.youtube.com/embed/" + videoID + "?showinfo=0";
							String A_href = "http://www.youtube.com/watch?v=" + videoID;
			
							String newIFRAME_IMG = "<A href=\"" + A_href + "\"><font color=\"red\">サムネイルをクリックで再生</font><br/><IMG src=\"" + videoThumbnailPath + "\" width=\"100%25\"/></A>";
							contentHTML = contentHTML.replace(IFRAME.toString(), newIFRAME_IMG);							
						}
					}
					
		            // コメント内容の幅を少し縮める
		//          contentHTML = [contentHTML stringByReplacingOccurrencesOfString:@"cols=\"50\"" withString:@"cols=\"47\""];
		          
		          // 記事以外の余計な部分を削除する
		          //                contentHTML = [[contentHTML componentsSeparatedByString:@"<p>例によって良ければ帰る前にこちらをクリックしてやって下さい。</p>"]objectAtIndex:0];
		          
		          // コメント欄を消す
		          contentHTML = contentHTML.split("<h3 id=\"respond\">")[0];
		
		          // メール登録フォールを小さくする
//		          contentHTML = contentHTML.replace("<table style=\"width: 500px; font-size: 14px;\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#953716\">", "<table style=\"width: 100%25; font-size: 12px;\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\" bgcolor=\"#953716\">");
//		          contentHTML = contentHTML.replace("<td align=\"center\" bgcolor=\"#edbba6\" width=\"150\"><b>名前</b></td>", "<td style=\"width:150px;\" bgcolor=\"#edbba6\"><center><b>名　前</b></center></td>");
//		          contentHTML = contentHTML.replace("<td align=\"center\" bgcolor=\"#edbba6\" width=\"150\"><b>メールアドレス</b></td>", "<td bgcolor=\"#edbba6\"><center><b>メール</b></center></td>");
		
		          // メール登録フォームの送信先変更
//		          contentHTML = contentHTML.replace("<input name=\"tno\" type=\"hidden\" value=\"131\" /><input name=\"fld40\" type=\"hidden\" value=\"RES_TSG_BLG_RSR\" />", "<input name=\"tno\" type=\"hidden\" value=\"133\" /><input name=\"fld40\" type=\"hidden\" value=\"RES_TSG_IAP_RSR\" /><input name=\"bak\" type=\"hidden\" value=\"-2\" />");
		          
		          String html = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" + 
									"<style type=\"text/css\">" +
		                              "html{" +
		                              "overflow-x : hidden;" +
		                              "overflow-y : auto;" +
		                              "}" +
		                              "body{" +
		                                  "overflow-x : hidden;" +
		                                  "overflow-y : auto;" +
		                              "}" +
		                              "p{margin:10;}" +
		                              "#main p{font-size:13.5pt; color:#524942; padding:0 10 0 10; line-height:150%25;}" +
		                              "#main h2{height:auto !important; text-align:center; padding:30 0 30 0; font-size:18px; color:#F5F5F5; background:#f8f7ed url(http://urban-ascetic.com/wp/wp-content/themes/cloudtpl_787/images/bg_top.png) repeat-x;}" +
		                              "#date {text-align:right; padding:0 10 0 0; color:#524942;}" +
		                              "body{margin:0; overflow-x:hidden;}" +
		                              "#wrap{padding:0px 0; width:100%25; margin:0;}" +
		                              "</style>" +
		                              "</head>" +
		                              "<body width=\"320px\">" + 
		                              contentHTML.replaceAll("%", "%25") +
		                              "</div></body>" +
		                              "</html>";
		          
		          	analizeHTML = html;		          
//		          	analizeHTML = element.toString();
		          	webviewHandler.post(new Runnable() {
						@Override
						public void run() {
							webView.loadData(analizeHTML, "text/html; charset=utf-8", "utf-8");
							progressBar.setVisibility(View.INVISIBLE);
							if (resumeButton != null) {
								resumeButton.setEnabled(true);
							}
						}
					});
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}).start();
	}
	
	@Override
	protected void onDestroy() {
		Intent gettedIntent = getIntent();
		String url = gettedIntent.getStringExtra("url");

		Intent intent = new Intent();
		intent.putExtra("url", url);
		if (getParent() == null) {
			setResult(Activity.RESULT_OK, intent);			
		} else {
			getParent().setResult(Activity.RESULT_OK, intent);
		}
		
//		finish();
		
		super.onDestroy();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		EasyTracker.getInstance(this).activityStop(this);
	}
}
