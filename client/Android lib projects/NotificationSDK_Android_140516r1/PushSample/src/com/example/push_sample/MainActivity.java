package com.example.push_sample;

import android.app.Activity;
import android.content.Intent;

import com.unicon_ltd.konect.sdk.KonectNotificationsAPI;

public class MainActivity extends Activity {

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	@Override
	public void onResume() {
		super.onResume();
		KonectNotificationsAPI.processIntent(getIntent());
	}
	
}
