package com.example.push_sample;

import org.json.JSONObject;

import android.util.Log;

import com.unicon_ltd.konect.sdk.IKonectNotificationsCallback;

public class NotificationsCallback implements
		IKonectNotificationsCallback {

	@Override
	public void onLaunchFromNotification(String id, String message,
			JSONObject extra) {
		Log.d("KonectNotificationCallback", "プッシュ通知から起動しました");
	}

	@Override
	public void onCompleteAdRequest(String scene, boolean success) {
		Log.d("KonectNotificationCallback", "ここで広告リクエストの成否を受け取る事が出来ます");
	}

	@Override
	public void onCloseAd(String scene, String reason) {
		Log.d("KonectNotificationCallback", "ここで広告が閉じたイベントを受け取る事が出来ます");
	}

	@Override
	public void onShowAd(String scene) {
		Log.d("KonectNotificationCallback", "ここで広告が開いたイベントを受け取る事が出来ます");
	}
	
}
