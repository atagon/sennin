package com.example.push_sample;

import android.app.Application;

import com.unicon_ltd.konect.sdk.IKonectNotificationsCallback;
import com.unicon_ltd.konect.sdk.KonectNotificationsAPI;

public class MainApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		// プッシュ通知からの起動を受け取るためのオブジェクトを作成
		IKonectNotificationsCallback callback = new NotificationsCallback();
		// SDKを初期化
		KonectNotificationsAPI.initialize(this, callback);
		// プッシュ通知を設定
		KonectNotificationsAPI.setupNotifications();
	}
}
